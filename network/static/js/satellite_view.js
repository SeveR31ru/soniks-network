/* global Chart MicroModal human_frequency, moment, calcPolarPlotSVG, gettext, Fancybox */

$(document).ready(function () {
    'use strict';

    // btn-copy TLE
    var btnCopy = document.getElementsByClassName('btn-copy');
    for (var i = 0; i < btnCopy.length; i++){
        btnCopy[i].addEventListener('click', event => {
            var copyTarget = event.target.getAttribute('data-target');
            var copyText = document.getElementById(copyTarget).innerHTML;
            copyText = copyText.replace('<br>', '\n');
            copyFunction(copyText);
        });
    }

    // JSON formatter
    var jsonFormatter = document.querySelectorAll('code[data-type="json"]');
    for (i = 0; i < jsonFormatter.length; i++){
        const jsonFormatted = jsonFormatter[i].innerHTML;
        let regexString = '';
        const brace = {
            brace: 0
        };
        
        
        regexString = jsonFormatted.replace(
            /({|}[,]*|[^{}:]+:[^{}:,]*[,{]*)/g,
            (m, p1) => {
                const returnFunction = () =>
                    `<div style='text-indent: ${brace['brace'] * 20}px;'>${p1}</div>`;
                let returnString = 0;
                if (p1.lastIndexOf('{') === p1.length - 1) {
                    returnString = returnFunction();
                    brace['brace'] += 1;
                }
                else if (p1.indexOf('}') === 0) {
                    brace['brace'] -= 1;
                    returnString = returnFunction();
                } else {
                    returnString = returnFunction();
                }
                return returnString;
            }
        );
        jsonFormatter[i].innerHTML += regexString;
    }
    var translated_type={'Приемник':gettext('Приемник'),
        'Передатчик':gettext('Передатчик'),
        'Приемопередатчик':gettext('Приемопередатчик')
    };
    var fields = {
        'type': gettext('Тип'),
        'downlink_mode': gettext('Модуляция канала вниз'),
        'downlink_low': gettext('Частота канала вниз'),
        'baud': gettext('Бод'),
        'uplink_mode': gettext('Модуляция канала вверх'),
        'uplink_low': gettext('Частота канала вверх'),
    };
    var sat_id = $('#sat_id').data('satid');
    
    $.ajax({
        url: `/api/transmitters/?format=json&satellite__sat_id=${sat_id}`
    }).done(function(data) {
        var cards = $('.transmitters-cards');
        var cnt = 0;
        for (var transmitter of data) {
            transmitter.type=translated_type[transmitter.type]; // need for proper card translation
            var transmitter_card = create_transmitter_card(transmitter, cnt);
            cards.append(transmitter_card);
            var transmitter_body = cards.find('.card-body:eq(' + cnt + ')');
            for (var val in fields) {
                if (transmitter[val] != null) {
                    transmitter_body.append(create_card_row(fields[val], transmitter[val]));
                }
            }
            cnt++;
        }

        if (data.length > 6) {
            var show_all_text = gettext('Показать все');
            var hide_text = gettext('Скрыть');

            cards.append(`
            <div class="transmitters-cards__more-btn">
                <a id="btn-more-transmitters" type="button" data-vidible="hide">${show_all_text}</a>
            </div>
            `);

            // click 'show all' on transmitter block
            $('#btn-more-transmitters').click(function() {
                $('.hide-card').toggleClass('d-none');
                if ($(this).data('vidible') == 'hide') {
                    $(this).data('vidible', 'show');
                    $(this).text(hide_text);
                }
                else {
                    $(this).text(show_all_text);
                    $(this).data('vidible', 'hide');
                }
            });
        }
        
        var modal_links = $('.transmitters-cards [data-target-type="modal"]');
        modal_links.click(function(e) {
            var modal_text = e.target.dataset['modalCitation'];
            $('#modalCitation-content code').empty();
            $('#modalCitation-content code').append(modal_text);

            MicroModal.show('modalCitation');
        });

        // btn-copy UUID
        var btnCopyUUID = document.getElementsByClassName('btn-copy-uuid');
        for (var i = 0; i < btnCopyUUID.length; i++){
            btnCopyUUID[i].addEventListener('click', event => {
                var uuid = event.target.getAttribute('data-uuid');
                copyFunction(uuid);
            });
        }
            
    });

    create_chart(sat_id);
    $('#StationModal').on('show.bs.modal', function (event) {
        var satlink = $(event.relatedTarget);
        var modal = $(this);
        var stationId = satlink.data('stationid');
        var satId = satlink.data('satid');

        $.ajax({
            url: `/station-popup/${satId}/${stationId}`
        }).done(function (data) {
            var station = data.station;
            var satellite = data.satellite;
            var can_schedule = station['can_schedule'];
            modal.find('#StationModalTitle').text(`${stationId}-${station.info.name}`);
            modal.find('.card-title').text(station.info.name);
            modal.find('#station-link').attr('href', `/stations/${stationId}`);
            modal.find('.station-owner').attr('href', `/users/${station.info.owner}`);
            modal.find('.station-owner').text(station.info.owner);
            modal.find('.station-success-rate').text(`${station.info.success_rate.good}%`);
            modal.find('.station-antennas').empty();
            modal.find('.station-total-obs').text(station.total_obs);
            modal.find('.station-good').text(station.good_obs);
            modal.find('.station-unknown').text(station.unknown_obs);
            modal.find('.station-bad').text(station.bad_obs);
            modal.find('.station-future').text(station.future_obs);
            modal.find('#all-obs-link').attr('href', `/observations/?station=${stationId}`);
            modal.find('#good-obs-link').attr('href', `/observations/?future=0&good=1&bad=0&unknown=0&failed=0&station=${stationId}`);
            modal.find('#unknown-obs-link').attr('href', `/observations/?future=0&good=0&bad=0&unknown=1&failed=0&station=${stationId}`);
            modal.find('#bad-obs-link').attr('href', `/observations/?future=0&good=0&bad=1&unknown=0&failed=0&station=${stationId}`);
            modal.find('#future-obs-link').attr('href', `/observations/?future=1&good=0&bad=0&unknown=0&failed=0&station=${stationId}`);
            modal.find('#open-all').attr('href', `/observations/new/?norad=${satellite.norad}&ground_station=${stationId}`);

            $.each(station.info.antenna, function (i, antenna) {
                modal.find('.station-antennas').append(`
                <span class="antenna-pill" title="${human_frequency(antenna['frequency'])} - ${human_frequency(antenna['frequency_max'])}" data-placement="bottom" data-toggle="tooltip" data-original-title="
                ${human_frequency(antenna['frequency'])} - ${human_frequency(antenna['frequency_max'])}">
                ${antenna['antenna_type_name']} (${antenna.band})</span><br>`
                );
            });


            if (data.station.info.image) {
                modal.find('.modal-img-full').attr('src', data.station.info.image);
            } else {
                modal.find('.modal-img-full').attr('src', '/static/img/ground_station_no_image.png');
            }
            
            calculate_predictions(stationId, satId, can_schedule);
        });
    });
});

function create_chart(sat_id) {
    $.ajax({
        url: '/decoded_data/' + sat_id + '/'
    }).done(function(data) {
        if (data.data.length) {
            $('.decoded-data-table').prepend('<canvas id="decodedDataChart"></canvas>');
            var result = [];
            for (var r of data.data) {
                result.push({
                    'time': r.timestamp__year + '-' + r.timestamp__month + '-' + r.timestamp__day,
                    'count': r.count
                });
            }
            new Chart(
                document.getElementById('decodedDataChart'),
                {
                    type: 'bar',
                    data: {
                        labels: result.map(row => row.time),
                        datasets: [{
                            borderWidth: 2,
                            label: gettext('Количество данных'),
                            borderColor: '#4A4A4A',
                            hoverBackgroundColor: '#FE5E22',
                            hoverBorderColor: '#FE5E22',
                            legend: [{
                                display: false,
                            }],
                            data: result.map(row => row.count)
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    min: 0
                                },
                                type: 'logarithmic',
                            }]
                        },
                        plugins: {
                            legend: {
                                display: false
                            }
                        },
                        responsive: true,
                        maintainAspectRatio: false
                    }
                }
            );
            
        }
        
        else {
            var no_data_text=gettext('За последние 30 дней для спутника отсутствуют принятые данные');
            $('.decoded-data-table').prepend(`<div class="description">${no_data_text}</div>`);
        }
        
    });
}

// copy to buffer function
function copyFunction(string) {
    
    const copyText = string;
    const textArea = document.createElement('textarea');
    textArea.textContent = copyText;
    document.body.append(textArea);
    textArea.select();
    document.execCommand('copy');
    textArea.remove();
    
    var alert_text=gettext('Скопировано в буфер');
    alert(alert_text);
}

function create_transmitter_card(transmitter, index) {
    var status = 'inactive';
    var display = '';
    if (transmitter.status == 'active') {
        status = '';
    }
    if (index > 5) {
        display = 'd-none hide-card';
    }
    var copy_uuid_text= gettext('Копировать UUID');
    var info_from_satnogs_text=gettext('Информация с SatNOGS');
    var source_text=gettext('Источник');
    var card =`
    <div class="transmitters-cards__item ${display}">
        <div class="card-header ${status}">
          <strong>${transmitter.description}</strong>
          <div class="card-header-btn">
            <a class="btn-copy-uuid" data-uuid="${transmitter.uuid}"><img class="btn-copy" src="/static/img/btn-copy.svg" alt="Copy">${copy_uuid_text}</a>
            <a data-toggle="modal" data-target="#modalCitation" data-target-type="modal" data-modal-citation="${transmitter.citation}"><img class="btn-copy" src="/static/img/btn-link.svg" alt="Copy">${source_text}</a>
          </div>
        </div>
        <div class="card-body">
        </div>
        <div class="card-footer">${info_from_satnogs_text}</div>
      </div>
    `;
    return card;
}

function create_card_row(title, val) {
    var row = `
    <div class="row">
        <div class="title">${title}</div>
        <div class="description">${val}</div>
    </div>
    `;
    return row;
}

function calculate_predictions(stationId, satId, can_schedule) {
    var polar_color = localStorage.getItem('darkmode') == 'false' ? 'white' : 'black';

    // Pass predictions loading
    $('#calculate-button').prop('disabled', true);
    $('.loading-spinner').show();
    $('.pass').remove();
    $('#error-pass-predictions').remove();
    $('#prediction-results').hide();
    $('#resTable').addClass('d-none');
    $.ajax({
        url: `/pass_predictions/${stationId}/${satId}/`,
        cache: false,
        error: function () {
            var error_text=gettext('Произошла ошибка, убедитесь, что станция существует и ее местоположение определено! Если ошибка повторится, обратитесь к администратору сайта.');
            $('.table-wrapper').append(`<span id="error-pass-predictions">${error_text}<span>`);
            $('#resTable').addClass('d-none');
        },
        success: function (data) {
            var len = data.nextpasses.length - 1;
            var new_obs = '/observations/new/';
            $('.success-rate-wrapper').empty();
            var good_text=gettext('Хорошие');
            var unknown_text=gettext('Неизвестные');
            var bad_text=gettext('Плохие');
            if (len > 0) {
                $('.success-rate-wrapper').append(`
                    <div class="progress satellite-success">
                      <div class="progress-bar pb-green" style="width: ${data.nextpasses[0].success_rate}%"
                        data-toggle="tooltip" data-placement="bottom" title="${data.nextpasses[0].success_rate}% (${data.nextpasses[0].good_count}) ${good_text}">
                      </div>
                      <div class="progress-bar pb-orange" style="width: ${data.nextpasses[0].unknown_rate}%"
                        data-toggle="tooltip" data-placement="bottom" title="${data.nextpasses[0].unknown_rate}% (${data.nextpasses[0].unknown_count}) ${unknown_text}">
                      </div>
                      <div class="progress-bar pb-red" style="width: ${data.nextpasses[0].bad_rate}%"
                        data-toggle="tooltip" data-placement="bottom" title="${data.nextpasses[0].bad_rate}% (${data.nextpasses[0].bad_count}) ${bad_text}">
                      </div>
                    </div>
                `);
            }

            for (var i = 0; i <= len; i++) {
                var schedulable = data.nextpasses[i].valid && can_schedule;
                var tr = moment(data.nextpasses[i].tr).format('YYYY/MM/DD HH:mm');
                var ts = moment(data.nextpasses[i].ts).format('YYYY/MM/DD HH:mm');
                var tr_display_date = moment(data.nextpasses[i].tr).format('YYYY-MM-DD');
                var tr_display_time = moment(data.nextpasses[i].tr).format('HH:mm');
                var ts_display_date = moment(data.nextpasses[i].ts).format('YYYY-MM-DD');
                var ts_display_time = moment(data.nextpasses[i].ts).format('HH:mm');
                var tr_svg = moment.utc(data.nextpasses[i].tr).format();
                var ts_svg = moment.utc(data.nextpasses[i].ts).format();

                var overlap_style = null;
                var overlap = 0;
                if (data.nextpasses[i].overlapped) {
                    overlap = Math.round(data.nextpasses[i].overlap_ratio * 100);
                    overlap_style = 'overlap';
                }
                if (!can_schedule) {
                    $('#can_schedule').addClass('d-none');
                }
                var AOS_text=gettext('Градусы восхода');
                var TCA_text=gettext('Градусы наибольшего сближения');
                var LOS_text=gettext('Градусы захода');
                var north_text=gettext('С');
                var south_text=gettext('Ю');
                var east_text=gettext('В');
                var west_text=gettext('З');
                var schedule_text=gettext('Запланировать');
                var overlap_span=gettext('перекрытие');
                var overlap_text=gettext('Наблюдение перекрывается');
                $('#pass-predictions-results').append(`
                  <tr class="pass ${overlap_style}" data-overlap="${overlap}">
                    <td class="pass-datetime">
                      <span class="pass-time">${tr_display_time}</span><br>
                      <span class="pass-date">${tr_display_date}</span>
                    </td>
                    <td class="pass-datetime">
                      <span class="pass-time">${ts_display_time}</span><br>
                      <span class="pass-date">${ts_display_date}</span>
                    </td>
                    <td class="max-elevation" data-max="${data.nextpasses[i].altt}">
                      <span class="polar-deg" aria-hidden="true"
                            data-toggle="tooltip" data-placement="left"
                            title="${AOS_text}">
                          <span class="text-green">⤉</span> ${data.nextpasses[i].azr}°
                      </span>
                      <span class="polar-deg" aria-hidden="true"
                            data-toggle="tooltip" data-placement="left"
                            title="${TCA_text}">
                          <span class="text-orange">⇴</span> ${data.nextpasses[i].altt}°
                      </span>
                      <span class="polar-deg" aria-hidden="true"
                            data-toggle="tooltip" data-placement="left"
                            title="${LOS_text}">
                          <span class="text-red">⤈</span> ${data.nextpasses[i].azs}°
                      </span>
                    </td>
                    <td>
                      <div class="polar_plot">
                        <svg
                            xmlns="http://www.w3.org/2000/svg" version="1.1"
                            id="polar${i}"
                            data-tle1="${data.nextpasses[i].tle1}"
                            data-tle2="${data.nextpasses[i].tle2}"
                            data-timeframe-start="${tr_svg}"
                            data-timeframe-end="${ts_svg}"
                            data-groundstation-lat="${data.ground_station.lat}"
                            data-groundstation-lon="${data.ground_station.lng}"
                            data-groundstation-alt="${data.ground_station.alt}"
                            width="100px" height="100px"
                            viewBox="-110 -110 220 220"
                            overflow="hidden">
                            <path
                                fill="none" stroke=${polar_color} stroke-width="1"
                                d="M 0 -95 v 190 M -95 0 h 190"
                                />
                            <circle
                                fill="none" stroke=${polar_color}
                                cx="0" cy="0" r="30"
                                />
                            <circle
                                fill="none" stroke=${polar_color}
                                cx="0" cy="0" r="60"
                                />
                            <circle
                                fill="none" stroke=${polar_color}
                                cx="0" cy="0" r="90"
                                />
                            <text x="-4" y="-96" fill=${polar_color}>
                                ${north_text}
                            </text>
                            <text x="-4" y="105" fill=${polar_color}>
                                ${south_text}
                            </text>
                            <text x="96" y="4" fill=${polar_color}>
                                ${east_text}
                            </text>
                            <text x="-106" y="4" fill=${polar_color}>
                                ${west_text}
                            </text>
                        </svg>
                      </div>
                    </td>
                    ${can_schedule ? `
                      <td class="pass-schedule">
                          ${schedulable ? `
                            <a href="${new_obs}?norad=${data.nextpasses[i].norad_cat_id}&ground_station=${stationId}&start=${tr}&end=${ts}"
                              class="btn btn-sm btn-outline-blue schedulable"
                              target="_blank">
                              ${schedule_text}
                              ${overlap ? `
                                <br>
                                <div class="badget badget-sm badget-red" aria-hidden="true" data-toggle="tooltip"
                                      title="${overlap_text}">
                                  ${overlap}% ${overlap_span}
                                </div>
                              ` : `
                              `}
                            </a>
                          ` : `
                            <a class="btn btn-sm btn-outline-blue disabled">
                              ${schedule_text}
                              ${overlap ? `
                                <br>
                                <div class="badget badget-sm badget-red" aria-hidden="true" data-toggle="tooltip"
                                      title="${overlap_text}">
                                  ${overlap}% ${overlap_span}
                                </div>
                              ` : `
                              `}
                            </a>
                          `}
                      </td>
                    ` : `
                    `}
                  </tr>
                `);

                // Draw orbit in polar plot
                var tleLine1 = $('svg#polar' + i.toString()).data('tle1');
                var tleLine2 = $('svg#polar' + i.toString()).data('tle2');

                var timeframe = {
                    start: new Date($('svg#polar' + i.toString()).data('timeframe-start')),
                    end: new Date($('svg#polar' + i.toString()).data('timeframe-end'))
                };

                var groundstation = {
                    lon: $('svg#polar' + i.toString()).data('groundstation-lon'),
                    lat: $('svg#polar' + i.toString()).data('groundstation-lat'),
                    alt: $('svg#polar' + i.toString()).data('groundstation-alt')
                };

                const polarPlotSVG = calcPolarPlotSVG(timeframe,
                    groundstation,
                    tleLine1,
                    tleLine2);

                $('svg#polar' + i.toString()).append(polarPlotSVG);
                $('svg#polar' + i.toString() + ' path:last').css('stroke', '#0097ff');
            }

            // Show predicion results count
            $('#prediction-results').show();
            $('#prediction-results-count').html(data.nextpasses.length);
            $('#resTable').removeClass('d-none');
        },
        complete: function () {
            $('.loading-spinner').hide();
            $('#calculate-button').prop('disabled', false);
        }
    });
}

Fancybox.bind('[data-fancybox]', {
    Toolbar: {
        display: {
            right: ['close'],
        },
    },
    l10n: {
        CLOSE: gettext('Закрыть'),
        NEXT: gettext('Следующий'),
        PREV: gettext('Предыдущий'),
        MODAL: gettext('Вы можете закрыть окно нажатием ESC'),
        ERROR: gettext('Ошибка, попробуйте повторить попытку'),
        IMAGE_ERROR: gettext('Изображение не найдено'),
        ELEMENT_NOT_FOUND: gettext('HTML элемент не найден'),
        AJAX_NOT_FOUND: gettext('Ошибка загрузки AJAX: Не найдено'),
        AJAX_FORBIDDEN: gettext('Ошибка загрузки AJAX: Запрещено'),
        IFRAME_ERROR: gettext('Ошибка при загрузке фрейма'),
        TOGGLE_ZOOM: gettext('Приблизить'),
        TOGGLE_THUMBS: gettext('Миниатюры'),
        TOGGLE_SLIDESHOW: gettext('Слайдшоу'),
        TOGGLE_FULLSCREEN: gettext('Полноэкранный режим'),
        DOWNLOAD: gettext('Загрузить')
    }
});