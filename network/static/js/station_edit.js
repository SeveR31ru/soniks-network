/*global human_frequency, bands_from_range,gettext */

/*eslint no-control-regex: 0*/
$(document).ready(function () {
    'use strict';
    $('#antennas-loading').toggle();
    $('.selectpicker').selectpicker();

    $( '.accordion' ).accordion({
        collapsible: true,
        heightStyle: 'content'
    });

    $('.card-body.collapse').on('hide.bs.collapse', function () {
        $(this).prev().find('i').addClass('bi-arrows-expand').removeClass('bi-arrows-collapse');
    });

    $('.card-body.collapse').on('show.bs.collapse', function () {
        $(this).prev().find('i').addClass('bi-arrows-collapse').removeClass('bi-arrows-expand');
    });

    // Parse and initialize station data and remove html elements that holding them
    var station_element = $('#station-data-to-parse');
    var station_data = station_element.data();
    const max_frequency_ranges = station_data.max_frequency_ranges_per_antenna;
    const max_antennas = station_data.max_antennas_per_station;
    const maximum_frequency = station_data.max_frequency_for_range;
    const minimum_frequency = station_data.min_frequency_for_range;
    const vhf_max = station_data.vhf_max_frequency;
    const vhf_min = station_data.vhf_min_frequency;
    const uhf_max = station_data.uhf_max_frequency;
    const uhf_min = station_data.uhf_min_frequency;
    const s_max = station_data.s_max_frequency;
    const s_min = station_data.s_min_frequency;
    const l_max = station_data.l_max_frequency;
    const l_min = station_data.l_min_frequency;
    station_element.remove();

    // Parse and initialize antenna data and remove html elements that holidng them
    var current_antenna = {};
    var current_order = -1;
    var antenna_element = $('#antennas-data-to-parse');
    var antennas = [];

    function get_band_range(frequency_data) {
        let range = {
            'min': frequency_data.min,
            'human_min': human_frequency(frequency_data.min),
            'max': frequency_data.max,
            'human_max': human_frequency(frequency_data.max),
            'bands': bands_from_range(frequency_data.min, frequency_data.max),
            'initial': Object.prototype.hasOwnProperty.call(frequency_data, 'id'),
            'deleted': Object.prototype.hasOwnProperty.call(frequency_data, 'deleted')
        };
        if (range.initial) {
            range.id = frequency_data.id;
        }
        return range;
    }

    antenna_element.children().each(function () {
        var antenna_data = $(this).data();
        var frequency_ranges = [];
        $(this).children().each(function () {
            var frequency_data = $(this).data();
            frequency_ranges.push(
                get_band_range(frequency_data)
            );
        });
        let antenna = {
            'type_name': antenna_data.typeName,
            'type_id': antenna_data.typeId,
            'initial': Object.prototype.hasOwnProperty.call(antenna_data, 'id'),
            'deleted': Object.prototype.hasOwnProperty.call(antenna_data, 'deleted'),
            'frequency_ranges': frequency_ranges
        };
        if (antenna.initial) {
            antenna.id = antenna_data.id;
        }
        antennas.push(antenna);
    });
    antenna_element.remove();


    // Functions to create and update antenna cards
    function create_antenna_card(antenna, order) {
        var freq_range_text=gettext('Частотные диапазоны:');
        var add_range_text=gettext('Добавить частотный диапазон');
        var frequency_ranges_elements = '';
        for (let range of antenna.frequency_ranges) {
            if (!range.deleted) {
                frequency_ranges_elements += '<div>' + range.human_min + ' - ' + range.human_max + ' (' + range.bands + ')</div>';
            }
        }
        var add_frequency_ranges_button = `<button type="button" data-action="edit" data-order="${order}" class="btn btn-sm btn-blue" data-toggle="modal" data-target="#modal">
                                             <span class="bi bi-plus" aria-hidden="true"></span>
                                             ${add_range_text}
                                           </button>`;
        frequency_ranges_elements = frequency_ranges_elements || add_frequency_ranges_button;
        return `<div class="card" id="${order}">
                  <div class="card-header">
                    <div class="row justify-content-between align-items-center">
                      <div class="antenna-label">${antenna.type_name}</div>
                      <button type="button" data-action="edit" data-order="${order}" data-toggle="modal" data-target="#modal" class="btn btn-sm btn-outline-blue">
                          <span class="bi bi-pencil-square" aria-hidden="true"></span>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="antenna-label">${freq_range_text}</div>
                        <div class="frequency-ranges">${frequency_ranges_elements}
                        </div>
                    </div>
                </div>`;
    }
    function update_antennas_cards() {
        $('#antennas-loading').show();
        $('#antennas-card-body').addClass('d-none');
        $('#antennas-card-group .card').remove();
        let antennas_cards = '';
        let deleted = 0;
        antennas.forEach(function (antenna, order) {
            if (!antenna.deleted) {
                antennas_cards += create_antenna_card(antenna, order);
            } else {
                deleted++;
            }
        });
        $('#new-antenna').toggle(
            (antennas.length - deleted) < max_antennas
        );
        $('#antennas-card-group').prepend(antennas_cards);
        $('#antennas-loading').hide();
        $('#antennas-card-body').removeClass('d-none');
    }

    // Create initial antenna bootstrap cards if antennas exist
    update_antennas_cards();

    // Form fields validations
    function update_text_input_and_validation(text_input, text, valid) {
        text_input.val(text);
        text_input.toggleClass('is-valid', valid);
        text_input.toggleClass('is-invalid', !valid);
        text_input.parent().toggleClass('is-valid', valid);
        text_input.parent().toggleClass('is-invalid', !valid);
    }

    function check_validity_of_frequencies() {
        $('button[data-action=save]').prop('disabled', false);
        let valid = true;
        var error_1 = gettext('Некорректное минимальное или максимальное значение');
        var error_2 = gettext('Максимальное значение больше минимального');
        var error_3 = gettext('Минимальное или максимальное значение вне допустимого диапазона');
        var error_4 = gettext('Диапазон поддиапазон другого диапазона');
        var error_5 = gettext('Диапазон наддиапазон другого диапазона');
        var error_6 = gettext('Диапазон конфликтует с другим диапазоном');

        for (let order in current_antenna.frequency_ranges) {
            let valid_range = true;
            let range = current_antenna.frequency_ranges[order];

            if (range.deleted) {
                continue;
            }

            let min = parseInt(range.min);
            let max = parseInt(range.max);
            let text_input = $('#' + order + '-range-text');

            if (isNaN(min) || isNaN(max)) {
                valid_range = false;
                update_text_input_and_validation(text_input, error_1, false);
            } else if (max < min) {
                valid_range = false;
                update_text_input_and_validation(text_input, error_2, false);
            } else if (min < minimum_frequency || max > maximum_frequency) {
                valid_range = false;
                update_text_input_and_validation(text_input, error_3, false);
            }
            else {
                for (let index in current_antenna.frequency_ranges) {
                    let index_range = current_antenna.frequency_ranges[index];
                    let index_min = parseInt(index_range.min);
                    let index_max = parseInt(index_range.max);
                    if (index_range.deleted || index === order || isNaN(index_min) || isNaN(index_max)) {
                        continue;
                    }
                    if (index_min < min && index_max > max) {
                        valid_range = false;
                        update_text_input_and_validation(text_input, error_4, false);
                        break;
                    } else if (index_min > min && index_max < max) {
                        valid_range = false;
                        update_text_input_and_validation(text_input, error_5, false);
                        break;
                    } else if (!(index_min > max || index_max < min)) {
                        valid_range = false;
                        update_text_input_and_validation(text_input, error_6, false);
                        break;
                    }
                }
            }

            if (valid_range) {
                range.human_min = human_frequency(range.min);
                range.human_max = human_frequency(range.max);
                range.bands = bands_from_range(range.min, range.max);
                update_text_input_and_validation(text_input, range.human_min + '-' + range.human_max + '(' + range.bands + ')', true);
            } else {
                valid = false;
            }
        }

        if (!valid) {
            $('button[data-action=save]').prop('disabled', true);
        }
    }

    function check_validity_of_input(element) {
        let input = $(element);
        let valid = element.checkValidity();
        $('#submit').prop('disabled', !$('form')[0].checkValidity());
        input.toggleClass('is-valid', valid);
        input.toggleClass('is-invalid', !valid);
        input.parents('label').toggleClass('is-invalid', !valid);
        if (input.parents('.card-body').find('.is-invalid').length) {
            input.parents('.card-body').prev().addClass('is-invalid');
        }
        else {
            input.parents('.card-body').prev().removeClass('is-invalid');
        }
    }

    $('input, textarea').each(function () {
        if (!$(this).hasClass('frequency')) {
            check_validity_of_input(this);
        }
    });

    // Events related to validation
    $('body').on('input', function (e) {
        let input = $(e.target);
        let value = input.val();
        let order = input.data('order');
        let field = input.data('field');
        if (input.hasClass('frequency')) {
            current_antenna.frequency_ranges[order][field] = value;
            check_validity_of_frequencies();
        } else {
            check_validity_of_input(e.target);
        }
    });

    // Functions to initialize modal
    function band_ranges(band) {
        switch (band) {
        case 'VHF':
            return get_band_range({ 'min': vhf_min, 'max': vhf_max });
        case 'UHF':
            return get_band_range({ 'min': uhf_min, 'max': uhf_max });
        case 'L':
            return get_band_range({ 'min': l_min, 'max': l_max });
        case 'S':
            return get_band_range({ 'min': s_min, 'max': s_max });
        default:
            return get_band_range({ 'min': minimum_frequency, 'max': maximum_frequency });
        }
    }

    function create_frequency_range_card(range, order) {
        var min_text = gettext('Минимум');
        var max_text = gettext('Максимум');
        var min_placeholder = gettext('Минимальная частота');
        var max_placeholder = gettext('Максимальная частота');
        var delete_text = gettext('Удалить диапазон частот');

        return `<div class="card">
                  <div data-order="${order}" class="row no-gutters justify-content-between align-items-center frequency-range-fields">
                    <div class="col form-group">
                      <label>${min_text}
                        <div>
                          <input data-order="${order}" data-field="min" value="${range.min}" id="${order}-min" type="number" min="${minimum_frequency}" max="${maximum_frequency}" class="frequency w-100" placeholder="${min_placeholder}">
                        </div>
                      </label>
                    </div>
                    <div class="col form-group">
                      <label>${max_text}
                        <div>
                          <input data-order="${order}" data-field="max" value="${range.max }" id="${order}-max" type="number" min="${minimum_frequency}" max="${maximum_frequency}" class="frequency w-100" placeholder="${max_placeholder}">
                        </div>
                      </label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col form-group">
                      <input id="${order}-range-text" readonly="" type="text" value="${range.human_min} - ${range.human_max} (${range.bands})" class="w-100 range-text">
                    </div>
                  </div>
                  <button class="btn btn-sm btn-without-border-red remove-range" type="button" data-order="${order}" aria-label="${delete_text}">
                    <span class="bi bi-x"></span>
                  </button>
                </div>`;
    }

    function update_frequency_ranges_cards() {
        $('#frequency-ranges-loading').show();
        $('#frequency-ranges').hide();
        $('#frequency-ranges .card').remove();
        let frequency_ranges_cards = '';
        let deleted = 0;
        current_antenna.frequency_ranges.forEach(function (frequency_range, order) {
            if (!frequency_range.deleted) {
                frequency_ranges_cards += create_frequency_range_card(frequency_range, order);
            } else {
                deleted++;
            }
        });
        $('#new-ranges').toggle(
            (current_antenna.frequency_ranges.length - deleted) < max_frequency_ranges
        );
        if ((current_antenna.frequency_ranges.length - deleted) == 0) {
            frequency_ranges_cards = gettext('Добавьте диапазон частот, выбрав один из предложенных, или создайте свой ниже');
            
        }
        $('#frequency-ranges').html(frequency_ranges_cards);
        check_validity_of_frequencies();
        $('#frequency-ranges-loading').hide();
        $('#frequency-ranges').show();
    }

    // Events related to modal
    $('#antenna-type').on('changed.bs.select', function (e, clickedIndex, isSelected) {
        if (isSelected !== null) {
            let value = e.target.value;
            current_antenna.type_name = $('#antenna-type option[value=' + value + ']').data('content');
            current_antenna.type_id = value;
        }
    });

    $('#frequency-ranges').on('click', '.remove-range', function (e) {
        let order = $(e.currentTarget).data('order');
        if (current_antenna.frequency_ranges[order].initial) {
            current_antenna.frequency_ranges[order].deleted = true;
        } else {
            current_antenna.frequency_ranges.splice(order, 1);
        }
        update_frequency_ranges_cards();
    });

    $('.new-range').on('click', function () {
        let range = band_ranges($(this).data('range'));
        current_antenna.frequency_ranges.push(range);
        update_frequency_ranges_cards();
    });

    $('#modal').on('show.bs.modal', function (e) {
        $('#submit').prop('disabled', true); // Disable submit button
        let action = $(e.relatedTarget).data('action');
        if (action == 'edit') {
            $('#AntennaModalTitle').text(gettext('Изменить антенну'));
            let order = $(e.relatedTarget).data('order');
            current_antenna = $.extend(true, {}, antennas[order]);
            current_order = order;
            $('#antenna-type').selectpicker('val', antennas[order].type_id);
            $('#delete-antenna').show();

        } else if (action == 'new') {
            $('#AntennaModalTitle').text(gettext('Новая антенна'));
            
            let value = $('#antenna-type').children(':first').val();
            current_antenna = { 'type_name': $('#antenna-type option[value=' + value + ']').data('content'), 'type_id': value, 'initial': false, 'deleted': false, 'frequency_ranges': [] };
            current_order = -1;
            $('#antenna-type').selectpicker('val', value);
            $('#delete-antenna').hide();
        }
        update_frequency_ranges_cards();
    });

    $('#modal').on('click', '.modal-action', function (e) {
        let action = $(e.currentTarget).data('action');
        let order = current_order;
        if (action == 'save') {
            let to_save_antenna = $.extend(true, {}, current_antenna);
            to_save_antenna.frequency_ranges.forEach(function (range) {
                range.human_min = human_frequency(range.min);
                range.human_max = human_frequency(range.max);
                range.bands = bands_from_range(range.min, range.max);
            });
            if (current_order >= 0) {
                antennas[order] = to_save_antenna;
            } else {
                antennas.push(to_save_antenna);
            }
        } else if (action == 'delete') {
            if (antennas[order].initial) {
                antennas[order].deleted = true;
            } else {
                antennas.splice(order, 1);
            }
        }
        update_antennas_cards();
        $(e.delegateTarget).modal('hide');
    });

    $('#modal').on('hidden.bs.modal', function () {
        let value = $('#antenna-type').first().val();
        current_antenna = { 'type_name': $('#antenna-type option[value=' + value + ']').data('content'), 'type_id': value, 'initial': false, 'deleted': false, 'frequency_ranges': [] };
        var add_freg_range_text=gettext('Добавьте диапазон частот, выбрав один из предложенных, или создайте свой ниже');
        $('#antenna-type').selectpicker('val', value);
        $('#frequency-ranges').html(add_freg_range_text);
        $('#frequency-ranges').show();
        $('#delete-antenna').hide();
        $('#submit').prop('disabled', false); // Enable submit button
    });

    // Initialize Station form elements
    var horizon_value = $('#horizon').val();
    $('#horizon').slider({
        id: 'horizon_value',
        min: 0,
        max: 90,
        step: 1,
        value: horizon_value
    });

    var utilization_value = $('#utilization').val();
    $('#utilization').slider({
        id: 'utilization_value',
        min: 0,
        max: 100,
        step: 1,
        value: utilization_value
    });

    var image_exists = Object.prototype.hasOwnProperty.call($('#station-image').data(), 'existing');
    var send_remove_file = false;
    if (image_exists) {
        $('#station-image').fileinput({
            browseLabel : '',
            removeLabel: '',
            dropZoneTitle: gettext('Перетащите изображение сюда...'),
            msgProcessing: gettext('Обработка...'),
            msgSelected: gettext('Файл выбран'),
            msgPlaceholder: gettext('Выберите файл...'),
            showRemove: true,
            showUpload: false,
            showClose: false,
            initialPreview: $('#station-image').data('existing'),
            initialPreviewAsData: true,
            initialPreviewShowDelete: false,
            allowedFileTypes: ['image'],
            autoOrientImage: false,
            fileActionSettings: {
                showDownload: false,
                showRemove: false,
                showZoom: true,
                showDrag: false,
            }
        });
    } else {
        $('#station-image').fileinput({
            browseLabel : '',
            removeLabel: '',
            dropZoneTitle: gettext('Перетащите изображение сюда...'),
            msgProcessing: gettext('Обработка...'),
            msgSelected: gettext('Файл выбран'),
            msgPlaceholder: gettext('Выберите файл...'),
            showRemove: true,
            showUpload: false,
            showClose: false,
            allowedFileTypes: ['image'],
            autoOrientImage: false,
            fileActionSettings: {
                showDownload: false,
                showRemove: false,
                showZoom: true,
                showDrag: false,
            }
        });
    }

    $('#satnogs-rx-samp-rate').on('change', function () {
        if (this.value) {
            this.value = Number(this.value);
        }
    });

    $('#station-image').on('change', function () {
        send_remove_file = false;
    });

    $('#station-image').on('fileclear', function () {
        send_remove_file = image_exists;
    });

    $('#violator_scheduling').change(function () {
        $('.violator-scheduling-option-description').hide();
        $('#violator-scheduling-' + this.value).show();
    });
    $('#violator_scheduling').change();

    // Submit or Cancel form
    $('#cancel').on('click', function () {
        if ($(this).data('back') == 'station') {
            location.href = location.href.replace('edit/', '');
        } else {
            location.href = location.origin + '/users/redirect/';
        }
    });

    $('form').on('submit', function (event) {
        $('#antenna-type').remove();
        let antennas_total = 0;
        let antennas_initial = 0;
        let form = $('form');
        // Prepare station form
        if (send_remove_file) {
            form.append('<input type="checkbox" name="image-clear" style="display: none" checked>');
        }
        // Prepare antennas forms
        antennas.forEach(function (antenna, order) {
            antennas_total++;
            let antenna_prefix = 'ant-' + order;
            if (antenna.deleted) {
                form.append('<input type="checkbox" name="' + antenna_prefix + '-DELETE" style="display: none" checked>');
            }
            if (antenna.initial) {
                antennas_initial++;
                form.append('<input type="hidden" name="' + antenna_prefix + '-id" value="' + antenna.id + '">');
            }
            form.append('<input type="hidden" name="' + antenna_prefix + '-antenna_type" value="' + antenna.type_id + '">');

            //Prepare frequency ranges forms
            let frequency_ranges_total = 0;
            let frequency_ranges_initial = 0;
            antenna.frequency_ranges.forEach(function (range, range_order) {
                frequency_ranges_total++;
                let range_prefix = antenna_prefix + '-fr-' + range_order;
                if (range.deleted) {
                    form.append('<input type="checkbox" name="' + range_prefix + '-DELETE" style="display: none" checked>');
                }
                if (range.initial) {
                    frequency_ranges_initial++;
                    form.append('<input type="hidden" name="' + range_prefix + '-id" value="' + range.id + '">');
                }
                form.append('<input type="hidden" name="' + range_prefix + '-min_frequency" value="' + range.min + '">');
                form.append('<input type="hidden" name="' + range_prefix + '-max_frequency" value="' + range.max + '">');
            });
            form.append('<input type="hidden" name="' + antenna_prefix + '-fr-TOTAL_FORMS" value="' + frequency_ranges_total + '">');
            form.append('<input type="hidden" name="' + antenna_prefix + '-fr-INITIAL_FORMS" value="' + frequency_ranges_initial + '">');
            form.append('<input type="hidden" name="' + antenna_prefix + '-fr-MAX_NUM_FORMS" value="' + max_frequency_ranges + '">');
        });
        form.append('<input type="hidden" name="ant-TOTAL_FORMS" value="' + antennas_total + '">');
        form.append('<input type="hidden" name="ant-INITIAL_FORMS" value="' + antennas_initial + '">');
        form.append('<input type="hidden" name="ant-MAX_NUM_FORMS" value="' + max_antennas + '">');
        //name check
        var url = '/user_station_name_dublicate/';
        var data ={};
        data.future_name=$('#station-name').val();
        data.station_id=$('#station').data('station-id');
        $.ajax({
            url: url,
            data: data,
            async: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function (results) {
            if (results.check==false){
                var text = gettext('У вас уже имеется станция с таким именем. Вы хотите добавить ещё одну станцию с таким же именем?');
                if(!confirm(text)) {event.preventDefault();}
            }
        });
    });

    if (window.innerWidth < 576) {
        $('#delete-antenna').text('Удалить');
        $('.close-btns [data-action=save]').text(gettext('Сохранить'));
    }
});
