/*global Chart, gettext*/

$(document).ready(function () {
    'use strict';

    var borderColor = '#232323';
    var theme = localStorage.getItem('darkmode');
    if (theme == 'true') {
        borderColor = '#d7d7d7';
    }

    var partData = $('.part-data');
    for (var el of partData) {
        var all_data = el.dataset.full;
        el.innerHTML += ` (${Math.floor(100 / all_data * el.innerHTML)}%)`;
    }
    $('.loading-spinner').show();


    Chart.pluginService.register({
        afterUpdate: function (chart) {
            if (chart.config.options.elements.center) {
                var helpers = Chart.helpers;
                var centerConfig = chart.config.options.elements.center;
                var globalConfig = Chart.defaults.global;
                var ctx = chart.chart.ctx;

                var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
                var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

                var fontSize;

                if (centerConfig.fontSize) {
                    fontSize = centerConfig.fontSize;
                }
                // figure out the best font size, if one is not specified
                else {
                    ctx.save();
                    fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
                    var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
                    var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

                    var breakage = true;
                    do {
                        ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
                        var textWidth = ctx.measureText(maxText).width;

                        // check if it fits, is within configured limits and that we are not simply toggling back and forth
                        if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize) {
                            fontSize += 1;
                        }
                        else {
                            // reverse last step
                            fontSize -= 1;
                            breakage = false;
                        }
                    } while (breakage);
                    ctx.restore();
                }

                // save properties
                chart.center = {
                    font: helpers.fontString(fontSize, fontStyle, fontFamily),
                    fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
                };
            }
        },
        afterDraw: function (chart) {
            if (chart.center) {
                var centerConfig = chart.config.options.elements.center;
                var ctx = chart.chart.ctx;

                ctx.save();
                ctx.font = chart.center.font;
                ctx.fillStyle = chart.center.fillStyle;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
                ctx.fillText(centerConfig.text, centerX, centerY);
                ctx.restore();
            }
        },
    });

    var fail_text = gettext('В процессе загрузки данных произошла ошибка.');

    $.ajax({
        url: 'top/satellites'
    }).done(function (data) {
        select_show_table_sat(data);

        $('.period-select option').click(function() {
            select_show_table_sat(data);
        });

        $('#satellites_top .top-btn').click(function () {
            var currentBtn = $(this);
            var otherBtn;
            if (currentBtn.val() == 'sonik') {
                otherBtn = currentBtn.next();
            }
            else {
                otherBtn = currentBtn.prev();
            }

            otherBtn.removeClass('focus-btn');
            otherBtn.addClass('nonfocus-btn');
            currentBtn.removeClass('nonfocus-btn');
            currentBtn.addClass('focus-btn');
            select_show_table_sat(data);
        });
    }).fail(function() {
        $('#top-satellites card-body').append(`<p>${fail_text}//p>`);
    }).always(function() {
        $('#satellites-spinner').hide();
    });

    $.ajax({
        url: 'top/stations'
    }).done(function (data) {
        select_show_table_station(data);

        $('.period-select-station option').click(function() {
            select_show_table_station(data);
        });

        $('#stations_top .top-btn').click(function () {
            var currentBtn = $(this);
            var otherBtn;
            if (currentBtn.val() == 'sonik') {
                otherBtn = currentBtn.next();
            }
            else {
                otherBtn = currentBtn.prev();
            }

            otherBtn.removeClass('focus-btn');
            otherBtn.addClass('nonfocus-btn');
            currentBtn.removeClass('nonfocus-btn');
            currentBtn.addClass('focus-btn');
            select_show_table_station(data);
        });
    }).fail(function() {
        $('#top-stations card-body').append(fail_text);
    }).always(function() {
        $('#stations-spinner').hide();
    });

    $.ajax({
        url: 'modes'
    }).done(function (data) {
        var i;
        var h;
        var s;
        var l;
        var color;
        // Create colors for Mode Chart
        var mode_colors = [];
        var mode_labels = [];
        var mode_data = [];
        for (i = 0; i < data.modes.length; i++) {
            mode_labels.push(data.modes[i].name);
            mode_data.push(data.modes[i].count);
        }
        for (i = 0; i < data.modes.length; i++) {
            // Switching to HSL to stick with hue of LSF logo
            h = 16;
            s = 100;
            if (theme == 'true') {
                l = 40 + i*5;
                if (l > 90) {
                    l = 90;
                }
            }
            else {
                l = 60 - i*5;
                if (l < 10) {
                    l = 10;
                }
            }
            color = 'hsl(' + h + ',' + Math.floor(s) + '%,' + Math.floor(l) + '%)';
            mode_colors.push(color);
        }

        var band_colors = [];
        for (i = 0; i < data.band_label.length; i++) {
            h = 204;
            s = 100;
            if (theme == 'true') {
                l = 40 + i*10;
                if (l > 90) {
                    l = 90;
                }
            }
            else {
                l = 60 - i*10;
                if (l < 10) {
                    l = 10;
                }
            }
            color = 'hsl(' + h + ',' + s + '%,' + l + '%)';
            band_colors.push(color);
        }

        Chart.defaults.global.legend.display = false;
        Chart.defaults.global.title.display = false;

        var mode_text = gettext(' Модуляций');
        var mode_c = document.getElementById('modes');
        var mode_chart = new Chart(mode_c, {
            type: 'doughnut',
            data: {
                labels: mode_labels,
                datasets: [{
                    backgroundColor: mode_colors,
                    data: mode_data,
                    borderWidth: 1,
                    borderColor: borderColor
                }]
            },
            options: {
                elements: {
                    center: {
                        // the longest text that could appear in the center
                        maxText: '100%',
                        text: data.modes.length + mode_text,
                        fontColor: '#0097ff',
                        fontFamily: 'Inter',
                        fontStyle: 'normal',
                        minFontSize: 1,
                        maxFontSize: 20,
                    }
                },
                legend: false,
                legendCallback: function (chart) {
                    var legendHtml = [];
                    var item = chart.data.datasets[0];
                    for (var i = 0; i < item.data.length; i++) {
                        if (item.data[i] > 7) {
                            legendHtml.push('<div class="row">');
                            legendHtml.push('<div class="col-4 mod-label">' + chart.data.labels[i] + '</div>');
                            legendHtml.push('<div class="col-4 mod-count">' + item.data[i] + '</div>');
                            legendHtml.push('</div>');
                        }
                    }
                    return legendHtml.join('');
                }
            }
        });
        $('#modes-labels').html(mode_chart.generateLegend());

        var band_text = gettext(' Диапазонов');
        var band_c = document.getElementById('bands');
        var band_chart = new Chart(band_c, {
            type: 'doughnut',
            data: {
                labels: data.band_label,
                datasets: [{
                    backgroundColor: band_colors,
                    data: data.band_count,
                    borderWidth: 1,
                    borderColor: borderColor
                }]
            },
            options: {
                elements: {
                    center: {
                        // the longest text that could appear in the center
                        maxText: '100%',
                        text: data.band_count.length + band_text,
                        fontColor: '#0097ff',
                        fontFamily: 'Inter',
                        fontStyle: 'normal',
                        minFontSize: 1,
                        maxFontSize: 20,
                    }
                },
                legend: false,
                legendCallback: function (chart) {
                    var legendHtml = [];
                    var item = chart.data.datasets[0];
                    for (var i = 0; i < item.data.length; i++) {
                        if (item.data[i] >= 0) {
                            legendHtml.push('<div class="row">');
                            legendHtml.push('<div class="col-4 band-label">' + chart.data.labels[i] + '</div>');
                            legendHtml.push('<div class="col-4 band-count">' + item.data[i] + '</div>');
                            legendHtml.push('</div>');
                        }
                    }
                    return legendHtml.join('');
                }
            }
        });
        $('#bands-labels').html(band_chart.generateLegend());
    });
});

function select_show_table_station(data) {
    var option_val = $('#stations_top .period-select-station option:selected').val();
    var btn_val = $('#stations_top .focus-btn').val();

    if (btn_val == 'sonik') {
        createTable($('#top-stations tbody'), data['top_station_sonik' + option_val]);
    }
    else {
        var sonik_array = data['top_station_sonik' + option_val];
        var not_sonik_array = data['top_station_not_sonik' + option_val];
        var result_data = [];
        var l = 0;
        var r = 0;
        for (var k=0; k<Math.max(sonik_array.length, not_sonik_array.length); k++) {
            if (not_sonik_array[r]) {
                if (sonik_array[l]) {
                    if (sonik_array[l].count >= not_sonik_array[r].count) {
                        result_data.push(sonik_array[l]);
                        l++;
                    }
                    else {
                        result_data.push(not_sonik_array[r]);
                        r++;
                    }
                }
                else {
                    result_data.push(not_sonik_array[r]);
                    r++;
                }
            }
            else {
                result_data.push(sonik_array[l]);
                l++;
            }
        }
        createTable($('#top-stations tbody'), result_data);
    }
}

function select_show_table_sat(data) {
    var option_val = $('#satellites_top .period-select option:selected').val();
    var btn_val = $('#satellites_top .focus-btn').val();

    if (btn_val == 'sonik') {
        createTable($('#top-satellites tbody'), data['top_sat_sonik' + option_val]);
    }
    else {
        createTable($('#top-satellites tbody'), data['top_sat_total' + option_val]);
    }
}

function createTable(el, data) {
    el.empty();
    for (var row of data) {
        var link;
        var td_name;
        if (row.sat_id) {
            link = '/satellites/' + row.sat_id;
        }
        else {
            link = '/stations/' + row.id;
        }
        if (row.name) {
            td_name = `<a href="${link}" target="_blank">${row.name}</a>`;
        }
        else {
            td_name = row.observer;
        }
        el.append(`
        <tr>
            <td>${td_name}</td>
            <td>${row.count}</td>
        </tr>
        `);
    }
}
