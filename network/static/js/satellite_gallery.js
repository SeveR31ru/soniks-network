/* global gettext Fancybox */
$(document).ready(function () {
    $('.loading-spinner').show();
    var cards = $('.image-card');
    cards.each(function() {
        var images = $(this).find('img');
        images.each(function() {
            $(this).on('error', function() {
                $(this).parent().append('<span class="bi bi-image"></span>');
                $(this).remove();
            });
            $(this).on('load', function() {
                $(this).parent().parent().find('.loading-spinner').hide();
            });
            if (this.complete) {
                if (this.naturalWidth == 0) {
                    $(this).parent().append('<span class="bi bi-image"></span>');
                    $(this).remove();
                }
                else {
                    $(this).parent().parent().find('.loading-spinner').hide();
                }
            }
        });
    });

    $('.status-filter').on('click', group_image);
});

function group_image(e) {
    var input_val = $(e.currentTarget).prop('checked');
    var cards = $('.image-card');
    if (input_val) {
        cards.each(function() {
            var image_count = 1;
            var obs_id = $(this).find('.title-link').text();
            var next_card = $(this).next();
            while (next_card.find('.title-link').text() == obs_id) {
                image_count++;
                var image = next_card.find('img').clone();
                next_card.remove();
                next_card = $(this).next();
                $(this).find('.images').append(image);
            }
            $(this).prepend(`<span class="badget badget-small badget-orange image-count">${image_count}</span>`);
        });
    }
    else {
        cards.each(function() {
            $(this).find('.image-count').remove();
            var img_count = $(this).find('img').length;
            for (var i = 0; i < img_count-1; i++) {
                var image = $(this).find('img').eq(0).clone();
                $(this).find('img').eq(0).remove();
                var new_card = $(this).clone();
                new_card.find('img').each(function() {
                    $(this).remove();
                });
                new_card.find('.images').append(image);
                new_card.insertAfter($(this));

                new_card.find('img').on('error', function() {
                    $(this).parent().append('<span class="bi bi-image"></span>');
                    $(this).remove();
                });
                new_card.find('img').on('load', function() {
                    $(this).parent().parent().find('.loading-spinner').hide();
                });
            }
        });
    }
}

Fancybox.bind('[data-fancybox]', {
    caption: function(fancybox, slide) {
        return `
            <div class="obs-content">
              <div class="row obs-id"><strong><a href="/observations/${slide.obs}">#${slide.obs}</a></strong></div>
              <div class="row">
                <div class="col name">${gettext('Спутник')}</div>
                <div class="col value">${slide.satellite}</div>
              </div>
              <div class="row">
                <div class="col name">${gettext('Станция')}</div>
                <div class="col value">${slide.station}</div>
              </div>
              <div class="row">
                <div class="col name">${gettext('Время начала')}</div>
                <div class="col value">${slide.start}</div>
              </div>
              <div class="row">
                <div class="col name">${gettext('Время окончания')}</div>
                <div class="col value">${slide.end}</div>
              </div>
              <div class="row">
                <p class="col name">${gettext('Время пакета')}</p>
                <p class="col value">${slide.timestamp}</p>
              </div>
            </div>
        `;
    },
    Toolbar: {
        display: {
            right: ['close'],
        },
    },
    l10n: {
        CLOSE: gettext('Закрыть'),
        NEXT: gettext('Следующий'),
        PREV: gettext('Предыдущий'),
        MODAL: gettext('Вы можете закрыть окно нажатием ESC'),
        ERROR: gettext('Ошибка, попробуйте повторить попытку'),
        IMAGE_ERROR: gettext('Изображение не найдено'),
        ELEMENT_NOT_FOUND: gettext('HTML элемент не найден'),
        AJAX_NOT_FOUND: gettext('Ошибка загрузки AJAX: Не найдено'),
        AJAX_FORBIDDEN: gettext('Ошибка загрузки AJAX: Запрещено'),
        IFRAME_ERROR: gettext('Ошибка при загрузке фрейма'),
        TOGGLE_ZOOM: gettext('Приблизить'),
        TOGGLE_THUMBS: gettext('Миниатюры'),
        TOGGLE_SLIDESHOW: gettext('Слайдшоу'),
        TOGGLE_FULLSCREEN: gettext('Полноэкранный режим'),
        DOWNLOAD: gettext('Загрузить')
    }
});