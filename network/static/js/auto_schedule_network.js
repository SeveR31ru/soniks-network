/* global Slider,gettext  */

$(document).ready(function () {
    var domain = document.domain.split('.')[0];
    var lang_en = domain == 'en' ? true : false;
    
    $('.selectpicker').selectpicker();  
    var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        },
        updateIndex = function(e, ui) {
            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i+1);
            });
            $('input[type=text]', ui.item.parent()).each(function (i) {
                $(this).val(i + 1);
            });
    
        };

    $('#satellite-table tbody').sortable({
        helper: fixHelperModified,
        beforeStop:updateIndex,
        stop:function(){
            save_schedule();
        }
    }).disableSelection();

    $('#satellite-table tbody').sortable({
        distance: 5,
        delay: 100,
        opacity: 0.6,
        cursor: 'move',
    });
    
    // функции, связанные с таблицей
    function show_alert(type, msg) {
        var alert_container = $('#alert-messages');
        if (!alert_container.find('.row').length) {
            alert_container.append('<div class="row"></div>');
        }
        $('#alert-messages .row').prepend(
            `<div class="alert alert-${type} alert-dismissible" role="alert">
              ${msg}
              <button type="button" class="btn btm-sm btn-without-border-red" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" class="bi bi-x"></span>
              </button>
            </div>`);
        setTimeout(function() {
            $('#alert-messages .row .alert:not(.disappear)').last().addClass('disappear');
            setTimeout(function() {
                $('#alert-messages .row .alert').last().remove();
            }, 1200);
        }, 5000);
    }
    
    $('#primary_schedule').click(function () {
        if ($('#primary_schedule span').hasClass('bi-chevron-down')) {
            $(this).html(`<span class="bi bi-chevron-up"></span> ${gettext('Скрыть основное расписание')}`);
        } else {
            $(this).html(`<span class="bi bi-chevron-down"></span> ${gettext('Показать основное расписание')}`);
        }
    });
    
    $('#advanced-options').click(function () {
        if ($('#advanced-options span').hasClass('bi-chevron-down')) {
            $(this).html(`<span class="bi bi-chevron-up"></span>${gettext('Скрыть настройки')}`);
        } else {
            $(this).html(`<span class="bi bi-chevron-down"></span>${gettext('Показать больше настроек')}`);
        }
    });
    
    
    $('#satellite-selection').on('changed.bs.select', function ()  {
        var satellite = $(this).find(':selected').data('norad');
        var station = $('#station-info').data('id');
        select_proper_transmitters({
            satellite: satellite,
            station: station
        });
    });
    
    $('#add-button').on('click',function () {
        var table_body = $('#satellite-table tbody')[0];
        var sat_id=$('#satellite-selection').find(':selected').data('sat-id');
        var norad_cat_id=$('#satellite-selection').find(':selected').data('norad');
        var sat_name=$('#satellite-selection').find(':selected').data('name');
        var row = table_body.insertRow(0);
        row.className='ui-sortable-handle';
        var index_cell = row.insertCell();
        var satellite_name_cell = row.insertCell(1);
        var transmitter_uuid_cell = row.insertCell(2);
        var transmitter_freq=row.insertCell(3);
        var transmitter_mode=row.insertCell(4);
        var button=row.insertCell(5);
        index_cell.className='index';
        index_cell.innerText = 1;
        satellite_name_cell.innerHTML = `<a href="#" data-toggle="modal" data-target="#SatelliteModal" data-id=${norad_cat_id} data-sat-id=${sat_id}>${sat_name}</a>`;
        transmitter_uuid_cell.innerText = $('#transmitter-selection').find(':selected').data('uuid');
        transmitter_freq.innerText = format_frequency($('#transmitter-selection').find(':selected').data('downlink-low'));
        transmitter_mode.innerText = $('#transmitter-selection').find(':selected').data('downlink-mode');
        satellite_name_cell.dataset.sat_id=sat_id;
        satellite_name_cell.dataset.norad_cat_id=norad_cat_id;
        button.innerHTML='<button type="button" class="delete-row-button" aria-hidden="true">&times;</button>';
        button.addEventListener('click',function(event) {
            var confirm_text = gettext('Вы правда хотите удалить этот спутник?');
            if(!confirm(confirm_text)) {return;} 
            var $closest = event.target.closest('tr');
            $closest.remove();
            let  table = $('#satellite-table');
            $('td.index', table).each(function (i) {
                $(this).html(i+1);
            });
            $('input[type=text]', table).each(function (i) {
                $(this).val(i + 1);
            });
            save_schedule(); 
        });
        $('td.index', table_body).each(function (i) {
            $(this).html(i+1);
        });
        $('input[type=text]', table_body).each(function (i) {
            $(this).val(i + 1);
        });
        //update_add_satellite_button_status();
        $('#add-button').prop('disabled',true);
        save_schedule();
        unlock_schedule_status_button();   
    });
    
    $('.delete-row-button').on('click',function(event) {
        var confirm_text = gettext('Вы правда хотите удалить этот спутник из расписания?');
        if(!confirm(confirm_text)) {return;} 
        var $closest = event.target.closest('tr');
        $closest.remove();
        let  table = $('#satellite-table');
        $('td.index', table).each(function (i) {
            $(this).html(i+1);
        });
        $('input[type=text]', table).each(function (i) {
            $(this).val(i + 1);
        });
        save_schedule(); 
    });
    
    $('[name="status"]').click(function () {
        save_status();
    });
    
    function update_add_satellite_button_status(){
        //check if satellite have transmitters in frequency range
        if ($('#transmitter-selection').val())
        {
            $('#add-button').prop('disabled',false);
        }else
        {
            $('#add-button').prop('disabled',true);
        }
        // check if this transmitter is already in schedule
        var table = document.querySelectorAll('#satellite-table tbody tr');
        var transmitter_uuid= $('#transmitter-selection').find(':selected').data('uuid');
        table.forEach(function(tr){
            if (tr.querySelectorAll('td')[2].innerText==transmitter_uuid)
            {
                $('#add-button').prop('disabled',true);
                var error_msg=gettext('Этот передатчик уже добавлен в основное расписание');
                show_alert('danger',error_msg);
            }
            
        });
    }

    
    function select_proper_transmitters(filters) {
        var url = '/transmitters_list/';
        var data = { 'satellite': filters.satellite };
        if (filters.station) {
            data.station_id = filters.station;
        }
        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
        }).done(function (data) {
            if (data.length == 1 && data[0].error) {
                $('#transmitter-selection').html(`<option id="no-transmitter"
                                                          value="" selected>
                                                    ${gettext('Нет доступных передатчиков')}
                                                  </option>`).prop('disabled', true);
                $('#transmitter-selection').selectpicker('refresh');
                update_add_satellite_button_status();
            } else if (data.transmitters_active.length > 0 || data.transmitters_inactive.length > 0 || data.transmitters_unconfirmed.length > 0) {
                var transmitters_options = '';
                var inactive_transmitters_options = '';
                var unconfirmed_transmitters_options = '';
                var hidden_input = '';
                var transmitter;
                if (filters.transmitter) {
                    var is_transmitter_available_active = (data.transmitters_active.findIndex(tr => tr.uuid == filters.transmitter) > -1);
                    if (is_transmitter_available_active) {
                        transmitter = data.transmitters_active.find(tr => tr.uuid == filters.transmitter);
                        transmitters_options = create_transmitter_option(filters.satellite, transmitter);
                        $('#transmitter-selection').html(transmitters_options);
                        $('#transmitter-selection').selectpicker('val', filters.transmitter);
                        hidden_input = `<input type="hidden" name="transmitter" value="${filters.transmitter}">`;
                        $('#transmitter-selection').after(hidden_input);
                        filters.transmitter = transmitter.uuid;
                    } else {
                        var is_transmitter_available_inactive = (data.transmitters_inactive.findIndex(tr => tr.uuid == filters.transmitter) > -1);
                        if (is_transmitter_available_inactive) {
                            transmitter = data.transmitters_inactive.find(tr => tr.uuid == filters.transmitter);
                            transmitters_options = create_transmitter_option(filters.satellite, transmitter);
                            $('#transmitter-selection').html(transmitters_options);
                            $('#transmitter-selection').selectpicker('val', filters.transmitter);
                            hidden_input = `<input type="hidden" name="transmitter" value="${filters.transmitter}">`;
                            $('#transmitter-selection').after(hidden_input);
                            filters.transmitter = transmitter.uuid;
                        } else {
                            var is_transmitter_available_unconfirmed = (data.transmitters_unconfirmed.findIndex(tr => tr.uuid == filters.transmitter) > -1);
                            if (is_transmitter_available_unconfirmed) {
                                transmitter = data.transmitters_unconfirmed.find(tr => tr.uuid == filters.transmitter);
                                transmitters_options = create_transmitter_option(filters.satellite, transmitter);
                                $('#transmitter-selection').html(transmitters_options);
                                $('#transmitter-selection').selectpicker('val', filters.transmitter);
                                hidden_input = `<input type="hidden" name="transmitter" value="${filters.transmitter}">`;
                                $('#transmitter-selection').after(hidden_input);
                                filters.transmitter = transmitter.uuid;
                            }
                            else {
                                $('#transmitter-selection').html(`<option id="no-transmitter" value="" selected>
                                ${gettext('Нет доступных передатчиков')}
                            </option>`).prop('disabled', true);
                                delete filters.transmitter;
                                update_add_satellite_button_status();
                            }
                        }
                    }
                    $('#transmitter-selection').selectpicker('refresh');
                } else {
                    var max_good_count = 0;
                    var max_good_val = '';
                    $.each(data.transmitters_active, function (i, transmitter) {
                        if (max_good_count <= transmitter.good_count) {
                            max_good_count = transmitter.good_count;
                            max_good_val = transmitter.uuid;
                        }
                        transmitters_options += create_transmitter_option(filters.satellite, transmitter);
                    });
                    var inactive_max_good_count = 0;
                    var inactive_max_good_val = '';
                    $.each(data.transmitters_inactive, function (i, transmitter) {
                        if (!max_good_count && inactive_max_good_count <= transmitter.good_count) {
                            inactive_max_good_count = transmitter.good_count;
                            inactive_max_good_val = transmitter.uuid;
                        }
                        inactive_transmitters_options += create_transmitter_option(filters.satellite, transmitter);
                    });
                    var unconfirmed_max_good_count = 0;
                    var unconfirmed_max_good_val = '';
                    $.each(data.transmitters_unconfirmed, function (i, transmitter) {
                        if ((!max_good_count || inactive_max_good_count) && unconfirmed_max_good_count <= transmitter.good_count) {
                            unconfirmed_max_good_count = transmitter.good_count;
                            unconfirmed_max_good_val = transmitter.uuid;
                        }
                        unconfirmed_transmitters_options += create_transmitter_option(filters.satellite, transmitter);
                    });

                    if (transmitters_options) {
                        transmitters_options = `<optgroup label="${gettext('Активные')}">${transmitters_options}</optgroup>`;
                    }

                    if (inactive_transmitters_options) {
                        inactive_transmitters_options = `<optgroup label="${gettext('Неактивные')}">${inactive_transmitters_options}</optgroup>`;
                    }

                    if (unconfirmed_transmitters_options) {
                        unconfirmed_transmitters_options = `<optgroup label="${gettext('Неподтвержденные')}">${unconfirmed_transmitters_options}</optgroup>`;
                    }

                    $('#transmitter-selection').html(transmitters_options + inactive_transmitters_options + unconfirmed_transmitters_options).prop('disabled', false);
                    
                    $('#transmitter-selection').selectpicker('refresh');
                    $('#transmitter-selection').selectpicker('val', max_good_val || inactive_max_good_val || unconfirmed_max_good_val);
                    filters.transmitter = max_good_val || inactive_max_good_val || unconfirmed_max_good_val;
                }
                $('.tle').hide();
                $('.tle[data-norad="' + filters.satellite + '"]').show();
            } else {
                $('#transmitter-selection').html(`<option id="no-transmitter"
                value="" selected>
                ${gettext('Нет доступных передатчиков')}
                </option>`).prop('disabled', true);
                $('#transmitter-selection').selectpicker('refresh');
                $('#station-field-loading').hide();
                $('#station-field').show();
            }
            $('#transmitter-field-loading').hide();
            $('#transmitter-field').show();
            update_add_satellite_button_status();
        });
    }

    
    function create_transmitter_option(satellite, transmitter) {
        const transmitter_freq = (transmitter.type === 'Transponder') ? (transmitter.downlink_low / 1e6).toFixed(3) + ' - ' + (transmitter.downlink_high / 1e6).toFixed(3) : (transmitter.downlink_low / 1e6).toFixed(3);
        return `
        <option data-satellite="` + satellite + `"
        data-transmitter-type="` + transmitter.type + `"
        data-downlink-low="` + transmitter.downlink_low + `"
        data-downlink-mode="` + transmitter.downlink_mode + `"
        data-downlink-high="` + transmitter.downlink_high + `"
        data-downlink-drift="` + transmitter.downlink_drift + `"
                    data-uuid="` + transmitter.uuid  + `"
                    value="` + transmitter.uuid + `"
                    data-success-rate="` + transmitter.success_rate + `"
                    data-content='<div class="transmitter-option">
                    <div class="transmitter-description">
                    ` + transmitter.description + ' | ' + transmitter_freq + ' MHz | ' + transmitter.downlink_mode +
            `</div>
            <div class="progress">
            <div class="progress-bar pb-success transmitter-good"
            data-toggle="tooltip" data-placement="bottom"
            title="` + transmitter.success_rate + '% (' + transmitter.good_count + `) Good"
            style="width:` + transmitter.success_rate + `%"></div>
            <div class="progress-bar pb-warning transmitter-unknown"
            data-toggle="tooltip" data-placement="bottom"
            title="` + transmitter.unknown_rate + '% (' + transmitter.unknown_count + `) Unknown"
            style="width:` + transmitter.unknown_rate + `%"></div>
            <div class="progress-bar pb-danger transmitter-bad"
            data-toggle="tooltip" data-placement="bottom"
            title="` + transmitter.bad_rate + '% (' + transmitter.bad_count + `) Bad"
            style="width:` + transmitter.bad_rate + `%"></div>
            <div class="progress-bar pb-info transmitter-future"
            data-toggle="tooltip" data-placement="bottom"
            title="` + transmitter.future_rate + '% (' + transmitter.future_count + `) Future"
            style="width:` + transmitter.future_rate + `%"></div>
            </div>
            </div>'>
            </option>
            `;
    }
        
    function format_frequency(val) {
        if (!Number.isInteger(val)) {
            return 'Error';
        }
        
        const unit_table = ['Hz', 'kHz', 'MHz', 'GHz'];
        const div = Math.floor(Math.log10(val) / 3);
        const unit = unit_table[(div > 3) ? 3 : div];
        
        return val / (Math.pow(1000, (div > 3) ? 3 : div)) + ' ' + unit;
    }
    const frequency_input_format = $('#frequency-input-format');
    const frequency_input = $('#center-frequency-input');
    const frequency_formgroup = $('#center-frequency-formgroup');
    const frequency_errors = { '1': 'Value is not a number', '2': 'Value out of range' };
    
    frequency_input.on('input', function () {
        var val = parseInt($(this).val());
        const min = parseInt(frequency_input.attr('min'));
        const max = parseInt(frequency_input.attr('max'));
        var has_error = 0;
        
        if (isNaN(val)) {
            has_error = 1;
        }
        else if (val < min || val > max) {
            has_error = 2;
        }

        if (!has_error) {
            frequency_formgroup.removeClass('has-error');
            frequency_input_format.removeClass('alert-error');
            frequency_input.data('is-valid', true);
            frequency_input_format.html(format_frequency(val));
            
        } else {
            frequency_formgroup.addClass('has-error');
            frequency_input_format.addClass('alert-error');
            frequency_input_format.html(frequency_errors[has_error]);
            
            frequency_input.data('is-valid', false);
        }
    });
    
    $('#transmitter-selection').on('changed.bs.select', function () {
        var transmitter_object = $(this).find(':selected');
        var transmitter_type = transmitter_object.data('transmitter-type');
        var downlink_high = transmitter_object.data('downlink-high');
        var downlink_low = transmitter_object.data('downlink-low');
        var downlink_drift = transmitter_object.data('downlink-drift');
        if (transmitter_type === 'Transponder') {
            frequency_input.attr({ 'min': (downlink_drift && downlink_drift < 0) ? downlink_low + downlink_drift : downlink_low, 'max': (downlink_drift && downlink_drift > 0) ? downlink_high + downlink_drift : downlink_high });
            frequency_input.val(Math.floor((downlink_high + downlink_low) / 2));
            frequency_input.data('is-valid', true);
            frequency_input_format.html(format_frequency(Math.floor((downlink_high + downlink_low) / 2)));
            $('#center-frequency-formgroup').fadeIn('fast');
        } else {
            frequency_input.data('is-valid', false);
            $('#center-frequency-formgroup').fadeOut('fast');
        }
        update_add_satellite_button_status();
    });
            
    function unlock_schedule_status_button(){
        if (($('#active-status').prop('disabled')==true)&&($('#inactive-status').prop('disabled')==true)) {
            $('#active-status').prop('disabled',false).removeAttr('disabled');
            $('#inactive-status').prop('disabled',false).removeAttr('disabled');
            $('#active-status').prop('checked',true);
        }
    }
        
    function save_schedule(){
        var url = '/save_auto_schedule_network/';
        var data ={};
        data.station=$('#station-info').data('id');
        var satellites=[];
        var table = document.querySelectorAll('#satellite-table tbody tr');
        table.forEach(function(tr){
            var row=tr.querySelectorAll('td');
            var row_data={};
            var sat_row=row[1].getElementsByTagName('a')[0];
            row_data.prio=row[0].innerText;
            row_data.sat_name=sat_row.innerText;
            row_data.sat_id=sat_row.dataset.satId;
            row_data.transmitter_uuid=row[2].innerText;
            satellites.push(JSON.stringify(row_data));
        });
        data.satellites=satellites;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function (results) {
            var success_msg=results.success;
            show_alert('success',success_msg);
        }).fail(function(error){
            var error_msg=error.responseJSON.error;
            show_alert('danger',error_msg);
        });
    }
    
    function save_status(){
        var url = '/save_auto_schedule_status_network/';
        var data ={};
        data.station=$('#station-info').data('id');
        data.status=$('input[name="status"]:checked').val(); 
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function (results) {
            if (results.length == 1 && results[0].error) {
                var error_msg = results[0].error;
                show_alert('danger',error_msg);
            } else if (results[0].success){
                var success_msg=results[0].success;
                show_alert('success',success_msg);
            } 
        });
    }
    
    function save_parameters(input_data){
        var url='/save_auto_schedule_parameters_network/';
        var data=input_data;
        data.station=$('#station-info').data('id');
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function (results) {
            var success_msg=results.success;
            show_alert('success',success_msg);
            $('#set-parameters').prop('disabled',true);
            
        }).fail(function(error){
            var error_msg=error.responseJSON.error;
            show_alert('danger',error_msg);
        });
    }
    // функции для параметров автопланировщика
    
    $('#default-horizon').click(function () {
        $('#min-horizon').remove();
        $('#min-horizon-slider').parent().parent().remove();
        $('#set-parameters').prop('disabled',false);
    });
    
    $('#custom-horizon').click(function () {
        if (!$('#min-horizon').length) {
            var horizon_default_value=$('#default-horizon input').val();
            $('#horizon-status').append('<div class="row  justify-content-end"><div class="col-md-6"><input type="hidden" name="min-horizon" id="min-horizon"></div></div>');
            new Slider('#min-horizon',{id:'min-horizon-slider',
                min:0,
                max:90,
                step:1,
                value:parseInt(horizon_default_value),
                ticks:[0, 30, 60, 90],
                ticks_labels: ['0', '30', '60', '90'],
                ticks_positions:[0,33,66,100]
            }).on('change',function(){
                $('#set-parameters').prop('disabled',false);
            });
        }
    });
    
    $('#default-culmination').click(function () {
        $('#max-culmination').remove();
        $('#max-culmination-slider').parent().parent().remove();
        $('#set-parameters').prop('disabled',false);
    });
    
    $('#custom-culmination').click(function () {
        if (!$('#max-culmination').length) {
            $('#culmination-status').append('<div class="row justify-content-end"><div class="col-md-6"><input type="hidden" name="max-culmination" id="max-culmination"></div></div>');
            new Slider('#max-culmination',{id:'max-culmination-slider',
                min:0,
                max:90,
                step:1,
                value:[0,90],
                ticks:[0, 30, 60, 90],
                ticks_labels: ['0', '30', '60', '90'],
                ticks_positions:[0,33,66,100]
            }).on('change',function(){
                $('#set-parameters').prop('disabled',false);
            });
        }
    });
    
    const custom_split_formgroup = $('#split-duration-formgroup');
    function reset_split_duration() {
        custom_split_formgroup.removeClass('has-error');
        $('#split-duration-custom').parent().parent().remove();
        $('#split-duration-span').remove();
    }
    
    $('#default-split-duration').click(function () {
        reset_split_duration();
        $('#set-parameters').prop('disabled',false);
    });

    function get_errors(code, min_val) {
        switch (code) {
        case 1:
            return lang_en ? 'The value is not a number.':'Значение не число.';
        case 2:
            return lang_en ? `Enter a value equal to or greater than ${min_val}.`:`Введите значение равное и больше, чем ${min_val}.`;
        default:
            return lang_en ? 'Invalid input.':'Некорректный ввод.';
        }
    }

    $('#custom-split-duration').click(function () {
        if (!$('#split-duration-custom').length) {
            var value = $('#default-split-duration input')[0].value;
            var min_value = $('#default-split-duration input')[0].dataset.min;
            $('#split-duration-status').append('<div class="row justify-content-end"><div class="col-md-6 p-0"><input type="number" name="split_duration_custom" id="split-duration-custom" class="duration-number-input form-control" min="' + min_value + '" step="1" value="' + value + '"></div><div>');
            $('#split-duration-status').append('<span id="split-duration-span"></span>');
            const custom_split = $('#split-duration-custom');
            custom_split.on('input',function(){
                $('#set-parameters').prop('disabled',false);
            });
            custom_split.on('input', function () {
                var has_error = 0;
                if (isNaN(custom_split.val()) || custom_split.val() == '') {
                    has_error = 1;
                } else if (parseInt(custom_split.val()) < min_value) {
                    has_error = 2;
                }
                if (has_error) {
                    custom_split_formgroup.addClass('has-error');
                    $('#split-duration-span').addClass('alert-error');
                    $('#calculate-observation').prop('disabled', true);
                    $('#schedule-observation').prop('disabled', true).addClass('disabled');
                    $('#split-duration-span').html(get_errors(has_error, min_value));
                } else {
                    custom_split_formgroup.removeClass('has-error');
                    $('#split-duration-span').removeClass('alert-error');
                    $('#calculate-observation').prop('disabled', false);
                    $('#schedule-observation').prop('disabled', false).removeClass('disabled');
                    $('#split-duration-span').html('');
                }
            });
        }
    });
    
    $('#default-break-duration').click(function () {
        $('#break-duration-custom').parent().parent().remove();
        $('#set-parameters').prop('disabled',false);
    });
    
    $('#custom-break-duration').click(function () {
        if (!$('#break-duration-custom').length) {
            var value = $('#default-break-duration input')[0].value;
            $('#break-duration-status').append('<div class="row justify-content-end"><div class="col-md-6 p-0"><input type="number" name="break_duration_custom" id="break-duration-custom" class="duration-number-input form-control" min="0" step="1" value="' + value + '"></div><div>');
            $('#break-duration-custom').on('input',function(){
                $('#set-parameters').prop('disabled',false);
            });
        }
    });

    $('#default-azimutes').click(function () {
        $('#min-max-azimutes').remove();
        $('#min-max-azimutes-slider').parent().parent().remove();
        $('#set-parameters').prop('disabled',false);
    });

    $('#custom-azimutes').click(function () {
        if (!$('#min-max-azimutes').length) {
            $('#azimuts').append('<div class="row justify-content-end"><div class="col-md-6"><input type="hidden" name="min-max-azimutes" id="min-max-azimutes"></div></div>');
            new Slider('#min-max-azimutes',{id:'min-max-azimutes-slider',
                range:true,    
                min:0,
                max:360,
                step:1,
                value:[0,360],
                ticks:[0, 90, 180, 270,360],
                ticks_positions:[0,25,50,75,100],
                ticks_labels: ['0', '90', '180', '270','360']}).on('change',function(){
                $('#set-parameters').prop('disabled',false);
            });
        }
    });
    
    $('#set-parameters').click(function(){
        var data ={};
        var is_custom_horizon=$('#horizon-status input[type=radio]').filter(':checked').val()=='custom';
        if(is_custom_horizon){
            data.min_horizon=parseInt($('#min-horizon').val());
        }
        else{
            data.min_horizon=parseInt($('#horizon-status input[type=radio]').filter(':checked').val());
        }
        var is_split_duration = $('#split-duration-status input[type=radio]').filter(':checked').val() == 'custom';
        if (is_split_duration) {
            var split_duration = parseInt($('#split-duration-custom').val());
            var min_value = parseInt($('#default-split-duration input')[0].dataset.min);
            if (!isNaN(split_duration) && split_duration >= min_value) {
                data.split_duration = split_duration;
            }
        }
        else{
            data.split_duration = parseInt($('#split-duration-status input[type=radio]').filter(':checked').val());
        }
        var is_break_duration = $('#break-duration-status input[type=radio]').filter(':checked').val() == 'custom';
        if (is_break_duration) {
            var break_duration = parseInt($('#break-duration-custom').val());
            if (!isNaN(break_duration) && break_duration > 0) {
                data.break_duration = break_duration;
            }
            else if(break_duration < 0){
                data.break_duration = 0;
            }
        }
        else{
            data.break_duration = parseInt($('#break-duration-status input[type=radio]').filter(':checked').val());
        }
        
        var is_custom_azimutes= $('#azimuts input[type=radio]').filter(':checked').val() == 'custom';
        if(is_custom_azimutes){
            var list_az=$('#min-max-azimutes').val().split(',');
            data.start_azimuth = parseInt(list_az[0]);
            data.end_azimuth = parseInt(list_az[1]);
        }
        else{
            var default_list_az=$('#default-azimutes input[type=radio]').filter(':checked').val().split(',');
            data.start_azimuth=parseInt(default_list_az[0]);
            data.end_azimuth=parseInt(default_list_az[1]);
        }
        var is_custom_elevation=$('#culmination-status input[type=radio]').filter(':checked').val()=='custom';
        if(is_custom_elevation){
            var list_cul=$('#max-culmination').val().split(',');
            data.start_culmination = parseInt(list_cul[0]);
            data.end_culmination = parseInt(list_cul[1]);
        }
        else{
            var default_list_cul=$('#culmination-status input[type=radio]').filter(':checked').val().split(',');
            data.start_culmination=parseInt(default_list_cul[0]);
            data.end_culmination=parseInt(default_list_cul[1]);
        }
        save_parameters(data);
    });
    
    function check_if_have_parameters(){
        var url = '/parameters_list_network/';
        var data= {};
        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
        }).done(function (data) {
            if (data==null)
            {
                data={};
            }
            if ('min_horizon' in data) {
                $('#custom-horizon').addClass('active');
                ($('input[name="horizon"][value="custom"]')).prop('checked',true);
                $('#horizon-status').append('<div class="row justify-content-end"><div class="col-md-6"><input type="hidden" name="min-horizon" id="min-horizon"></div></div>');
                new Slider('#min-horizon',{id:'min-horizon-slider',
                    min:0,
                    max:90,
                    step:1,
                    value:parseInt(data.min_horizon),
                    ticks:[0, 30, 60, 90],
                    ticks_labels: ['0', '30', '60', '90'],
                    ticks_positions:[0,33,66,100]}).on('change',function(){
                    $('#set-parameters').prop('disabled',false);
                });
            }
            else{
                $('#default-horizon').addClass('active');
                ($('#default-horizon input[name="horizon"]')).prop('checked',true);
            }

            if ('start_culmination' in data || 'end_culmination' in data ) {
                $('#custom-culmination').addClass('active');
                ($('input[name="culmination"][value="custom"]')).prop('checked',true);
                $('#culmination-status').append('<div class="row justify-content-end"><div class="col-md-6"><input type="hidden" name="max-culmination" id="max-culmination"></div></div>');
                new Slider('#max-culmination',{id:'max-culmination-slider',
                    min:0,
                    max:90,
                    step:1,
                    value:[parseInt(data.start_culmination)||0,parseInt(data.end_culmination)||90],
                    ticks:[0, 30, 60, 90],
                    ticks_labels: ['0', '30', '60', '90'],
                    ticks_positions:[0,33,66,100]}).on('change',function(){
                    $('#set-parameters').prop('disabled',false);
                });
            }
            else{
                $('#default-culmination').addClass('active');
                ($('#default-culmination input[name="culmination"]')).prop('checked',true);
            }
            
            if ('split' in data) {
                $('#custom-split-duration').addClass('active');
                ($('input[name="split_duration"][value="custom"]')).prop('checked',true);
                var split_value = parseInt(data.split);
                var min_value = $('#default-split-duration input')[0].dataset.min;
                $('#split-duration-status').append('<div class="row justify-content-end"><div class="col-md-6 p-0"><input type="number" name="split_duration_custom" id="split-duration-custom" class="duration-number-input form-control" min="' + min_value + '" step="1" value="' + split_value + '"></div></div>');
                $('#split-duration-status').append('<span id="split-duration-span"></span>');
                const custom_split = $('#split-duration-custom').on('change',function(){
                    $('#set-parameters').prop('disabled',false);
                });
                custom_split.on('input', function () {
                    var has_error = 0;
                    if (isNaN(custom_split.val()) || custom_split.val() == '') {
                        has_error = 1;
                    } else if (parseInt(custom_split.val()) < min_value) {
                        has_error = 2;
                    }
    
                    if (has_error) {
                        custom_split_formgroup.addClass('has-error');
                        $('#split-duration-span').addClass('alert-error');
                        $('#calculate-observation').prop('disabled', true);
                        $('#schedule-observation').prop('disabled', true).addClass('disabled');
                        $('#split-duration-span').html(get_errors(has_error, min_value));
                    } else {
                        custom_split_formgroup.removeClass('has-error');
                        $('#split-duration-span').removeClass('alert-error');
                        $('#calculate-observation').prop('disabled', false);
                        $('#schedule-observation').prop('disabled', false).removeClass('disabled');
                        $('#split-duration-span').html('');
                    }
                });
            }
            else{
                $('#default-split-duration').addClass('active');
                ($('#default-split-duration input[name="split_duration"]')).prop('checked',true);
            }
    
            if ('break' in data) {
                $('#custom-break-duration').addClass('active');
                ($('input[name="break_duration"][value="custom"]')).prop('checked',true);
                var break_value = parseInt(data.break);
                $('#break-duration-status').append('<div class="row justify-content-end"><div class="col-md-6 p-0"><input type="number" name="break_duration_custom" id="break-duration-custom" class="duration-number-input form-control" min="0" step="1" value="' + break_value + '"></div></div>')
                    .on('input',function(){
                        $('#set-parameters').prop('disabled',false);
                    });
            }
            else{
                $('#default-break-duration').addClass('active');
                ($('#default-break-duration input[name="break_duration"]')).prop('checked',true);
            }
    
            if ('min_az' in data || 'max_az' in data ) {
                $('#custom-azimutes').addClass('active');
                ($('input[name="azimutes"][value="custom"]')).prop('checked',true);
                $('#azimuts').append('<div class="row justify-content-end"><div class="col-md-6"><input type="hidden" name="min-max-azimutes" id="min-max-azimutes"></div></div>');
                new Slider('#min-max-azimutes',{id:'min-max-azimutes-slider',  
                    min:0,
                    max:360,
                    step:1,
                    value:[parseInt(data.min_az)||0,parseInt(data.max_az)||360],
                    ticks:[0, 90, 180, 270,360],
                    ticks_positions:[0,25,50,75,100],
                    ticks_labels: ['0', '90', '180', '270','360']}).on('change',function(){
                    $('#set-parameters').prop('disabled',false);
                });
            }
            else{
                $('#default-azimutes').addClass('active');
                ($('#default-azimutes input[name="azimutes"]')).prop('checked',true);
            }
        }); 
    }
    
    check_if_have_parameters($);
});
