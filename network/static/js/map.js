/*global mapboxgl*/

$(document).ready(function () {
    'use strict';

    var whiteStyle = 'mapbox://styles/kurnosovaiv/clmhjm9nr01mv01r74civ01a3';
    var darkStyle = 'mapbox://styles/kurnosovaiv/clmhl04sh01kj01r40rkg9o66';
    var online_points = {};
    var testing_points = {};
    var offline_points = {};

    var mapboxtoken = $('div#map').data('mapboxtoken');
    var stations = $('div#map').data('stations');
    var mapStyle = localStorage.getItem('darkmode') == 'false' ? darkStyle : whiteStyle;

    mapboxgl.accessToken = mapboxtoken;

    var map = new mapboxgl.Map({
        container: 'map',
        style: mapStyle,
        zoom: 2,
        minZoom: 1.8,
        center: [90, 55]
    });

    if (window.innerWidth < 770) {
        map.setMinZoom(1);
        map.setZoom(1);
    }

    map.touchZoomRotate.disableRotation();
    map.dragRotate.disable();
    if (!('ontouchstart' in window)) {
        map.addControl(new mapboxgl.NavigationControl());
    }

    map.on('load', function () {

        map.loadImage('/static/img/online.png', function (error, image) {
            map.addImage('online', image);
        });

        map.loadImage('/static/img/testing.png', function (error, image) {
            map.addImage('testing', image);
        });

        online_points = {
            'id': 'online-points',
            'type': 'symbol',
            'source': {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': []
                }
            },
            'layout': {
                'icon-image': 'online',
                'icon-size': 0.25,
                'icon-allow-overlap': true
            }
        };

        testing_points = {
            'id': 'testing-points',
            'type': 'symbol',
            'source': {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': []
                }
            },
            'layout': {
                'icon-image': 'testing',
                'icon-size': 0.25,
                'icon-allow-overlap': true
            }
        };

        $.ajax({
            url: stations
        }).done(function (data) {
            data.forEach(function (m) {
                if (m.status == 1) {
                    create_station_point(testing_points, m);
                } else if (m.status == 2) {
                    create_station_point(online_points, m);
                }
            });

            // Add layers to map
            map.addLayer(offline_points);
            map.addLayer(testing_points);
            map.addLayer(online_points);

            // Register keys for toggling visibility of layers
            $(document).bind('keyup', function (event) {
                if (event.which == 79) {
                    toggle_layer(map, offline_points);
                } 
                else if (event.which == 78) {
                    toggle_layer(map, online_points);
                } 
                else if (event.which == 84) {
                    toggle_layer(map, testing_points);
                }
            });

        });
    });

    // Toggle map layer
    function toggle_layer(map, layer) {
        var visibility = map.getLayoutProperty(layer.id, 'visibility');

        //Check if layer is already visible
        if (visibility === 'visible') {
            map.setLayoutProperty(layer.id, 'visibility', 'none');
            layer.className = '';
        } else {
            layer.className = 'active';
            map.setLayoutProperty(layer.id, 'visibility', 'visible');
        }
    }

    // Create a popup, but don't add it to the map yet.
    var popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: true
    });

    map.on('mouseenter', 'online-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    map.on('click', 'online-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    map.on('mouseenter', 'testing-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    map.on('click', 'testing-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    // Resize map for Stations modal
    $('#MapModal').on('shown.bs.modal', function () {
        map.resize();
    });

    $('button.darkmode-toggle').click(function() {
        if (localStorage.getItem('darkmode') == 'false') {
            map.setStyle(darkStyle);
        }
        else {
            map.setStyle(whiteStyle);
        }
        map.on('style.load', function() {
            if (!map.getLayer(offline_points.id)) {
                map.loadImage('/static/img/offline.png', function (error, image) {
                    map.addImage('offline', image);
                });
                map.addLayer(offline_points);
            }
            if (!map.getLayer(online_points.id)) {
                map.loadImage('/static/img/online.png', function (error, image) {
                    map.addImage('online', image);
                });
                map.addLayer(online_points);
            }
            if (!map.getLayer(testing_points.id)) {
                map.loadImage('/static/img/testing.png', function (error, image) {
                    map.addImage('testing', image);
                });
                map.addLayer(testing_points);
            }
        });
    });

    $('div.gotocard').click(function() {
        create_big_map(map);
    });

    $('div.gotoback').click(function() {
        hide_big_map(map);
    });
});


function create_big_map(map) {
    $('header.darkmode-ignore').addClass('d-none');
    var blockMap = $('#map');
    blockMap.addClass('prev-map');
    $('.gotoback').addClass('show-arrow').removeClass('d-none');
    map.resize();
}

function hide_big_map(map) {
    $('header.darkmode-ignore').removeClass('d-none');
    var blockMap = $('#map');
    blockMap.removeClass('prev-map');
    $('.gotoback').removeClass('show-arrow').addClass('d-none');
    $('.mapboxgl-popup').hide();
    map.resize();
    map.flyTo({center:[90, 55], zoom: 2});
}

function create_station_point(array, station) {
    var key = `${station.lng}${station.lat}`;
    var index = array.source.data.features.findIndex(e => e.key === key);
    if (index > -1){
        array.source.data.features[index].properties.description += '</br><a href="/stations/' + station.id + '">' + station.id + ' - ' + station.name + '</a>';
    }
    array.source.data.features.push({
        'key': key,
        'type': 'Feature',
        'geometry': {
            'type': 'Point',
            'coordinates': [
                parseFloat(station.lng),
                parseFloat(station.lat)]
        },
        'properties': {
            'description': '<a href="/stations/' + station.id + '">' + station.id + ' - ' + station.name + '</a>',
        }
    });
}
