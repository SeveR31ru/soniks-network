/* global mapboxgl, moment, Fancybox, Slider, calcPolarPlotSVG, bands_from_range,gettext  */
/* jshint esversion: 6 */

$(document).ready(function () {
    'use strict';

    $('.loading-spinner').hide();

    $('#btnFilter').click(function() {
        $('#collapseFilters').slideToggle();
    });

    $('.card-body.collapse').on('hide.bs.collapse', function () {
        $(this).parent().find('.card-header i').addClass('bi bi-arrows-expand').removeClass('bi bi-arrows-collapse');
    });

    $('.card-body.collapse').on('show.bs.collapse', function () {
        $(this).parent().find('.card-header i').addClass('bi bi-arrows-collapse').removeClass('bi bi-arrows-expand');
    });

    // Render Station success rate
    var success_rate = $('.gs.pb-success').data('success-rate');
    var percentagerest = $('.gs.pb-danger').data('percentagerest');
    $('.gs.pb-success').css('width', success_rate + '%');
    $('.gs.pb-danger').css('width', percentagerest + '%');

    // Reading data for station
    var station_info = $('#station-info').data();

    // Confirm station deletion
    var actions = $('#station-delete');
    if (actions.length) {
        actions[0].addEventListener('click', function (e) {
            if ($('#delete-confirm-id').val() != station_info.id) {
                $('#delete-confirm-id').val('');
                $('#delete-confirm-id').attr('placeholder', 'Wrong ID!');
                e.preventDefault();
            }
        });
    }

    var show_map = false;
    var error_text = gettext('Карта не может быть отображена:<br/> Местоположение станции не определено.'); 

    // Init the map
    if (isNaN(parseFloat(station_info.lat)) || isNaN(parseFloat(station_info.lng))) {
        
        $('#map-station').addClass('error');
        $('#map-station').addClass('alert-error');
        $('#map-station').html(error_text);
    } else {
        if (!mapboxgl.supported()) {
            error_text = gettext('Карта не может быть отображена:<br/> Ваш браузер не поддерживает MapboxGL (требуется WebGL).'); 
            $('#map-station').addClass('error');
            $('#map-station').addClass('alert-error');
            $('#map-station').html(error_text);
        } else {
            var mapboxtoken = $('div#map-station').data('mapboxtoken');
            if (!mapboxtoken) {
                error_text = gettext('Карта не может быть отображена:<br/> Токен Mapbox недоступен.');
                $('#map-station').addClass('error');
                $('#map-station').addClass('alert-error');
                $('#map-station').html(error_text);
            } else {
                var whiteStyle = 'mapbox://styles/kurnosovaiv/clmhjm9nr01mv01r74civ01a3';
                var darkStyle = 'mapbox://styles/kurnosovaiv/clmhl04sh01kj01r40rkg9o66';
                var mapStyle = localStorage.getItem('darkmode') == 'false' ? darkStyle : whiteStyle;
                mapboxgl.accessToken = mapboxtoken;
                show_map = true;
                var map_points = {};

                var map = new mapboxgl.Map({
                    container: 'map-station',
                    style: mapStyle,
                    zoom: 5,
                    minZoom: 2,
                    center: [parseFloat(station_info.lng), parseFloat(station_info.lat)]
                });
                map.addControl(new mapboxgl.NavigationControl());
                map.on('load', function () {
                    map.loadImage('/static/img/' + station_info.status + '.png', function (error, image) {
                        map.addImage(station_info.status, image);
                        map_points = {
                            'id': 'points',
                            'type': 'symbol',
                            'source': {
                                'type': 'geojson',
                                'data': {
                                    'type': 'FeatureCollection',
                                    'features': [{
                                        'type': 'Feature',
                                        'geometry': {
                                            'type': 'Point',
                                            'coordinates': [
                                                parseFloat(station_info.lng),
                                                parseFloat(station_info.lat)]
                                        },
                                        'properties': {
                                            'description': '<a href="/stations/' + station_info.id + '">' + station_info.id + ' - ' + station_info.name + '</a>',
                                            'icon': 'circle'
                                        }
                                    }]
                                }
                            },
                            'layout': {
                                'icon-image': station_info.status,
                                'icon-size': 0.4,
                                'icon-allow-overlap': true
                            }
                        };
                        map.addLayer(map_points);
                    });
                    map.repaint = true;
                });
            }
        }
    }

    $('button.darkmode-toggle').click(function() {
        if (show_map) {
            if (localStorage.getItem('darkmode') == 'false') {
                map.setStyle(darkStyle);
            }
            else {
                map.setStyle(whiteStyle);
            }

            map.on('style.load', function() {
                if (!map.getLayer(map_points.id)) {
                    map.loadImage('/static/img/' + station_info.status + '.png', function (error, image) {
                        map.addImage(station_info.status, image);
                    });
                    map.addLayer(map_points);
                    map.repaint = true;
                }
            });
        }


    });

    //Slider filters for pass predictions
    var success_slider = new Slider('#success-filter', {
        range: true,
        min: 0,
        max: 100,
        value: [0, 100],
        step: 5,
    });
    var elevation_slider = new Slider('#elevation-filter', {
        range: true,
        min: 0,
        max: 90,
        value: [0, 90],
        step: 1,
    });
    var overlap_slider = new Slider('#overlap-filter', {
        range: true,
        min: 0,
        max: 100,
        value: [0, 100],
        step: 1
    });

    var elmin = 0;
    var elmax = 90;
    var sumin = 0;
    var sumax = 100;
    var ovmin = 0;
    var ovmax = 100;

    function filter_passes() {
        var filter_unknown = $('.status-filter[name="unknown"').prop('checked');
        var filter_inactive = $('.status-filter[name="inactive"').prop('checked');
        var filter_freq = [];
        var is_freq = false;

        $('.freq-filter:checked').each(function(k, v) {
            filter_freq.push([$(v).data('min'), $(v).data('max')]);
        });

        if (filter_freq.length) {
            is_freq = true;
        }

        $('tr.pass').each(function (k, v) {
            var passmax = $(v).find('td.max-elevation').data('max');
            var success = $(v).find('td.success-rate').data('suc');
            var over = $(v).data('overlap');
            var unknown = $(v).data('sat-unknown');
            var active = $(v).data('sat-active');
            var sat_freqs = $(v).data('sat-freq').toString().split(',');
            var visibility = true;

            if (passmax < elmin || passmax > elmax) {
                visibility = false;
            }
            if (success < sumin || success > sumax) {
                visibility = false;
            }
            if (over < ovmin || over > ovmax) {
                visibility = false;
            }
            if (!filter_unknown && unknown) {
                visibility = false;
            }
            if (!filter_inactive && !active) {
                visibility = false;
            }
            if (is_freq && visibility) {
                var freq_ok = false;
                for (var sat_freq of sat_freqs) {
                    for (var filter of filter_freq) {
                        if (sat_freq >= filter[0] && sat_freq <= filter[1]) {
                            freq_ok = true;
                        }
                    }
                }
                visibility = freq_ok;
            }
            if (visibility) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        // Update count of predictions visible
        var filtered_count = $('tr.pass:visible').length;
        $('#prediction-results-count').html(filtered_count);
    }

    elevation_slider.on('slideStop', function(ui) {
        elmin = ui[0];
        elmax = ui[1];
        filter_passes();
    });

    success_slider.on('slideStop', function(ui) {
        sumin = ui[0];
        sumax = ui[1];
        filter_passes();
    });

    overlap_slider.on('slideStop', function(ui) {
        ovmin = ui[0];
        ovmax = ui[1];
        filter_passes();
    });

    function calculate_predictions() {
        // Pass predictions loading
        $('#calculate-button').prop('disabled', true);
        $('.loading-spinner').show();
        $('.pass').remove();
        $('#error-pass-predictions').remove();
        $('#resTable').addClass('d-none');

        var polar_size = '100px';
        if (window.innerWidth < 768) {
            polar_size = '70px';
        }

        $.ajax({
            url: '/pass_predictions/' + $('#station-info').attr('data-id') + '/',
            cache: false,
            error: function () {
                $('.table-wrapper').append(`
                  <span id="error-pass-predictions">
                    ${gettext('В процессе вычислений произошла ошибка, убедитесь что станция существует и её локация определена! Если ошибка повторится - свяжитесь с администратором сайта.')}
                  <span>`
                );
                $('#resTable').addClass('d-none');
            },
            success: function (data) {
                var len = data.nextpasses.length - 1;
                var new_obs = $('#station-info').attr('data-new-obs');
                var station = $('#station-info').attr('data-id');
                var can_schedule = $('#station-info').data('schedule');

                for (var i = 0; i <= len; i++) {
                    var schedulable = data.nextpasses[i].valid && can_schedule;
                    var tr = moment(data.nextpasses[i].tr).format('YYYY/MM/DD HH:mm');
                    var ts = moment(data.nextpasses[i].ts).format('YYYY/MM/DD HH:mm');
                    var tr_display_date = moment(data.nextpasses[i].tr).format('YYYY-MM-DD');
                    var tr_display_time = moment(data.nextpasses[i].tr).format('HH:mm');
                    var ts_display_date = moment(data.nextpasses[i].ts).format('YYYY-MM-DD');
                    var ts_display_time = moment(data.nextpasses[i].ts).format('HH:mm');
                    var tr_svg = moment.utc(data.nextpasses[i].tr).format();
                    var ts_svg = moment.utc(data.nextpasses[i].ts).format();

                    var overlap_style = null;
                    var overlap = 0;
                    if (data.nextpasses[i].overlapped) {
                        overlap = Math.round(data.nextpasses[i].overlap_ratio * 100);
                        overlap_style = 'overlap';
                    }

                    var freqs = data.nextpasses[i].satellite_freq.toString();
                    var good_text=gettext('Хорошие');
                    var unknown_text=gettext('Неизвестные');
                    var bad_text=gettext('Плохие');
                    var future_text=gettext('Будущие');
                    var north_text=gettext('С');
                    var south_text=gettext('Ю');
                    var east_text=gettext('В');
                    var west_text=gettext('З');
                    var schedule_text=gettext('Запланировать');
                    var overlap_percent_text=gettext('Процент перекрытия имеющихся наблюдений');
                    var overlap_text=gettext('перекрытие');
                    $('#pass-predictions-results').append(`
                      <tr class="pass ${overlap_style}"
                        data-overlap="${overlap}"
                        data-sat-active="${data.nextpasses[i].satellite_is_active}"
                        data-sat-unknown="${data.nextpasses[i].satellite_unknown}"
                        data-sat-freq="${freqs}">
                        <td class="success-rate" data-suc="${data.nextpasses[i].success_rate}">
                          <a href="#" data-toggle="modal" data-target="#SatelliteModal"
                          data-id="${data.nextpasses[i].norad_cat_id}" data-sat-id="${data.nextpasses[i].sat_id}">
                            ${data.nextpasses[i].norad_cat_id} - ${data.nextpasses[i].name}
                          </a>
                          <div class="progress satellite-success">
                            <div class="progress-bar pb-green" style="width: ${data.nextpasses[i].success_rate}%"
                                 data-toggle="tooltip" data-placement="bottom" title="${data.nextpasses[i].success_rate}% (${data.nextpasses[i].good_count}) ${good_text}">
                            </div>
                            <div class="progress-bar pb-orange" style="width: ${data.nextpasses[i].unknown_rate}%"
                                 data-toggle="tooltip" data-placement="bottom" title="${data.nextpasses[i].unknown_rate}% (${data.nextpasses[i].unknown_count}) ${unknown_text}">
                            </div>
                            <div class="progress-bar pb-red" style="width: ${data.nextpasses[i].bad_rate}%"
                                 data-toggle="tooltip" data-placement="bottom" title="${data.nextpasses[i].bad_rate}% (${data.nextpasses[i].bad_count}) ${bad_text}">
                            </div>
                            <div class="progress-bar pb-blue" style="width: ${data.nextpasses[i].future_rate}%"
                                 data-toggle="tooltip" data-placement="bottom" title="${data.nextpasses[i].future_rate}% (${data.nextpasses[i].future_count}) ${future_text}">
                            </div>
                          </div>
                        </td>
                        <td class="pass-datetime">
                          <span class="pass-time">${tr_display_time}</span>
                          <span class="pass-date">${tr_display_date}</span>
                        </td>
                        <td class="pass-datetime">
                          <span class="pass-time">${ts_display_time}</span>
                          <span class="pass-date">${ts_display_date}</span>
                        </td>
                        <td class="max-elevation" data-max="${data.nextpasses[i].altt}">
                          <span class="polar-deg" aria-hidden="true"
                                data-toggle="tooltip" data-placement="left"
                                title="AOS degrees">
                              <span class="text-green">⤉</span> ${data.nextpasses[i].azr}°
                          </span>
                          <span class="polar-deg" aria-hidden="true"
                                data-toggle="tooltip" data-placement="left"
                                title="TCA degrees">
                              <span>⇴</span> ${data.nextpasses[i].altt}°
                          </span>
                          <span class="polar-deg" aria-hidden="true"
                                data-toggle="tooltip" data-placement="left"
                                title="LOS degrees">
                              <span class="text-red">⤈</span> ${data.nextpasses[i].azs}°
                          </span>
                        </td>
                        <td>
                          <div class="polar_plot">
                            <svg
                                xmlns="http://www.w3.org/2000/svg" version="1.1"
                                id="polar${i}"
                                data-tle1="${data.nextpasses[i].tle1}"
                                data-tle2="${data.nextpasses[i].tle2}"
                                data-timeframe-start="${tr_svg}"
                                data-timeframe-end="${ts_svg}"
                                data-groundstation-lat="${data.ground_station.lat}"
                                data-groundstation-lon="${data.ground_station.lng}"
                                data-groundstation-alt="${data.ground_station.alt}"
                                width=${polar_size} height=${polar_size}
                                viewBox="-110 -110 220 220"
                                overflow="hidden">
                                <path class="change_stroke"
                                    fill="none" stroke="white" stroke-width="1"
                                    d="M 0 -95 v 190 M -95 0 h 190"
                                    />
                                <circle class="change_stroke"
                                    fill="none" stroke="white"
                                    cx="0" cy="0" r="30"
                                    />
                                <circle class="change_stroke"
                                    fill="none" stroke="white"
                                    cx="0" cy="0" r="60"
                                    />
                                <circle class="change_stroke"
                                    fill="none" stroke="white"
                                    cx="0" cy="0" r="90"
                                    />
                                <text class="change_fill" x="-4" y="-96" fill="white">
                                    ${north_text}
                                </text>
                                <text class="change_fill" x="-4" y="105" fill="white">
                                    ${south_text}
                                </text>
                                <text  class="change_fill" x="96" y="4" fill="white">
                                    ${east_text}
                                </text>
                                <text class="change_fill" x="-106" y="4" fill="white">
                                    ${west_text}
                                </text>
                            </svg>
                          </div>
                        </td>
                        ${can_schedule ? `
                          <td class="pass-schedule">
                              ${schedulable ? `
                                <a href="${new_obs}?norad=${data.nextpasses[i].norad_cat_id}&ground_station=${station}&start=${tr}&end=${ts}"
                                   class="btn btn-sm"
                                   target="_blank">
                                   ${schedule_text}
                                  <span class="bi bi-box-arrow-up-right" aria-hidden="true"></span>
                                  ${overlap ? `
                                    <br>
                                    <div  class="badget badget-sm badget-red" aria-hidden="true" data-toggle="tooltip"
                                          title="${overlap_percent_text}">
                                      ${overlap}% ${overlap_text}
                                    </div>
                                  ` : `
                                  `}
                                </a>
                              ` : `
                                <a class="btn btn-sm disabled">
                                  ${schedule_text}
                                  <span class="bi bi-box-arrow-up-right" aria-hidden="true"></span>
                                  ${overlap ? `
                                    <br>
                                    <div  class="badget badget-sm badget-red" aria-hidden="true" data-toggle="tooltip"
                                          title="${overlap_percent_text}">
                                      ${overlap}% ${overlap_text}
                                    </div>
                                  ` : `
                                  `}
                                </a>
                              `}
                          </td>
                        ` : `
                        `}
                      </tr>
                    `);

                    // Draw orbit in polar plot
                    var tleLine1 = $('svg#polar' + i.toString()).data('tle1');
                    var tleLine2 = $('svg#polar' + i.toString()).data('tle2');

                    var timeframe = {
                        start: new Date($('svg#polar' + i.toString()).data('timeframe-start')),
                        end: new Date($('svg#polar' + i.toString()).data('timeframe-end'))
                    };

                    var groundstation = {
                        lon: $('svg#polar' + i.toString()).data('groundstation-lon'),
                        lat: $('svg#polar' + i.toString()).data('groundstation-lat'),
                        alt: $('svg#polar' + i.toString()).data('groundstation-alt')
                    };

                    const polarPlotSVG = calcPolarPlotSVG(timeframe,
                        groundstation,
                        tleLine1,
                        tleLine2);

                    $('svg#polar' + i.toString()).append(polarPlotSVG);
                    $('svg#polar' + i.toString() + ' path:last').css('stroke', '#0097ff');
                }

                // Show predicion results count
                $('#prediction-results').show();
                $('#prediction-results-count').html(data.nextpasses.length);
                filter_passes();
                $('#resTable').removeClass('d-none');
            },
            complete: function () {
                $('.loading-spinner').hide();
                $('#calculate-button').prop('disabled', false);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    $('#calculate-button').click(function () {
        calculate_predictions();
    });

    // Open all visible predictions for scheduling
    $('#open-all').click(function () {
        $('tr.pass:visible a.schedulable').each(function () {
            window.open($(this).attr('href'));
        });
    });

    // Expand Station image
    $('.img-expand').click(function () {
        $('#modal-lightbox').show('slow');
    });
    $('#modal-lightbox .close').click(function () {
        $('#modal-lightbox').hide('slow');
    });

    $('.status-filter').click(function() {
        filter_passes();
    });

    $('#freq-container label').click(function() {
        filter_passes();
    });

    $('.freq-filter').each(function(k, v) {
        var r = bands_from_range($(v).data('min'), $(v).data('max'));
        $(v).next().append('(' + r + ')');
    });
});

Fancybox.bind('[data-fancybox]', {
    Toolbar: {
        display: {
            right: ['close'],
        },
    },
    l10n: {
        CLOSE: gettext('Закрыть'),
        NEXT: gettext('Следующий'),
        PREV: gettext('Предыдущий'),
        MODAL: gettext('Вы можете закрыть окно нажатием ESC'),
        ERROR: gettext('Ошибка, попробуйте повторить попытку'),
        IMAGE_ERROR: gettext('Изображение не найдено'),
        ELEMENT_NOT_FOUND: gettext('HTML элемент не найден'),
        AJAX_NOT_FOUND: gettext('Ошибка загрузки AJAX: Не найдено'),
        AJAX_FORBIDDEN: gettext('Ошибка загрузки AJAX: Запрещено'),
        IFRAME_ERROR: gettext('Ошибка при загрузке фрейма'),
        TOGGLE_ZOOM: gettext('Приблизить'),
        TOGGLE_THUMBS: gettext('Миниатюры'),
        TOGGLE_SLIDESHOW: gettext('Слайдшоу'),
        TOGGLE_FULLSCREEN: gettext('Полноэкранный режим'),
        DOWNLOAD: gettext('Загрузить')
    }
});
