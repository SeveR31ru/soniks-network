"""Django base views for SONIKS autoscheduling"""

import json

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.utils.translation import gettext_lazy

from network.base.decorators import ajax_required
from network.base.models import (
    NetworkSchedule,
    Satellite,
    Station,
    StationSchedule,
    StationScheduleSec,
)
from network.base.perms import modify_delete_station_perms


# Планирование основного расписания станции
@ajax_required
def save_schedule(request):
    """
    Save primary schedule. Triggereds after user \\
    add new satellite in schedule or shuffle old
    """

    satellites_json = []
    station_id = request.POST.get("station", None)
    if station_id is None:
        data = {"error": gettext_lazy("Вы должны выбрать станцию")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Вы должны выбрать станцию")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    if not modify_delete_station_perms(request.user, station):
        data = {"error": gettext_lazy("У вас нет прав на изменение")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    satellites = request.POST.getlist("satellites[]", None)
    for satellite in satellites:
        sat = json.loads(satellite)
        if not Satellite.objects.filter(sat_id=sat["sat_id"]).exists():
            continue
        sat_dict = {}
        sat_dict["prio"] = sat["prio"]
        sat_dict["sat_name"] = sat["sat_name"]
        sat_dict["sat_id"] = sat["sat_id"]
        sat_dict["transmitter_uuid"] = sat["transmitter_uuid"]
        satellites_json.append(sat_dict)
    station_schedule, _ = StationSchedule.objects.get_or_create(station=station)
    station_schedule.satellites = satellites_json
    station_schedule.save()
    data = {"success": gettext_lazy("Основное расписание успешно сохранено")}
    return JsonResponse(data)


@ajax_required
def save_status(request):
    """
    Save new status of primary observation
    """
    station_id = request.POST.get("station", None)
    schedule_status = request.POST.get("status")
    if station_id is None:
        data = {"error": gettext_lazy("Вы должны выбрать станцию.")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Вы должны выбрать станцию.")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    if not modify_delete_station_perms(request.user, station):
        data = {"error": gettext_lazy("У вас нет прав на изменение")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    try:
        station_schedule = StationSchedule.objects.get(station=station)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Основное расписание ещё не создано")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    if schedule_status == "active":
        station_schedule.active = True
        output_status = gettext_lazy("Планирует")
    elif schedule_status == "inactive":
        station_schedule.active = False
        output_status = gettext_lazy("Не планирует")
    station_schedule.save()
    data = {
        "success": gettext_lazy(
            'Статус основного расписания успешно изменен на "{output_status}"'
        ).format(output_status=output_status)
    }
    return JsonResponse(data)


@ajax_required
def save_parameters(request):
    """
    Save_parameters for primary schedule
    """
    station_id = request.POST.get("station", None)
    params = request.POST.dict()
    if station_id is None:
        data = {"error": gettext_lazy("Ошибка во время определения id станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Неизвестная ошибка во время выбора станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    if not modify_delete_station_perms(request.user, station):
        data = {"error": gettext_lazy("У вас нет прав на изменение")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    try:
        station_schedule = StationSchedule.objects.get(station=station)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Расписание ещё не создано")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    param_dict = {}
    if int(params["split_duration"]) != settings.OBSERVATION_SPLIT_DURATION:
        param_dict["split"] = params["split_duration"]
    if int(params["break_duration"]) != settings.OBSERVATION_SPLIT_BREAK_DURATION:
        param_dict["break"] = params["break_duration"]
    if int(params["start_azimuth"]) != 0:
        param_dict["min_az"] = params["start_azimuth"]
    if int(params["end_azimuth"]) != 360:
        param_dict["max_az"] = params["end_azimuth"]
    if int(params["min_horizon"]) != station.horizon:
        param_dict["min_horizon"] = params["min_horizon"]
    if int(params["start_culmination"]) != 0:
        param_dict["start_culmination"] = params["start_culmination"]
    if int(params["end_culmination"]) != 90:
        param_dict["end_culmination"] = params["end_culmination"]
    station_schedule.param = param_dict
    station_schedule.save()
    data = {
        "success": gettext_lazy(
            "Новые параметры основного расписания параметры успешно сохранены"
        )
    }
    return JsonResponse(data)


def parameters_list(request):
    """
    Parameter list for primary schedule
    """
    station_id = request.GET.get("station", None)
    if station_id is None:
        data = {"error": gettext_lazy("Ошибка во время определения id станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Неизвестная ошибка во время выбора станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station_schedule = StationSchedule.objects.get(station=station)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Расписание ещё не создано")}
        return JsonResponse(data, status=404, json_dumps_params={"ensure_ascii": False})
    data = station_schedule.param
    return JsonResponse(data, safe=False)


# Планирование дополнительного расписания станции
@ajax_required
def save_schedule_sec(request):
    """
    Save second schedule
    """
    satellites_json = []
    station_id = request.POST.get("station", None)
    if station_id is None:
        data = {"error": gettext_lazy("Вы должны выбрать станцию")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Вы должны выбрать станцию")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    if not modify_delete_station_perms(request.user, station):
        data = {"error": gettext_lazy("У вас нет прав на изменение")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    satellites = request.POST.getlist("satellites[]", None)
    for satellite in satellites:
        sat = json.loads(satellite)
        if not Satellite.objects.filter(sat_id=sat["sat_id"]).exists():
            continue
        sat_dict = {}
        sat_dict["sat_name"] = sat["sat_name"]
        sat_dict["sat_id"] = sat["sat_id"]
        sat_dict["sat_name"] = sat["sat_name"]
        sat_dict["transmitter_uuid"] = sat["transmitter_uuid"]
        satellite = Satellite.objects.get(sat_id=sat_dict["sat_id"])
        satellites_json.append(sat_dict)
    station_schedule, _ = StationScheduleSec.objects.get_or_create(station=station)
    station_schedule.satellites = satellites_json
    station_schedule.save()
    data = {"success": gettext_lazy("Дополнительное расписание успешно сохранено")}
    return JsonResponse(data)


@ajax_required
def save_status_sec(request):
    """
    Save status for second schedule
    """
    station_id = request.POST.get("station", None)
    schedule_status = request.POST.get("status")
    if station_id is None:
        data = {"error": gettext_lazy("Вы должны выбрать станцию.")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Вы должны выбрать станцию.")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    if not modify_delete_station_perms(request.user, station):
        data = {"error": gettext_lazy("У вас нет прав на изменение")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    try:
        station_schedule = StationScheduleSec.objects.get(station=station)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Дополнительное расписание ещё не создано")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    if schedule_status == "active":
        station_schedule.active = True
        output_status = gettext_lazy("Планирует")
    elif schedule_status == "inactive":
        station_schedule.active = False
        output_status = gettext_lazy("Не планирует")
    station_schedule.save()
    data = {
        "success": gettext_lazy(
            'Статус дополнительного расписания успешно изменен на "{output_status}"'
        ).format(output_status=output_status)
    }
    return JsonResponse(data)


@ajax_required
def save_parameters_sec(request):
    """
    Save parameters for second schedule
    """
    station_id = request.POST.get("station", None)
    params = request.POST.dict()
    if station_id is None:
        data = {"error": gettext_lazy("Ошибка во время определения id станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Неизвестная ошибка во время выбора станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    if not modify_delete_station_perms(request.user, station):
        data = {"error": gettext_lazy("У вас нет прав на изменение")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    try:
        station_schedule = StationScheduleSec.objects.get(station=station)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Дополнительное расписание ещё не создано")}
        return JsonResponse(data, status=404, json_dumps_params={"ensure_ascii": False})
    param_dict = {}
    if int(params["split_duration"]) != settings.OBSERVATION_SPLIT_DURATION:
        param_dict["split"] = params["split_duration"]
    if int(params["break_duration"]) != settings.OBSERVATION_SPLIT_BREAK_DURATION:
        param_dict["break"] = params["break_duration"]
    if int(params["start_azimuth"]) != 0:
        param_dict["min_az"] = params["start_azimuth"]
    if int(params["end_azimuth"]) != 360:
        param_dict["max_az"] = params["end_azimuth"]
    if int(params["min_horizon"]) != station.horizon:
        param_dict["min_horizon"] = params["min_horizon"]
    if int(params["start_culmination"]) != 0:
        param_dict["start_culmination"] = params["start_culmination"]
    if int(params["end_culmination"]) != 90:
        param_dict["end_culmination"] = params["end_culmination"]
    station_schedule.param = param_dict
    station_schedule.save()
    data = {
        "success": gettext_lazy(
            "Новые параметры дополнительного расписания успешно сохранены"
        )
    }
    return JsonResponse(data)


def parameters_list_sec(request):
    """
    Parameter list for second schedule
    """
    station_id = request.GET.get("station", None)
    if station_id is None:
        data = {"error": gettext_lazy("Ошибка во время определения id станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Неизвестная ошибка во время выбора станции")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    try:
        station_schedule = StationScheduleSec.objects.get(station=station)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Расписание ещё не создано")}
        return JsonResponse(data, status=404, json_dumps_params={"ensure_ascii": False})
    data = station_schedule.param
    return JsonResponse(data, safe=False)


# Планирование сети
@ajax_required
def save_schedule_network(request):
    """
    Save schedule for network
    """
    if not request.user.is_staff:
        data = {"error": gettext_lazy("Вам не разрешено это делать")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    satellites_json = []
    satellites = request.POST.getlist("satellites[]", None)
    for satellite in satellites:
        sat = json.loads(satellite)
        if not Satellite.objects.filter(sat_id=sat["sat_id"]).exists():
            continue
        sat_dict = {}
        sat_dict["sat_name"] = sat["sat_name"]
        sat_dict["sat_id"] = sat["sat_id"]
        sat_dict["sat_name"] = sat["sat_name"]
        sat_dict["transmitter_uuid"] = sat["transmitter_uuid"]
        sat_dict["prio"] = sat["prio"]
        satellite = Satellite.objects.get(sat_id=sat_dict["sat_id"])
        satellites_json.append(sat_dict)
    network_schedule, _ = NetworkSchedule.objects.get_or_create(id=1)
    network_schedule.satellites = satellites_json
    network_schedule.save()
    data = {"success": gettext_lazy("Расписание сети успешно сохранено")}
    return JsonResponse(data)


@ajax_required
def save_status_network(request):
    """
    Save status for network schedule
    """
    if not request.user.is_staff:
        data = {"error": gettext_lazy("Вам не разрешено это делать")}
        return JsonResponse(data, status=403, json_dumps_params={"ensure_ascii": False})
    schedule_status = request.POST.get("status")
    try:
        network_schedule = NetworkSchedule.objects.get(id=1)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Расписание сети ещё не создано")}
        return JsonResponse(data, status=404, json_dumps_params={"ensure_ascii": False})
    if schedule_status == "active":
        network_schedule.active = True
        output_status = gettext_lazy("Планирует")
    elif schedule_status == "inactive":
        network_schedule.active = False
        output_status = gettext_lazy("Не планирует")
    network_schedule.save()
    data = {
        "success": gettext_lazy(
            'Статус планирования сети успешно изменен на "{output_status}"'
        ).format(output_status=output_status)
    }
    return JsonResponse(data)


@ajax_required
def save_parameters_network(request):
    """
    Save parameters for network schedule
    """

    if not request.user.is_staff:
        data = {"error": gettext_lazy("Вам не разрешено это делать")}
        return JsonResponse(data, safe=False)
    params = request.POST.dict()
    try:
        network_schedule = NetworkSchedule.objects.get(id=1)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Расписание ещё не создано")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    param_dict = {}
    if int(params["split_duration"]) != settings.OBSERVATION_SPLIT_DURATION:
        param_dict["split"] = params["split_duration"]
    if int(params["break_duration"]) != settings.OBSERVATION_SPLIT_BREAK_DURATION:
        param_dict["break"] = params["break_duration"]
    if int(params["start_azimuth"]) != 0:
        param_dict["min_az"] = params["start_azimuth"]
    if int(params["end_azimuth"]) != 360:
        param_dict["max_az"] = params["end_azimuth"]
    if int(params["min_horizon"]) != 0:
        param_dict["min_horizon"] = params["min_horizon"]
    if int(params["start_culmination"]) != 0:
        param_dict["start_culmination"] = params["start_culmination"]
    if int(params["end_culmination"]) != 90:
        param_dict["end_culmination"] = params["end_culmination"]
    network_schedule.param = param_dict
    network_schedule.save()
    data = {"success": gettext_lazy("Новые параметры сети успешно сохранены")}
    return JsonResponse(data)


def parameters_list_network(request):
    """
    Parameter list for network schedule
    """
    try:
        network_schedule = NetworkSchedule.objects.get(id=1)
    except ObjectDoesNotExist:
        data = {"error": gettext_lazy("Расписание ещё не создано")}
        return JsonResponse(data, status=400, json_dumps_params={"ensure_ascii": False})
    data = network_schedule.param
    return JsonResponse(data, safe=False)
