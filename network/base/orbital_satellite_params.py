"""SONIKS Network base orbital satellite params"""

import math
from datetime import datetime, timedelta

from skyfield.api import EarthSatellite, load


class OrbitalSatelliteParams:
    """
    Class to represent satellite orbital parameters
    """

    def __init__(self, satellite_name, tle1, tle2):
        self.satellite = EarthSatellite(name=satellite_name, line1=tle1, line2=tle2)
        self.radiusearthkm = self.satellite.model.radiusearthkm
        self.model_a = self.satellite.model.a
        self.eccentricity = self.satellite.model.ecco

    def get_apogee(self):
        """
        Get satellite current apogey by tle
        """
        axis = self.model_a * self.radiusearthkm
        apogee = (axis * (1 + self.eccentricity)) - self.radiusearthkm
        return apogee

    def get_perigee(self):
        """
        Get satellite current pegiree by tle
        """
        axis = self.model_a * self.radiusearthkm
        perigee = (axis * (1 - self.eccentricity)) - self.radiusearthkm
        return perigee

    def get_semi_axe(self):
        """
        Get satellite current semi-axe by tle
        """
        return self.model_a * self.radiusearthkm

    def get_inclination(self):
        """Get satellite current inclination by tle"""
        return self.satellite.model.inclo * 180 / math.pi

    def get_velocity(self):
        """
        Get satellite current velocity by tle
        """
        timescale = load.timescale()
        time = timescale.now()
        geocentric = self.satellite.at(time)
        velocity_x, velocity_y, velocity_z = geocentric.velocity.km_per_s
        velocity = (velocity_x**2 + velocity_y**2 + velocity_z**2) ** 0.5
        return velocity

    def get_period(self):
        """
        Get satellite current velocity by tle
        """
        mid_v = self.satellite.model.no_kozai
        mid_v_per_day = mid_v / 2 * math.pi * 144
        period_min = 24 * 60 / mid_v_per_day
        return period_min

    def get_altitude(self, time=None):
        """
        Get satellite current altutude by tle \\
        
        Args:
            time(Time): UTC-time from timescale.utc func
        """
        if time is None:
            timescale = load.timescale()
            time = timescale.now()
        geocentric = self.satellite.at(time)
        return geocentric.distance().km - self.radiusearthkm

    def get_decay_date(self):
        """
        Get satellite current decay date by tle \\
        Decay date means date when satellite altutide is lower then 220 km \\
        If date is more then 50 days away its too unpredictable and counted as unknown
        """
        date = datetime.now()
        current_altitude = 5000000
        count = 0
        timescale = load.timescale()
        while current_altitude > 220 and count <= 99:
            time = timescale.utc(date.year, date.month, date.day, 0, 0, 0)
            current_altitude = self.get_altitude(time)
            date += timedelta(hours=12)
            count += 1
        return None if count == 100 else date
