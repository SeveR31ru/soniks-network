"""Module for calculating and keep in cache satellite and transmitter statistics"""

import logging
import math
from datetime import datetime, timedelta, timezone
from typing import List

from django.core.cache import cache
from django.db.models.query import QuerySet
from django.utils.timezone import now
from django.utils.translation import gettext_lazy

from network.base.models import Observation, StationStatusLog, TransmitterStats
from network.users.models import User

TIMESINCE_CHUNKS = (
    (60 * 60 * 24 * 365, gettext_lazy("г.")),
    (60 * 60 * 24 * 30, gettext_lazy("мес.")),
    (60 * 60 * 24 * 7, gettext_lazy("нед.")),
    (60 * 60 * 24, gettext_lazy("дн.")),
    (60 * 60, gettext_lazy("ч.")),
    (60, gettext_lazy("мин.")),
    (1, gettext_lazy("сек.")),
)


def transmitters_with_stats(transmitters_list: List[dict]):
    """
    Returns a list of transmitters with their statistics

    Args:
        transmitters_list(list[dict]): List of transmitters dicts

    Returns:
        list[Dict]: list of transmitters dicts with statistics

    TODO: убрать, заменив на запрос к апи
    """
    transmitters_with_stats_list = []
    for transmitter in transmitters_list:
        transmitter_stats = transmitter_stats_by_uuid(transmitter["uuid"])
        transmitter_with_stats = dict(transmitter, **transmitter_stats)
        transmitters_with_stats_list.append(transmitter_with_stats)
    return transmitters_with_stats_list


def transmitter_stats_by_uuid(uuid: str):
    """
    Calculate transmitter statistics

    Args:
        uuid(str): UUID of transmitter which stats calculated

    Returns:
        dict: Dict with transmitter statistics

    TODO: убрать, заменив на запрос к апи
    """
    try:
        stats = TransmitterStats.objects.get(transmitter__uuid=uuid)
        total_count = stats.total_count - stats.failed_count
        good_count = stats.good_count
        bad_count = stats.bad_count
        future_count = Observation.objects.filter(
            transmitter__uuid=uuid, end__gt=now()
        ).count()
        unknown_count = stats.unknown_count - future_count
        if unknown_count < 0:
            unknown_count = 0
    except TransmitterStats.DoesNotExist:
        total_count = 0
        good_count = 0
        bad_count = 0
        future_count = 0
        unknown_count = 0

    if total_count != 0:
        unknown_rate = math.trunc(10000 * (unknown_count / total_count)) / 100
        future_rate = math.trunc(10000 * (future_count / total_count)) / 100
        success_rate = math.trunc(10000 * (good_count / total_count)) / 100
        bad_rate = math.trunc(10000 * (bad_count / total_count)) / 100
    else:
        unknown_rate = 0
        future_rate = 0
        success_rate = 0
        bad_rate = 0
    return {
        "total_count": total_count,
        "unknown_count": unknown_count,
        "future_count": future_count,
        "good_count": good_count,
        "bad_count": bad_count,
        "unknown_rate": unknown_rate,
        "future_rate": future_rate,
        "success_rate": success_rate,
        "bad_rate": bad_rate,
    }


def get_satellite_stats_by_transmitter_list(transmitter_list: QuerySet):
    """
    Calculate satellite statistics based on the statistics of its transmitters

    Args:
        transmitter_list(QuerySet): QuerySet of all satellite transmitters

    Returns:
        dict: Dict with satellite statistics

    TODO: убрать, заменив на запрос к апи
    """
    total_count = 0.0
    unknown_count = 0.0
    future_count = 0.0
    good_count = 0.0
    bad_count = 0.0
    unknown_rate = 0.0
    future_rate = 0.0
    success_rate = 0.0
    bad_rate = 0.0

    for transmitter in transmitter_list:
        transmitter_stats = transmitter_stats_by_uuid(transmitter.uuid)

        total_count += transmitter_stats["total_count"]
        unknown_count += transmitter_stats["unknown_count"]
        future_count += transmitter_stats["future_count"]
        good_count += transmitter_stats["good_count"]
        bad_count += transmitter_stats["bad_count"]
    if total_count:
        unknown_rate = math.trunc(10000 * (unknown_count / total_count)) / 100
        future_rate = math.trunc(10000 * (future_count / total_count)) / 100
        bad_rate = math.trunc(10000 * (bad_count / total_count)) / 100
        try:
            success_rate = (
                math.trunc(10000 * (good_count / (total_count - future_count))) / 100
            )
        except ZeroDivisionError:
            success_rate = 0.0

    return {
        "total_count": total_count,
        "unknown_count": unknown_count,
        "future_count": future_count,
        "good_count": good_count,
        "bad_count": bad_count,
        "unknown_rate": unknown_rate,
        "future_rate": future_rate,
        "success_rate": success_rate,
        "bad_rate": bad_rate,
    }


def unknown_observations_count(user: User):
    """
    Returns a count of unknown status observations per user

    Args:
        user(User): Instance of User which unknown observations counting

    Returns:
        int: Count of user unknown observation

    TODO: убрать, заменив на запрос к апи
    """
    user_unknown_count = cache.get("user-{0}-unknown-count".format(user.id))
    if user_unknown_count is None:
        user_unknown_count = (
            Observation.objects.filter(
                author=user, status__range=(0, 99), end__lte=now()
            )
            .exclude(waterfall="")
            .count()
        )
        cache.set("user-{0}-unknown-count".format(user.id), user_unknown_count, 120)

    return user_unknown_count


def display_time(seconds: float, granularity: int = 2) -> str:
    """
    Transform second into years,weeks etc.
    Args:
        seconds (int): sum of second to transform
        granularity (int, optional): A number indicating how small units the numbers need to
            be converted into. Defaults to 2.

    Returns:
        str: string with transformed seconds
    """
    result = []

    for count, name in TIMESINCE_CHUNKS:
        value = seconds // count
        if value:
            seconds -= value * count
            result.append("{} {}".format(int(value), name))
    return ", ".join(result[:granularity])


def station_work_time_count(
    station_id: int,
    start: datetime = datetime.now(timezone.utc) - timedelta(days=200000),
    end: datetime = datetime.now(timezone.utc) + timedelta(hours=2),
) -> str:
    """
    Calculate station work time in current period. If period isnt given \\
    then calculate all work time in network

    Args:
        station_id (int): Id of station which work time we calculated
        start (datetime, optional): Start of period. Defaults to Earth creating.
        end (datetime, optional): End of period. Defaults to Now + 2 hours.
    """
    station_logs = StationStatusLog.objects.filter(station__id=station_id).order_by(
        "-changed"
    )

    now = datetime.now(timezone.utc)
    period_end = min(now, end)
    total_online = 0.0
    for entry in station_logs:
        if period_end <= start:
            break

        if period_end > end:
            period_end = end

        if entry.status == 0:
            period_end = entry.changed
            continue

        if entry.changed >= end:
            continue

        period_start = max(entry.changed, start)
        total_online += (period_end - period_start).total_seconds()
        if period_start == start:
            break

        period_end = entry.changed

    return display_time(total_online, 5) if total_online else "0"
