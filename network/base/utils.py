"""Miscellaneous functions for SONIKS Network"""

import csv
import math
from builtins import str
from datetime import datetime
from decimal import Decimal
from typing import List, Optional, Union

import requests  # pylint: disable=C0412
from django.conf import settings
from django.contrib.admin.helpers import label_for_field
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied
from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from requests.exceptions import RequestException


def format_frequency_range(low, high):
    """
    Returns Hz formatted frequency range html string

    * Нигде не используется
    """
    formatted_low = format_frequency(low, append_unit=False)
    formatted_high = format_frequency(high, append_unit=True)
    if "-" in (formatted_low, formatted_high):
        return "-"
    return f"{formatted_low} - {formatted_high}"


def format_frequency(value: Union[int, str], append_unit: Optional[bool] = True):
    """
    Returns Hz formatted frequency html string

    Args:
        value(int,str): Value for formatting. Can be number or string
        append_unit(bool): Option to add unit measurement to frequency
            Default is True

    Returns:
        str: Formatted frequency

    """
    try:
        to_format = float(value)
    except (TypeError, ValueError):
        return "-"
    if to_format >= 1000000000000:
        formatted = format(to_format / 1000000000000, ".3f")
        formatted = formatted + (_(" ТГц") if append_unit else "")
    elif to_format >= 1000000000:
        formatted = format(to_format / 1000000000, ".3f")
        formatted = formatted + (_(" ГГц") if append_unit else "")
    elif to_format >= 1000000:
        formatted = format(to_format / 1000000, ".3f")
        formatted = formatted + (_(" МГц") if append_unit else "")
    elif to_format >= 1000:
        formatted = format(to_format / 1000, ".3f")
        formatted = formatted + (_(" КГц") if append_unit else "")
    else:
        formatted = format(to_format, ".3f")
        formatted = formatted + (_(" Гц") if append_unit else "")
    return formatted


def populate_formset_error_messages(messages, request, formset):
    """
    Add errors to django messages framework by extracting them from formset)
    """
    non_form_errors = formset.non_form_errors()
    if non_form_errors:
        messages.error(request, str(non_form_errors[0]))
        return
    for error in formset.errors:
        if error:
            for field in error:
                messages.error(request, str(error[field][0]))
        return


def bands_from_range(min_frequency: int, max_frequency: int):
    """
    Returns band names of the given frequency range based on
    https://www.itu.int/rec/R-REC-V.431-8-201508-I/en recommendation from ITU

    Args:
        min_frequency(int): minimal frequency of frequency range
        max_frequency(int): maximum frequency of frequency range

    Returns:
        list: List of bands names which in this frequency range
    """
    if max_frequency < min_frequency:
        return []

    frequency_bands = {
        "ULF": (300, 3000),
        "VLF": (3000, 30000),
        "LF": (30000, 300000),
        "MF": (300000, 3000000),
        "HF": (3000000, 30000000),
        "VHF": (30000000, 300000000),
        "UHF": (300000000, 1000000000),
        "L": (1000000000, 2000000000),
        "S": (2000000000, 4000000000),
        "C": (4000000000, 8000000000),
        "X": (8000000000, 12000000000),
        "Ku": (12000000000, 18000000000),
        "K": (18000000000, 27000000000),
        "Ka": (27000000000, 40000000000),
    }

    bands = []
    found_min = False

    for name, (min_freq, max_freq) in frequency_bands.items():
        if not found_min:
            if min_freq <= min_frequency <= max_freq:
                bands.append(name)
                if min_freq <= max_frequency <= max_freq:
                    return bands
                found_min = True
                continue
            continue
        bands.append(name)
        if min_freq < max_frequency <= max_freq:
            return bands
    return []


def export_as_csv(modeladmin, request, queryset):
    """
    Exports admin panel table in csv format
    """
    if not request.user.is_staff:
        raise PermissionDenied
    field_names = modeladmin.list_display
    if "action_checkbox" in field_names:
        field_names.remove("action_checkbox")

    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = "attachment; filename={}.csv".format(
        str(modeladmin.model._meta).replace(".", "_")
    )

    writer = csv.writer(response)
    headers = []
    for field_name in list(field_names):
        label = label_for_field(field_name, modeladmin.model, modeladmin)
        if label.islower():
            label = label.title()
        headers.append(label)
    writer.writerow(headers)
    for row in queryset:
        values = []
        for field in field_names:
            try:
                value = getattr(row, field)
            except AttributeError:
                value = getattr(modeladmin, field)
            if callable(value):
                try:
                    # get value from model
                    value = value()
                except TypeError:
                    # get value from modeladmin e.g: admin_method_1
                    value = value(row)
            if value is None:
                value = ""
            values.append(str(value).encode("utf-8"))
        writer.writerow(values)
    return response


def export_station_status(self, request, queryset):
    """
    Exports status of selected stations in csv format
    """
    meta = self.model._meta
    field_names = ["id", "status"]

    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = "attachment; filename={}.csv".format(meta)
    writer = csv.writer(response)

    writer.writerow(field_names)
    for obj in queryset:
        writer.writerow([getattr(obj, field) for field in field_names])

    return response


def community_get_discussion_details(
    observation_id, satellite_name, norad_cat_id, observation_url
):
    """
    Return the details of a discussion of the observation (if existent) in the
    soniks community (discourse)

    TODO: переделать функцию под наш форум, когда он будет создан
    """

    discussion_url = (
        "https://community.libre.space/new-topic?title=Observation {0}: {1}"
        " ({2})&body=Regarding [Observation {0}]({3}) ..."
        "&category=observations"
    ).format(observation_id, satellite_name, norad_cat_id, observation_url)

    discussion_slug = "https://community.libre.space/t/observation-{0}-{1}-{2}".format(
        observation_id, slugify(satellite_name), norad_cat_id
    )

    try:
        response = requests.get(
            "{}.json".format(discussion_slug), timeout=settings.COMMUNITY_TIMEOUT
        )
        response.raise_for_status()
        has_comments = response.status_code == 200
    except RequestException:
        # Community is unreachable
        has_comments = False

    return {
        "url": discussion_url,
        "slug": discussion_slug,
        "has_comments": has_comments,
    }


def sync_demoddata_to_db(frame):
    """
    Deprecated func for sync network with db

    TODO: удалить за ненадобностью
    """
    obs = frame.observation
    sat = obs.satellite
    ground_station = obs.ground_station

    try:
        # need to abstract the timestamp from the filename. hacky..
        file_datetime = frame.demodulated_data.name.split("/")[-1].split("_")[2]
        frame_datetime = datetime.strptime(file_datetime, "%Y-%m-%dT%H-%M-%S")
        submit_datetime = datetime.strftime(frame_datetime, "%Y-%m-%dT%H:%M:%S.000Z")
    except ValueError:
        return

    # SiDS parameters
    params = {
        "noradID": sat.norad_cat_id,
        "source": "Unknown",
        "timestamp": submit_datetime,
        "locator": "longLat",
        "longitude": obs.station.lng,
        "latitude": obs.station.lat,
        "frame": frame.display_payload_hex().replace(" ", ""),
        "soniks_network": "True",  # NOT a part of SiDS
        "observation_id": obs.id,  # NOT a part of SiDS
    }
    if ground_station:
        params["source"] = ground_station.name
        params["station_id"] = ground_station.id  # NOT a part of SiDS

    telemetry_url = f"{settings.DB_API_ENDPOINT}telemetry/"

    response = requests.post(
        telemetry_url, data=params, timeout=settings.DB_API_TIMEOUT
    )
    response.raise_for_status()

    frame.save()


def get_api_url():
    """
    Returns the URL of the API

    Returns:
        str: url of API
    """
    site = Site.objects.get_current()
    domain = site.domain

    if "https://" in domain or "http://" in domain:
        api_url = domain + "/api"
    elif "localhost" in domain:
        api_url = "http://" + domain + "/api"
    else:
        api_url = "https://" + domain + "/api"

    return api_url


def gridsquare(lat: float, lng: float):
    """
    Calculates a maidenhead grid square from a lat/long

    Used when we get a SiDS submission, we want to store and display the\\
    location of the submitter as a grid square.

    Args:
        lat(float): Latitude
        lng(float): Longtitude
    Returns:
        str: A string of the grid square, ie: EM69uf
    """
    upper = "ABCDEFGHIJKLMNOPQRSTUVWX"
    lower = "abcdefghijklmnopqrstuvwx"

    try:
        if not -180 <= lng < 180:
            return _("Неизвестно")
        if not -90 <= lat < 90:
            return _("Неизвестно")
    except TypeError:
        return _("Неизвестно")

    adj_lat = lat + 90.0
    adj_lon = lng + 180.0

    grid_lat_sq = upper[int(adj_lat / 10)]
    grid_lon_sq = upper[int(adj_lon / 20)]

    grid_lat_field = str(int(adj_lat % 10))
    grid_lon_field = str(int((adj_lon / 2) % 10))

    adj_lat_remainder = (adj_lat - int(adj_lat)) * 60
    adj_lon_remainder = ((adj_lon) - int(adj_lon / 2) * 2) * 60

    grid_lat_subsq = lower[int(adj_lat_remainder / 2.5)]
    grid_lon_subsq = lower[int(adj_lon_remainder / 5)]

    qth = "{}".format(
        grid_lon_sq
        + grid_lat_sq
        + grid_lon_field
        + grid_lat_field
        + grid_lon_subsq
        + grid_lat_subsq
    )

    return qth


def remove_exponent(converted_number: Decimal):
    """
    Remove exponent
    """
    return (
        converted_number.quantize(Decimal(1))
        if converted_number == converted_number.to_integral()
        else converted_number.normalize()
    )


def millify(number: int, precision: int = 0):
    """
    Humanize number
    """
    millnames = ["", "k", "M", "B", "T", "P", "E", "Z", "Y"]
    number = float(number)
    millidx = max(
        0,
        min(
            len(millnames) - 1,
            int(math.floor(0 if number == 0 else math.log10(abs(number)) / 3)),
        ),
    )
    result = "{:.{precision}f}".format(
        number / 10 ** (3 * millidx), precision=precision
    )
    result = remove_exponent(Decimal(result))
    return "{0}{dx}".format(result, dx=millnames[millidx])


def calculate_band_stats(transmitters: QuerySet):
    """
    Helper function to provide data and labels for bands associated with
    transmitters provided

    Args:
        transmitters(Queryset): Queryset of Transmitters
    """
    band_label = []
    band_data = []

    bands = [
        # <30.000.000 - HF
        {"lower_limit": 0, "upper_limit": 30000000, "label": "HF"},
        # 30.000.000 ~ 300.000.000 - VHF
        {"lower_limit": 30000000, "upper_limit": 300000000, "label": "VHF"},
        # 300.000.000 ~ 1.000.000.000 - UHF
        {
            "lower_limit": 300000000,
            "upper_limit": 1000000000,
            "label": "UHF",
        },
        # 1G ~ 2G - L
        {
            "lower_limit": 1000000000,
            "upper_limit": 2000000000,
            "label": "L",
        },
        # 2G ~ 4G - S
        {
            "lower_limit": 2000000000,
            "upper_limit": 4000000000,
            "label": "S",
        },
        # 4G ~ 8G - C
        {
            "lower_limit": 4000000000,
            "upper_limit": 8000000000,
            "label": "C",
        },
        # 8G ~ 12G - X
        {
            "lower_limit": 8000000000,
            "upper_limit": 12000000000,
            "label": "X",
        },
        # 12G ~ 18G - Ku
        {
            "lower_limit": 12000000000,
            "upper_limit": 18000000000,
            "label": "Ku",
        },
        # 18G ~ 27G - K
        {
            "lower_limit": 18000000000,
            "upper_limit": 27000000000,
            "label": "K",
        },
        # 27G ~ 40G - Ka
        {
            "lower_limit": 27000000000,
            "upper_limit": 40000000000,
            "label": "Ka",
        },
    ]

    for band in bands:
        filtered = transmitters.filter(
            downlink_low__gte=band["lower_limit"], downlink_low__lt=band["upper_limit"]
        ).count()
        band_label.append(band["label"])
        band_data.append(filtered)

    band_data_sorted, band_label_sorted = list(
        zip(*sorted(zip(band_data, band_label), reverse=True))
    )

    return band_label_sorted, band_data_sorted


def get_dict_from_list(dicts: List[dict], key: str, value_to_compare: str):
    """
    Get a dict from list by a key

    Args:
        dicts(list): List of dicts
        key(str): Key which values compared
        value_to_compare(str): Value which compairing with key value

    Returns:
        dict or None: Dict with such value of dict or None if it doesnt exist
    """
    _dict: dict
    for _dict in dicts:
        if _dict.get(key, None) == value_to_compare:
            return _dict
    return None
