f"""Django template tags for SONIKS Network"""

from datetime import date, datetime, timezone
from hashlib import md5
from typing import Optional

from django import template
from django.template import defaultfilters
from django.urls import reverse
from django.utils.timesince import timesince
from django.utils.timezone import is_aware
from django.utils.translation import (
    gettext_lazy,
    ngettext,
    ngettext_lazy,
    npgettext_lazy,
)
from django.utils.translation import ngettext_lazy as _

from network.base.models import DemodData, Satellite, Transmitter
from network.base.stats import station_work_time_count
from network.base.utils import format_frequency
from network.base.validators import is_transmitter_in_station_range

register = template.Library()

TIME_STRINGS = {
    "year": ngettext_lazy("%(num)d г.", "%(num)d г.", "num"),
    "month": ngettext_lazy("%(num)d мес.", "%(num)d мес.", "num"),
    "week": ngettext_lazy("%(num)d нед.", "%(num)d нед.", "num"),
    "day": ngettext_lazy("%(num)d д.", "%(num)d д.", "num"),
    "hour": ngettext_lazy("%(num)d ч.", "%(num)d ч.", "num"),
    "minute": ngettext_lazy("%(num)d мин.", "%(num)d мин.", "num"),
}


# TEMPLATE USE:  {{ email|gravatar_url:150 }}
@register.filter
def gravatar_url(email, size=40):
    """
    Returns the Gravatar URL based on user's email address
    """
    return "https://www.gravatar.com/avatar/%s?s=%s" % (
        md5(email.lower().encode("utf-8")).hexdigest(),
        str(size),
    )


@register.filter(name="not_true")
def not_true(value):
    """
    A filter to be used as a not-equals operator
    """
    return not value


@register.simple_tag
def active(request, urls):
    """
    Returns if this is an active URL
    """
    if request.path in (reverse(url) for url in urls.split()):
        return "active"
    return None


@register.simple_tag
def drifted_frq(value: int, drift: int):
    """
    Returns drifted frequency

    Args:
        value(int): frequency value
        drift(int): frequency drift

    Returns:
        int: drifted frequency
    """
    return int(round(value + ((value * drift) / 1e9)))


@register.filter
def sort_types(types):
    """
    Returns sorted 'Other' antenna types

    Args:
        types(AntennaTypes): Queryset of all AntennaTypes

    Returns:
        list:sorted_types plus other types
    """
    other = []
    sorted_types = []
    for antenna_type in types:
        if "Other" in antenna_type.name:
            other.append(antenna_type)
            continue
        sorted_types.append(antenna_type)
    return sorted_types + other


@register.filter
def frq(value: int):
    """
    Returns Hz formatted frequency html string

    Args:
        value(int): Hz for formatting

    Returns
        str:Formatted frequency
    """
    return format_frequency(value)


@register.filter
def percentagerest(value):
    """
    Returns the rest of percentage from a given (percentage) value

    Тег нигде не используется, входное значение не отследить

    Args:
        value(float?): Value of percentage

    Returns:
        float?: Rest of percentage from a given value
    """
    try:
        return 100 - value
    except (TypeError, ValueError):
        return 0


def get_demoddata_filenames(demoddata: DemodData):
    """
    Returns the filename of a demoddata file

    Args:
        demoddata(Demoddata): object of Demoddata model

    Returns:
        str: file name of demoddata
    """
    return demoddata.demodulated_data.name.split("/")[-1]


@register.filter
def lookup_with_key(dictionary: dict, key: str):
    """
    Returns a value from dictionary for a given key

    Args:
        dictionary(dict): Dictionary for key searching
    Returns:
        any: Given key or None
    """
    return dictionary.get(key)


@register.simple_tag
def check_transmitter_schedule(station, schedule_satellite_json):
    """
    tag for satellite_table in station_view for checking if \\
    transmitter is no longer in frequency range if station \\
    change her antennas or work frequency

    Args:
        station (Station): Station model
        schedule_satellite_json (list[dict]): List of dicts with transmitters

    Returns:
        dict: Return transmitter dict with bool values
    """
    transmitters_uuids = []
    for satellite in schedule_satellite_json:
        transmitters_uuids.append(satellite["transmitter_uuid"])
    transmitters_objects = Transmitter.objects.filter(uuid__in=transmitters_uuids).only(
        "uuid",
        "type",
        "downlink_high",
        "downlink_low",
    )
    transmitters_dict = {}
    for transmitter in transmitters_objects:
        if is_transmitter_in_station_range(transmitter, station):
            transmitters_dict[f"{transmitter.uuid}"] = True
        else:
            transmitters_dict[f"{transmitter.uuid}"] = False
    return transmitters_dict


@register.simple_tag
def check_if_sat_is_dead(schedule_satellite_json):
    """
    Tag for satellite_table in station_view for checking if
    satellite is decayed

    Arguments:
        schedule_satellite_json (list[dict]): List of dicts with transmitters

    Return:
        dict: Return satellite dict with bool values
    """
    satellites_id = []
    for satellite in schedule_satellite_json:
        satellites_id.append(satellite["sat_id"])
    satellite_objects = Satellite.objects.filter(sat_id__in=satellites_id).only(
        "sat_id",
        "id",
        "status",
    )
    satellite_dict = {}
    for satellite in satellite_objects:
        if satellite.status == "re-entered" or satellite.status == "dead":
            satellite_dict[f"{satellite.sat_id}"] = False
        else:
            satellite_dict[f"{satellite.sat_id}"] = True
    return satellite_dict


@register.filter("timesince_our", is_safe=False)
def timesince_filter(value, arg=None):
    """Formats a date as the time since that date (i.e. "4 days, 6 hours")."""
    if not value:
        return ""
    try:
        if arg:
            return timesince(d=value, now=arg, time_strings=TIME_STRINGS)
        return timesince(d=value, time_strings=TIME_STRINGS)
    except (ValueError, TypeError):
        return ""


@register.filter("naturaltime_our")
def naturaltime(value):
    """
    For date and time values show how many seconds, minutes, or hours ago
    compared to current timestamp return representing string.
    """
    return NaturalTimeFormatter.string_for(value)


class NaturalTimeFormatter:
    time_strings = {
        # Translators: delta will contain a string like '2 months' or '1 month, 2 weeks'
        "past-day": gettext_lazy("%(delta)s назад"),
        # Translators: please keep a non-breaking space (U+00A0) between count
        # and time unit.
        "past-hour": ngettext_lazy("час назад", "%(count)s ч. назад", "count"),
        # Translators: please keep a non-breaking space (U+00A0) between count
        # and time unit.
        "past-minute": ngettext_lazy("минуту назад", "%(count)s мин. назад", "count"),
        # Translators: please keep a non-breaking space (U+00A0) between count
        # and time unit.
        "past-second": ngettext_lazy("секунду назад", "%(count)s сек. назад", "count"),
        "now": gettext_lazy("сейчас"),
        # Translators: please keep a non-breaking space (U+00A0) between count
        # and time unit.
        "future-second": ngettext_lazy(
            "через секунду", "через %(count)s сек.", "count"
        ),
        # Translators: please keep a non-breaking space (U+00A0) between count
        # and time unit.
        "future-minute": ngettext_lazy("через минуту", "через %(count)s мин.", "count"),
        # Translators: please keep a non-breaking space (U+00A0) between count
        # and time unit.
        "future-hour": ngettext_lazy("через час", "через %(count)s ч.", "count"),
        # Translators: delta will contain a string like '2 months' or '1 month, 2 weeks'
        "future-day": gettext_lazy("через %(delta)s"),
    }
    past_substrings = {
        # Translators: 'naturaltime-past' strings will be included in '%(delta)s ago'
        "year": npgettext_lazy("naturaltime-past", "%(num)d г.", "%(num)d г.", "num"),
        "month": npgettext_lazy(
            "naturaltime-past", "%(num)d мес.", "%(num)d мес.", "num"
        ),
        "week": npgettext_lazy(
            "naturaltime-past", "%(num)d нед.", "%(num)d нед.", "num"
        ),
        "day": npgettext_lazy("naturaltime-past", "%(num)d дн.", "%(num)d дн.", "num"),
        "hour": npgettext_lazy("naturaltime-past", "%(num)d ч.", "%(num)d ч.", "num"),
        "minute": npgettext_lazy(
            "naturaltime-past", "%(num)d мин.", "%(num)d мин.", "num"
        ),
    }
    future_substrings = {
        # Translators: 'naturaltime-future' strings will be included in
        # '%(delta)s from now'.
        "year": npgettext_lazy("naturaltime-future", "%(num)d г.", "%(num)d г.", "num"),
        "month": npgettext_lazy(
            "naturaltime-future", "%(num)d мес.", "%(num)d мес.", "num"
        ),
        "week": npgettext_lazy(
            "naturaltime-future", "%(num)d нед.", "%(num)d нед.", "num"
        ),
        "day": npgettext_lazy("naturaltime-future", "%(num)d д.", "%(num)d д.", "num"),
        "hour": npgettext_lazy("naturaltime-future", "%(num)d ч.", "%(num)d ч.", "num"),
        "minute": npgettext_lazy(
            "naturaltime-future", "%(num)d мин.", "%(num)d мин.", "num"
        ),
    }

    @classmethod
    def string_for(cls, value):
        if not isinstance(value, date):  # datetime is a subclass of date
            return value

        now = datetime.now(timezone.utc if is_aware(value) else None)
        if value < now:
            delta = now - value
            if delta.days != 0:
                return cls.time_strings["past-day"] % {
                    "delta": defaultfilters.timesince(
                        value, now, time_strings=cls.past_substrings
                    ),
                }
            elif delta.seconds == 0:
                return cls.time_strings["now"]
            elif delta.seconds < 60:
                return cls.time_strings["past-second"] % {"count": delta.seconds}
            elif delta.seconds // 60 < 60:
                count = delta.seconds // 60
                return cls.time_strings["past-minute"] % {"count": count}
            else:
                count = delta.seconds // 60 // 60
                return cls.time_strings["past-hour"] % {"count": count}
        else:
            delta = value - now
            if delta.days != 0:
                return cls.time_strings["future-day"] % {
                    "delta": defaultfilters.timeuntil(
                        value, now, time_strings=cls.future_substrings
                    ),
                }
            elif delta.seconds == 0:
                return cls.time_strings["now"]
            elif delta.seconds < 60:
                return cls.time_strings["future-second"] % {"count": delta.seconds}
            elif delta.seconds // 60 < 60:
                count = delta.seconds // 60
                return cls.time_strings["future-minute"] % {"count": count}
            else:
                count = delta.seconds // 60 // 60
                return cls.time_strings["future-hour"] % {"count": count}


@register.simple_tag
def display_sum_of_worktime(station_id: int, start: str = None, end: str = None) -> str:
    """
    Tag for displaying sum of station work time
    Args:
        station_id (int): ID of station which worktime we calculating

    Returns:
        str: string with station sum work time
    """
    date_format = "%Y-%m-%d %H:%M"
    if start and end:
        start_date = datetime.strptime(start, date_format).replace(tzinfo=timezone.utc)
        end_date = datetime.strptime(end, date_format).replace(tzinfo=timezone.utc)
        worktime = station_work_time_count(
            station_id=station_id, start=start_date, end=end_date
        )
    elif start:
        start_date = datetime.strptime(start, date_format).replace(tzinfo=timezone.utc)
        worktime = station_work_time_count(station_id=station_id, start=start_date)
    elif end:
        end_date = datetime.strptime(end, date_format).replace(tzinfo=timezone.utc)
        worktime = station_work_time_count(station_id=station_id, end=end_date)
    else:
        worktime = station_work_time_count(station_id=station_id)
    return worktime


@register.simple_tag
def check_sat_exist(sat_id: str) -> Optional[str]:
    """
    Check if satellite with given ID exists
    Args:
        sat_id (str): ID of satellite which we are checking

    Returns:
        Optional[str]: The name of the deployer satellite or None
    """

    try:
        deployer = Satellite.objects.get(sat_id=sat_id)
        deployer_name = deployer.name
    except Satellite.DoesNotExist:
        deployer_name = None

    return deployer_name
