from django.db import migrations, models
from jsonschema import Draft202012Validator


def create_rf_configurations(apps, schema_editor):
    StationType = apps.get_model('base', 'StationType')
    StationConfigurationSchema = apps.get_model('base', 'StationConfigurationSchema')

    station_type = StationType(name='RF')
    station_type.save()

    basic_schema = {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "https://network.satnogs.org/static/rf_basic_configuration_schema",
        "title": "SatNOGS Basic RF Station Configuration",
        "description": "The basic configuration schema for a SatNOGS RF Station",
        "type": "object",
        "properties": {
            "Location Configuration": {
                "description": "Configuration related to Station's Location",
                "type": "object",
                "properties": {
                    "station_elev": {
                        "title": "Station Elevation",
                        "description": "Station Elevation above sea level in meters",
                        "type": "integer",
                        "minimum": -500
                    },
                    "station_lat": {
                        "title": "Station Latitude",
                        "description": "Station Latitude in decimal degrees",
                        "type": "number",
                        "minimum": -90,
                        "maximum": 90
                    },
                    "station_lon": {
                        "title": "Station Longitude",
                        "description": "Station Longitude in decimal degrees",
                        "type": "number",
                        "minimum": -180,
                        "maximum": 180
                    }
                },
                "required": [
                    "station_elev",
                    "station_lat",
                    "station_lon"
                ]
            },
            "Soapy Configuration": {
                "description": "Configuration to set Soapy parameters",
                "type": "object",
                "properties": {
                    "soapy_rx_device": {
                        "title": "Soapy RX Device",
                        "description": "Soapy RX Device",
                        "type": "string",
                        "maxLength": 40,
                        "default": "driver=rtlsdr"
                    },
                    "soapy_rx_antenna": {
                        "title": "Soapy RX Device Antenna",
                        "description": "Soapy RX Device Antenna",
                        "type": "string",
                        "maxLength": 40,
                        "default": "RX"
                    },
                    "soapy_rx_samp_rate": {
                        "title": "Soapy RX Sampling Rate",
                        "description": "Soapy RX sampling rate in samples per second(sps)",
                        "type": "integer",
                        "minimum": 0,
                        "default": 2048000.0
                    },
                    "soapy_rx_rf_gain": {
                        "title": "Soapy RX Device RF Gain",
                        "description": "Soapy RX Device RF Gain",
                        "type": "number",
                        "minimum": 0,
                        "default": 32.8
                    }
                },
                "required": [
                    "soapy_rx_device",
                    "soapy_rx_antenna",
                    "soapy_rx_samp_rate",
                    "soapy_rx_rf_gain"
                ]
            }
        },
        "required": [
            "Location Configuration",
            "Soapy Configuration"
        ]
    }


    basic_station_configuration_schema = StationConfigurationSchema(
        name='Basic Configuration', station_type=station_type, schema=basic_schema
    )
    basic_station_configuration_schema.save()

    unregistered_schema = {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "https://network.satnogs.org/static/rf_basic_configuration_schema",
        "title": "SatNOGS Unregistered RF Station Configuration",
        "description": "The unregistered configuration schema for a SatNOGS RF Station",
        "type": "object",
        "properties": {
            "Location Configuration": {
                "description": "Configuration related to Station's Location",
                "type": "object",
                "properties": {
                    "station_elev": {
                        "title": "Station Elevation",
                        "description": "Station Elevation above sea level in meters",
                        "type": "integer",
                        "minimum": -500
                    },
                    "station_lat": {
                        "title": "Station Latitude",
                        "description": "Station Latitude in decimal degrees",
                        "type": "number",
                        "minimum": -90,
                        "maximum": 90
                    },
                    "station_lon": {
                        "title": "Station Longitude",
                        "description": "Station Longitude in decimal degrees",
                        "type": "number",
                        "minimum": -180,
                        "maximum": 180
                    }
                },
                "required": [
                    "station_elev",
                    "station_lat",
                    "station_lon"
                ]
            }
        },
        "required": [
            "Location Configuration"
        ]
    }

    unregistered_station_configuration_schema = StationConfigurationSchema(
        name='Unregistered Configuration', station_type=station_type, schema=unregistered_schema
    )
    unregistered_station_configuration_schema.save()


def reverse_create_rf_configurations(apps, schema_editor):
    StationType = apps.get_model('base', 'StationType')
    # Remove StationType 'RF' and due to cascade it removes both StationConfigurationSchema,
    # 'Basic Configuration' and 'Unregistered Configuration'
    StationType.objects.filter(name='RF').delete()


def migrate_data_of_stations(apps, schema_editor):
    Station = apps.get_model('base', 'Station')
    StationConfiguration = apps.get_model('base', 'StationConfiguration')
    StationConfigurationSchema = apps.get_model('base', 'StationConfigurationSchema')

    basic_configuration_schema = StationConfigurationSchema.objects.get(name='Basic Configuration')
    unregistered_configuration_schema = StationConfigurationSchema.objects.get(name='Unregistered Configuration')
    stations = Station.objects.all()

    for station in stations:
        name = 'Unregistered'
        configuration_json = {
            "Location Configuration": {
                "station_elev": station.alt or 0,
                "station_lat": station.lat or 0,
                "station_lon": station.lng or 0
            }
        }
        configuration_schema = unregistered_configuration_schema

        if station.client_id:
            name = 'Basic'
            configuration_json = {
                "Location Configuration": {
                    "station_elev": station.alt or 0,
                    "station_lat": station.lat or 0,
                    "station_lon": station.lng or 0
                },
                "Soapy Configuration": {
                    "soapy_rx_device": station.satnogs_soapy_rx_device or 'driver=rtlsdr',
                    "soapy_rx_antenna": station.satnogs_antenna or 'RX',
                    "soapy_rx_samp_rate": station.satnogs_rx_samp_rate or 2048000,
                    "soapy_rx_rf_gain": station.satnogs_rf_gain or 32.8
                }
            }
            configuration_schema = basic_configuration_schema

        try:
            Draft202012Validator(configuration_schema.schema).validate(configuration_json)
        except:
            continue

        station_configuration = StationConfiguration(
            name=name, station=station, schema=configuration_schema,
            configuration=configuration_json, active=True
        )
        station_configuration.save()


def reverse_migrate_data_of_stations(apps, schema_editor):
    StationConfiguration = apps.get_model('base', 'StationConfiguration')
    StationConfiguration.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_stationtype_remove_station_satnogs_antenna_and_more'),
    ]

    operations = [
        migrations.RunPython(create_rf_configurations, reverse_create_rf_configurations),
        migrations.RunPython(migrate_data_of_stations, reverse_migrate_data_of_stations),
    ]
