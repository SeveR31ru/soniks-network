"""SONIKS Network API serializers, django rest framework"""

import json
import logging
import re

#  pylint: disable=no-self-use
from collections import defaultdict
from typing import Dict, Union

from django.db.models.query import QuerySet
from django.utils import translation
from django.utils.translation import gettext as _
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from PIL import Image
from rest_framework import serializers

logger = logging.getLogger(__name__)

from network.base.models import (
    Antenna,
    DemodData,
    FrequencyRange,
    LatestTleSet,
    Mode,
    Observation,
    Satellite,
    Station,
    Tle,
    Transmitter,
    TransmitterStats,
)
from network.base.perms import (
    UserNoPermissionError,
    check_schedule_perms_of_violators_per_station,
    check_schedule_perms_per_station,
)
from network.base.scheduling import create_new_observation
from network.base.validators import (
    ObservationOverlapError,
    OutOfRangeError,
    check_end_datetime,
    check_overlaps,
    check_start_datetime,
    check_start_end_datetimes,
    check_transmitter_station_pairs,
    check_violators_scheduling_limit,
)


class CreateDemodDataSerializer(serializers.ModelSerializer):
    """
    SONIKS Network DemodData API Serializer for creating demoddata
    """

    class Meta:
        model = DemodData
        fields = (
            "observation",
            "demodulated_data",
            "satellite",
            "station",
            "timestamp",
            "observer",
            "lat",
            "lng",
        )

    def create(self, validated_data):
        """
        Creates demoddata from a list of validated data after checking if \\
        demodulated_data is an image and add the result in is_image field
        """
        try:
            image = Image.open(validated_data["demodulated_data"])
            image.verify()
            validated_data["is_image"] = True
        except Exception:  # pylint: disable=W0703
            validated_data["is_image"] = False
        return DemodData.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Updates demoddata from a list of validated data\\
        currently disabled and returns None
        """
        return None


class DemodDataSerializer(serializers.ModelSerializer):
    """
    SONIKS Network DemodData API Serializer
    """

    demodulated_data = serializers.SerializerMethodField()

    class Meta:
        model = DemodData
        fields = ("demodulated_data",)

    def get_demodulated_data(self, obj):
        """
        Returns DemodData Link
        """
        request = self.context.get("request")
        if obj.demodulated_data:
            return request.build_absolute_uri(obj.demodulated_data.url)
        return None


class TleSerializer(serializers.ModelSerializer):
    """
    Tle API Serializer
    """

    class Meta:
        model = Tle
        fields = ("tle0", "tle1", "tle2", "updated", "tle_source")


class UpdateObservationSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Observation API Serializer for uploading audio and waterfall. \\
    This is Serializer is used temporarily until waterfall_old and payload_old fields are removed
    TODO: Убрать окончательно этот Сериализатор- поля у нас уже убраны
    """

    class Meta:
        model = Observation
        fields = ("id", "payload", "waterfall", "client_metadata", "client_version")


class ObservationSerializer(serializers.ModelSerializer):  # pylint: disable=R0904
    """
    SONIKS Network Observation API Serializer

    TODO: Убрать Deprecated поля
    """

    transmitter = serializers.SerializerMethodField()
    transmitter_updated = serializers.SerializerMethodField()
    norad_cat_id = serializers.SerializerMethodField()
    payload = serializers.SerializerMethodField()
    waterfall = serializers.SerializerMethodField()
    station_name = serializers.SerializerMethodField()
    station_lat = serializers.SerializerMethodField()
    station_lng = serializers.SerializerMethodField()
    station_alt = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    waterfall_status = serializers.SerializerMethodField()
    vetted_status = serializers.SerializerMethodField()  # Deprecated
    vetted_user = serializers.SerializerMethodField()  # Deprecated
    vetted_datetime = serializers.SerializerMethodField()  # Deprecated
    demoddata = DemodDataSerializer(required=False, many=True)
    tle = TleSerializer()
    observer = serializers.SerializerMethodField()
    center_frequency = serializers.SerializerMethodField()
    observation_frequency = serializers.SerializerMethodField()
    transmitter_status = serializers.SerializerMethodField()
    transmitter_unconfirmed = serializers.SerializerMethodField()
    image_count = serializers.SerializerMethodField()
    demoddata_count = serializers.SerializerMethodField()

    class Meta:
        model = Observation
        fields = (
            "id",
            "start",
            "end",
            "ground_station",
            "transmitter",
            "norad_cat_id",
            "payload",
            "waterfall",
            "demoddata",
            "station_name",
            "station_lat",
            "station_lng",
            "station_alt",
            "vetted_status",
            "vetted_user",
            "vetted_datetime",
            "archived",
            "archive_url",
            "client_version",
            "client_metadata",
            "status",
            "waterfall_status",
            "waterfall_status_user",
            "waterfall_status_datetime",
            "rise_azimuth",
            "set_azimuth",
            "max_altitude",
            "tle",
            "center_frequency",
            "observer",
            "observation_frequency",
            "transmitter_status",
            "transmitter_unconfirmed",
            "transmitter_updated",
            "image_count",
            "demoddata_count",
        )
        read_only_fields = [
            "id",
            "start",
            "end",
            "observation",
            "ground_station",
            "transmitter",
            "norad_cat_id",
            "archived",
            "archive_url",
            "station_name",
            "station_lat",
            "station_lng",
            "waterfall_status_user",
            "status",
            "waterfall_status",
            "station_alt",
            "vetted_status",
            "vetted_user",
            "vetted_datetime",
            "waterfall_status_datetime",
            "rise_azimuth",
            "set_azimuth",
            "max_altitude",
            "tle",
            "observer",
            "center_frequency",
            "observation_frequency",
        ]

    def update(self, instance, validated_data):
        """
        Updates observation object with validated data
        """
        super().update(instance, validated_data)
        return instance

    def get_observation_frequency(self, obj):
        """
        Returns observation center frequency
        """
        frequency = obj.center_frequency or obj.transmitter.downlink_low
        frequency_drift = obj.transmitter.downlink_drift
        if obj.center_frequency or frequency_drift is None:
            return frequency
        return int(round(frequency + ((frequency * frequency_drift) / 1e9)))

    def get_transmitter_unconfirmed(self, obj):
        """
        Returns whether the transmitter was unconfirmed at the time of observation
        """
        return obj.transmitter.unconfirmed

    def get_transmitter_status(self, obj):
        """
        Returns the status of the transmitter at the time of observation
        """
        if obj.transmitter.status:
            return "active"
        if obj.transmitter.status is not None:
            return "inactive"
        return "unknown"

    def get_center_frequency(self, obj):
        """
        Returns observation center frequency
        """
        return obj.center_frequency

    def get_transmitter(self, obj):
        """
        Returns Transmitter UUID
        """
        try:
            return obj.transmitter.uuid
        except AttributeError:
            return ""

    def get_transmitter_updated(self, obj):
        """
        Returns Transmitter last update date
        """
        try:
            return obj.transmitter.created
        except AttributeError:
            return ""

    def get_norad_cat_id(self, obj):
        """
        Returns Satellite NORAD ID
        """
        return obj.satellite.norad_cat_id

    def get_payload(self, obj):
        """
        Returns Audio Link
        """
        request = self.context.get("request")
        if obj.payload:
            return request.build_absolute_uri(obj.payload.url)
        return None

    def get_waterfall(self, obj):
        """
        Returns Watefall Link
        """
        request = self.context.get("request")

        if obj.waterfall:
            return request.build_absolute_uri(obj.waterfall.url)
        return None

    def get_station_name(self, obj):
        """
        Returns Station name
        """
        try:
            return obj.ground_station.name
        except AttributeError:
            return None

    def get_station_lat(self, obj):
        """
        Returns Station latitude
        """
        try:
            return obj.ground_station.lat
        except AttributeError:
            return None

    def get_station_lng(self, obj):
        """
        Returns Station longitude
        """
        try:
            return obj.ground_station.lng
        except AttributeError:
            return None

    def get_station_alt(self, obj):
        """
        Returns Station elevation
        """
        try:
            return obj.ground_station.alt
        except AttributeError:
            return None

    def get_status(self, obj):
        """
        Returns Observation status
        """
        return obj.status_badge

    def get_waterfall_status(self, obj):
        """
        Returns Observation status
        """
        return obj.waterfall_status_badge

    def get_vetted_status(self, obj):
        """
        DEPRECATED: Returns vetted status

        TODO: удалить метод
        """
        if obj.status_badge == "future":
            return "unknown"
        return obj.status_badge

    def get_vetted_user(self, obj):
        """
        DEPRECATED: Returns vetted user

        TODO: удалить метод
        """
        if obj.waterfall_status_user:
            return obj.waterfall_status_user.pk
        return None

    def get_vetted_datetime(self, obj):
        """
        DEPRECATED: Returns vetted datetime

        TODO: удалить метод
        """
        return obj.waterfall_status_datetime

    def get_tle(self, obj):
        """
        Return TLE
        """
        if obj.tle:
            return obj.tle
        return None

    def get_tle0(self, obj):
        """
        Returns tle0
        """
        if obj.tle:
            if obj.tle.tle0:
                return obj.tle.tle0
        return None

    def get_tle1(self, obj):
        """
        Returns tle1
        """
        if obj.tle:
            if obj.tle.tle1:
                return obj.tle.tle1
        return None

    def get_tle2(self, obj):
        """
        Returns tle2
        """
        if obj.tle:
            if obj.tle.tle2:
                return obj.tle.tle2
        return None

    def get_observer(self, obj):
        """
        Returns the author of the observation
        """
        if obj.author:
            return obj.author.pk
        return None

    def get_image_count(self, obj) -> int:
        """
        Returns the number of demoddata that are images associated with the observation
        """
        count = 0
        for demoddata in obj.demoddata.all():
            if demoddata.is_image:
                count += 1
        return count

    def get_demoddata_count(self, obj) -> int:
        """
        Returns the number of demoddata associated with the observation
        """
        return obj.demoddata.count()


class NewObservationListSerializer(serializers.ListSerializer):
    """
    SONIKS Network New Observation API List Serializer
    """

    transmitters: QuerySet
    violators: QuerySet
    tle_sets: QuerySet

    def validate(self, attrs):
        """
        Validates data from a list of new observations
        """

        (
            station_set,
            transmitter_uuid_set,
            transmitter_uuid_station_set,
            norad_id_set,
            transm_uuid_station_center_freq_set,
        ) = (set() for _ in range(5))
        uuid_to_norad_id = {}
        start_end_per_station = defaultdict(list)

        for observation in attrs:
            station = observation.get("ground_station")
            transmitter_uuid = observation.get("transmitter_uuid")
            station_set.add(station)
            transmitter_uuid_set.add(transmitter_uuid)
            transmitter_uuid_station_set.add((transmitter_uuid, station))
            start_end_per_station[int(station.id)].append(
                (observation.get("start"), observation.get("end"))
            )
        try:
            check_overlaps(start_end_per_station)
        except ObservationOverlapError as error:
            raise serializers.ValidationError(error, code="invalid")

        try:
            check_schedule_perms_per_station(self.context["request"].user, station_set)
        except UserNoPermissionError as error:
            raise serializers.ValidationError(error, code="forbidden")

        try:
            self.transmitters = Transmitter.objects.filter(
                uuid__in=transmitter_uuid_set
            )
            for uuid in transmitter_uuid_set:
                norad = self.transmitters.get(uuid).satellite.norad_cat_id
                norad_id_set.add(norad)
                uuid_to_norad_id[uuid] = norad
        except ValueError as error:
            raise serializers.ValidationError(error, code="invalid")

        self.violators = Satellite.objects.filter(
            norad_cat_id__in=norad_id_set, is_frequency_violator=True
        )
        violators_norad_ids = [satellite.norad_cat_id for satellite in self.violators]
        station_with_violators_set = {
            station
            for transmitter_uuid, station in transmitter_uuid_station_set
            if uuid_to_norad_id[transmitter_uuid] in violators_norad_ids
        }
        try:
            check_schedule_perms_of_violators_per_station(
                self.context["request"].user, station_with_violators_set
            )
        except UserNoPermissionError as error:
            raise serializers.ValidationError(error, code="forbidden")

        for observation in attrs:
            transmitter_uuid = observation.get("transmitter_uuid")
            station = observation.get("ground_station")
            center_frequency = observation.get("center_frequency", None)
            transmitter = self.transmitters.get(uuid=transmitter_uuid)
            if transmitter.type == "Transponder" and center_frequency is None:
                observation["center_frequency"] = (
                    transmitter.downlink_high + transmitter.downlink_low
                ) // 2
            transm_uuid_station_center_freq_set.add(
                (transmitter_uuid, station, center_frequency)
            )

        transmitter_station_list = [
            (self.transmitters.uuid, station, center_freq)
            for transmitter_uuid, station, center_freq in transm_uuid_station_center_freq_set
        ]
        try:
            check_transmitter_station_pairs(transmitter_station_list)
        except OutOfRangeError as error:
            raise serializers.ValidationError(error, code="invalid")
        return attrs

    def create(self, validated_data):
        """
        Creates new observations from a list of new observations validated data
        """
        new_observations = []
        observations_per_norad_id = defaultdict(list)
        for observation_data in validated_data:
            transmitter_uuid = observation_data["transmitter_uuid"]
            transmitter = self.transmitters.get(uuid=transmitter_uuid)

            observations_per_norad_id[transmitter.norad_cat_id].append(
                observation_data["start"]
            )

            observation = create_new_observation(
                station=observation_data["ground_station"],
                transmitter=transmitter,
                start=observation_data["start"],
                end=observation_data["end"],
                author=self.context["request"].user,
                center_frequency=observation_data.get("center_frequency", None),
            )
            new_observations.append(observation)

        if (
            self.violators
            and not self.context["request"]
            .user.groups.filter(name="Operators")
            .exists()
        ):
            check_violators_scheduling_limit(self.violators, observations_per_norad_id)

        for observation in new_observations:
            observation.save()

        return new_observations

    def update(self, instance, validated_data):
        """Updates observations from a list of validated data

        currently disabled and returns None
        """
        return None


class NewObservationSerializer(serializers.Serializer):
    """
    SONIKS Network New Observation API Serializer
    """

    start = serializers.DateTimeField(
        input_formats=["%Y-%m-%d %H:%M:%S.%f", "%Y-%m-%d %H:%M:%S"],
        error_messages={
            "invalid": "Start datetime should have either '%Y-%m-%d %H:%M:%S.%f' or "
            "'%Y-%m-%d %H:%M:%S' "
            "format.",
            "required": "Start('start' key) datetime is required.",
        },
    )
    end = serializers.DateTimeField(
        input_formats=["%Y-%m-%d %H:%M:%S.%f", "%Y-%m-%d %H:%M:%S"],
        error_messages={
            "invalid": "End datetime should have either '%Y-%m-%d %H:%M:%S.%f' or "
            "'%Y-%m-%d %H:%M:%S' "
            "format.",
            "required": "End datetime('end' key) is required.",
        },
    )
    ground_station = serializers.PrimaryKeyRelatedField(
        queryset=Station.objects.filter(
            status__gt=0, alt__isnull=False, lat__isnull=False, lng__isnull=False
        ),
        allow_null=False,
        error_messages={
            "does_not_exist": "Station should exist, be online and have a defined location.",
            "required": "Station('ground_station' key) is required.",
        },
    )
    transmitter_uuid = serializers.CharField(
        max_length=22,
        min_length=22,
        error_messages={
            "invalid": "Transmitter UUID should be valid.",
            "required": "Transmitter UUID('transmitter_uuid' key) is required.",
        },
    )

    center_frequency = serializers.IntegerField(
        error_messages={"negative": "Frequency cannot be a negative value."},
        required=False,
    )

    def validate_start(self, value):
        """
        Validates start datetime of a new observation
        """
        try:
            check_start_datetime(value)
        except ValueError as error:
            raise serializers.ValidationError(error, code="invalid")
        return value

    def validate_end(self, value):
        """
        Validates end datetime of a new observation
        """
        try:
            check_end_datetime(value)
        except ValueError as error:
            raise serializers.ValidationError(error, code="invalid")
        return value

    def validate(self, attrs):
        """
        Validates combination of start and end datetimes of a new observation
        """
        start = attrs["start"]
        end = attrs["end"]
        try:
            check_start_end_datetimes(start, end)
        except ValueError as error:
            raise serializers.ValidationError(error, code="invalid")
        return attrs

    def create(self, validated_data):
        """
        Creates a new observation

        Currently not implemented and raises exception. If in the future we want to implement this
        serializer accepting and creating observation from single object instead from a list of
        objects, we should remove raising the exception below and implement the validations that
        exist now only on NewObservationListSerializer
        """
        raise serializers.ValidationError(
            "Serializer is implemented for accepting and schedule\
                                           only lists of observations"
        )

    def update(self, instance, validated_data):
        """
        Updates an observation from validated data, currently disabled and returns None
        """
        return None

    class Meta:
        list_serializer_class = NewObservationListSerializer


class FrequencyRangeSerializer(serializers.ModelSerializer):
    """
    SONIKS Network FrequencyRange API Serializer
    """

    class Meta:
        model = FrequencyRange
        fields = ("min_frequency", "max_frequency", "bands")


class AntennaSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Antenna API Serializer
    """

    antenna_type = serializers.StringRelatedField()
    frequency_ranges = FrequencyRangeSerializer(many=True)

    class Meta:
        model = Antenna
        fields = ("antenna_type", "frequency_ranges")


class SatelliteSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Satellite API Serializer
    """

    norad = serializers.IntegerField(min_value=0, source="norad_cat_id")
    created_by = serializers.SerializerMethodField()
    operator = serializers.SerializerMethodField()
    network = serializers.SerializerMethodField()
    launch = serializers.SerializerMethodField()
    countries = serializers.CharField(source="countries_str")

    class Meta:
        model = Satellite
        fields = (
            "sat_id",
            "norad",
            "norad_follow_id",
            "launch",
            "name",
            "names",
            "call_sign",
            "status",
            "launched",
            "deployed",
            "decayed",
            "is_frequency_violator",
            "created",
            "created_by",
            "citation",
            "operator",
            "network",
            "countries",
            "image",
            "website",
            "unknown",
        )

    def get_created_by(self, obj):
        """
        Returns the name of the user who created the satellite
        """
        if obj.created_by:
            return obj.created_by.username
        return None

    def get_operator(self, obj):
        """
        Returns the satellite operator name
        """
        if obj.operator:
            return obj.operator.name
        return None

    def get_network(self, obj):
        """
        Returns the network name
        """
        if obj.network:
            return obj.network.name
        return None

    def get_launch(self, obj):
        """
        Returns the launch id
        """
        try:
            return obj.launch.id
        except AttributeError:
            return None


class StationSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Station API Serializer
    """

    # Using SerializerMethodField instead of directly the reverse relation (antennas) with the
    # AntennaSerializer for not breaking the API, it should change in next API version
    antenna = serializers.SerializerMethodField()
    min_horizon = serializers.SerializerMethodField()
    observations = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    altitude = serializers.IntegerField(min_value=0, source="alt")

    class Meta:
        model = Station
        fields = (
            "id",
            "name",
            "altitude",
            "min_horizon",
            "lat",
            "lng",
            "qthlocator",
            "antenna",
            "created",
            "last_seen",
            "status",
            "observations",
            "description",
            "client_version",
            "target_utilization",
        )

    def get_min_horizon(self, obj):
        """Returns Station minimum horizon"""
        return obj.horizon

    def get_antenna(self, obj):
        """Returns Station antenna list"""

        antenna_types = {
            "Dipole": "dipole",
            "V-Dipole": "v-dipole",
            "Discone": "discone",
            "Ground Plane": "ground",
            "Yagi": "yagi",
            "Cross Yagi": "cross-yagi",
            "Helical": "helical",
            "Parabolic": "parabolic",
            "Vertical": "vertical",
            "Turnstile": "turnstile",
            "Quadrafilar": "quadrafilar",
            "Eggbeater": "eggbeater",
            "Lindenblad": "lindenblad",
            "Parasitic Lindenblad": "paralindy",
            "Patch": "patch",
            "Other Directional": "other direct",
            "Other Omni-Directional": "other omni",
        }
        serializer = AntennaSerializer(obj.antennas, many=True)
        antennas = []
        for antenna in serializer.data:
            for frequency_range in antenna["frequency_ranges"]:
                antennas.append(
                    {
                        "frequency": frequency_range["min_frequency"],
                        "frequency_max": frequency_range["max_frequency"],
                        "band": frequency_range["bands"],
                        "antenna_type": antenna_types[antenna["antenna_type"]],
                        "antenna_type_name": antenna["antenna_type"],
                    }
                )
        return antennas

    def get_observations(self, obj):
        """Returns Station observations number"""
        return obj.total_obs

    def get_status(self, obj):
        """Returns Station status"""
        try:
            return obj.get_status_display()
        except AttributeError:
            return None


class JobSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Job API Serializer
    """

    frequency = serializers.SerializerMethodField()
    mode = serializers.SerializerMethodField()
    transmitter = serializers.SerializerMethodField()
    baud = serializers.SerializerMethodField()
    tle0 = serializers.SerializerMethodField()
    tle1 = serializers.SerializerMethodField()
    tle2 = serializers.SerializerMethodField()

    class Meta:
        model = Observation
        fields = (
            "id",
            "start",
            "end",
            "ground_station",
            "tle0",
            "tle1",
            "tle2",
            "frequency",
            "mode",
            "transmitter",
            "baud",
        )

    def get_tle0(self, obj):
        """
        Returns tle0
        """
        if obj.tle:
            return obj.tle.tle0
        return None

    def get_tle1(self, obj):
        """
        Returns tle1
        """
        if obj.tle:
            return obj.tle.tle1
        return None

    def get_tle2(self, obj):
        """
        Returns tle2
        """
        if obj.tle:
            return obj.tle.tle2
        return None

    def get_frequency(self, obj):
        """
        Returns Observation frequency
        """
        try:
            frequency = obj.center_frequency or obj.transmitter.downlink_low
            frequency_drift = obj.transmitter.downlink_drift
            if obj.center_frequency or frequency_drift is None:
                return frequency
            return int(round(frequency + ((frequency * frequency_drift) / 1e9)))
        except AttributeError:
            return ""

    def get_transmitter(self, obj):
        """
        Returns Transmitter UUID
        """
        try:
            return obj.transmitter.uuid
        except AttributeError:
            return ""

    def get_mode(self, obj):
        """
        Returns Transmitter mode
        """
        try:
            return obj.transmitter.downlink_mode.name
        except AttributeError:
            return ""

    def get_baud(self, obj):
        """
        Returns Transmitter baudrate
        """
        try:
            return obj.transmitter.baud
        except AttributeError:
            return ""


class TransmitterStatsSerializer(serializers.ModelSerializer):
    """
    SONIKS Network TransmitterStats API Serializer
    """

    future_count = serializers.SerializerMethodField()

    class Meta:
        model = TransmitterStats
        exclude = ["id", "transmitter"]

    def get_future_count(self, obj):
        """
        Returns count of future observations
        """
        return obj.transmitter.future_count


class TransmitterSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Transmitter API Serializer
    """

    satellite_name = serializers.SerializerMethodField()
    satellite_norad = serializers.SerializerMethodField()
    satellite_launch = serializers.SerializerMethodField()
    downlink_mode = serializers.SerializerMethodField()
    uplink_mode = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    stat = TransmitterStatsSerializer()

    class Meta:
        model = Transmitter
        fields = (
            "uuid",
            "description",
            "satellite_name",
            "satellite_norad",
            "satellite_launch",
            "type",
            "status",
            "uplink_low",
            "uplink_high",
            "uplink_drift",
            "downlink_low",
            "downlink_high",
            "downlink_drift",
            "downlink_mode",
            "uplink_mode",
            "invert",
            "baud",
            "citation",
            "stat",
            "unconfirmed",
            "unknown",
            "approved",
        )

    def get_satellite_name(self, obj):
        """
        Returns Mode name
        """
        if obj.satellite:
            return obj.satellite.name
        return None

    def get_satellite_norad(self, obj):
        """
        Returns Mode name
        """
        if obj.satellite:
            return obj.satellite.norad_cat_id
        return None

    def get_satellite_launch(self, obj):
        """
        Returns Launch ID
        """
        try:
            return obj.satellite.launch.id
        except AttributeError:
            return None

    def get_downlink_mode(self, obj):
        """
        Returns Mode name
        """
        if obj.downlink_mode:
            return obj.downlink_mode.name
        return None

    def get_uplink_mode(self, obj):
        """
        Returns Mode name
        """
        if obj.uplink_mode:
            return obj.uplink_mode.name
        return None

    def get_type(self, obj):
        """
        Returns Type name in Russian
        """
        obj_type = obj.type
        if obj_type == "Transmitter":
            return "Приемник"
        if obj_type == "Transceiver":
            return "Передатчик"
        if obj_type == "Transponder":
            return "Приемопередатчик"
        return None


class DemodDataViewSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Demoddata API Serializer
    """

    id = serializers.SerializerMethodField()
    sat_id = serializers.SerializerMethodField()
    norad_cat_id = serializers.SerializerMethodField()
    transmitter = serializers.SerializerMethodField()
    demodulated_data = serializers.SerializerMethodField()
    payload_json = serializers.SerializerMethodField()

    class Meta:
        model = DemodData
        fields = (
            "id",
            "sat_id",
            "norad_cat_id",
            "transmitter",
            "demodulated_data",
            "observer",
            "timestamp",
            "version",
            "observation_id",
            "station_id",
            "payload_json",
            "is_image",
            "sonik_data",
            "used_in_gallery",
            "gallery_cover",
        )

    @extend_schema_field(OpenApiTypes.STR)
    def get_id(self, obj):
        """
        Returns Demoddata ID
        """
        return obj.pk

    @extend_schema_field(OpenApiTypes.STR)
    def get_sat_id(self, obj):
        """
        Returns Satellite Identifier
        """
        return obj.satellite.sat_id

    @extend_schema_field(OpenApiTypes.INT64)
    def get_norad_cat_id(self, obj):
        """
        Returns Satellite NORAD ID
        """
        return obj.satellite.norad_cat_id

    @extend_schema_field(OpenApiTypes.UUID)
    def get_transmitter(self, obj):
        """
        Returns Transmitter UUID
        """
        try:
            return obj.transmitter.uuid
        except AttributeError:
            return ""

    @extend_schema_field(OpenApiTypes.STR)
    def get_demodulated_data(self, obj):
        """
        Returns the payload frame
        """
        try:
            with obj.demodulated_data.storage.open(
                obj.demodulated_data.name, "r"
            ) as file:
                data = file.read()
                if re.fullmatch(r"^[A-F0-9]+$", data):
                    return data
                raise TypeError
        except (TypeError, UnicodeDecodeError):
            return obj.display_payload_hex().replace(" ", "")

    @extend_schema_field(OpenApiTypes.JSON_PTR)
    def get_payload_json(self, obj):
        """
        Returns decoded payload frame translated according to trans_decode field in Satellite
        """

        def apply_translations(
            data: Dict[str, Union[int, float, str]],
            translations: Dict[str, str],
            multipliers: Dict[str, float],
        ) -> Dict[str, Union[int, float, str]]:
            """
            Applies translations and multipliers to data.

            Args:
                data (Dict[str, Union[int, float, str]]): Dictionary of data to be translated and multiplied.
                translations (Dict[str, str]): Dictionary of key translations.
                multipliers (Dict[str, float]): Dictionary of multipliers for each key.

            Returns:
                Dict[str, Union[int, float, str]]: Translated and multiplied data.
            """
            translated_data = {}
            for key, value in data.items():
                # Применение перевода (если есть)
                new_key = translations.get(key, key)

                # Применение множителя (если есть)
                multiplier = multipliers.get(key, 1.0)
                if isinstance(value, (int, float)):
                    translated_data[new_key] = value * multiplier
                else:
                    translated_data[new_key] = value
            return translated_data

        if obj.payload_json:
            try:
                # Чтение исходного JSON
                with obj.payload_json.open("r") as file:
                    data = json.load(file)

                # Получение текущего языка
                current_lang = translation.get_language()

                # Проверка наличия trans_decode в Satellites
                satellite = obj.satellite
                if not hasattr(satellite, "trans_decode"):
                    logger.error("Satellite не имеет поля trans_decode")
                    return ""

                trans_data = satellite.trans_decode

                if trans_data is None:
                    logger.error("trans_decode равно None")
                    return data  # Возвращаем исходные данные, если trans_decode пусто

                if not isinstance(trans_data, dict):
                    logger.error(
                        f"trans_decode не является словарем: {type(trans_data)}"
                    )
                    return data  # Возвращаем исходные данные, если формат неверный

                # Извлечение множителей и переводов
                multipliers = trans_data.get("multipliers", {})
                translations = trans_data.get("translations", {}).get(current_lang, {})

                # Обработка списка или словаря
                if isinstance(data, list):
                    for item in data:
                        if "fields" in item and isinstance(item["fields"], dict):
                            item["fields"] = apply_translations(
                                item["fields"], translations, multipliers
                            )
                elif isinstance(data, dict) and "fields" in data:
                    data["fields"] = apply_translations(
                        data["fields"], translations, multipliers
                    )

                return data

            except json.JSONDecodeError as e:
                logger.error(f"Ошибка парсинга JSON: {str(e)}")
                return ""
            except Exception as e:
                logger.error(f"Неизвестная ошибка: {str(e)}")
                return ""
        return ""


class SidsSerializer(serializers.ModelSerializer):
    """
    SONIKS Network SiDS API Serializer
    """

    class Meta:
        model = DemodData
        fields = (
            "satellite",
            "demodulated_data",
            "lat",
            "lng",
            "timestamp",
            "observer",
            "version",
            "observation",
            "station",
            "sonik_data",
        )


class ModeSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Mode API Serializer
    """

    class Meta:
        model = Mode
        fields = (
            "id",
            "name",
        )


class SatelliteTleSerializer(serializers.ModelSerializer):
    """
    Serializer for satellites in Tle API endpoint
    """

    class Meta:
        model = Satellite
        fields = ("name", "norad_cat_id", "sat_id")


class TleViewSerializer(serializers.ModelSerializer):
    """
    SONIKS Network Tle API Serializer
    """

    satellite = SatelliteTleSerializer()

    class Meta:
        model = Tle
        fields = ("tle0", "tle1", "tle2", "updated", "tle_source", "satellite")


class LatestTleViewSerializer(serializers.ModelSerializer):
    """
    SONIKS Network LatestTle API Serializer
    """

    satellite = SatelliteTleSerializer()
    latest = TleSerializer()

    class Meta:
        model = LatestTleSet
        fields = ("last_modified", "satellite", "latest")


class ObsTimeSerializer(serializers.Serializer):
    start = serializers.DateTimeField()
    end = serializers.DateTimeField()


class SatelliteObsOnStationsSerializer(serializers.Serializer):
    lat = serializers.FloatField()
    lon = serializers.FloatField(source="lng")
    obs = ObsTimeSerializer(source="observations", many=True)
