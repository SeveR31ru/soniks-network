"""SONIKS Network API test suites"""

import json
from datetime import timedelta

import pytest
from django.test import TestCase
from django.utils.timezone import now
from requests.utils import parse_header_links
from rest_framework import status
from rest_framework.utils.encoders import JSONEncoder

from network.api.pagination import ObservationCursorPagination
from network.base.tests import (
    AntennaFactory,
    DemodDataFactory,
    FrequencyRangeFactory,
    LatestTleFactory,
    ModeFactory,
    NetworkFactory,
    ObservationFactory,
    OperatorFactory,
    SatelliteFactory,
    StationFactory,
    TleFactory,
    TransmitterFactory,
)
from network.users.tests import UserFactory


@pytest.mark.django_db(transaction=True)
class SatelliteViewApiTest(TestCase):
    """
    Tests the Satellite View API
    """

    satellite = None

    def setUp(self):
        self.encoder = JSONEncoder()
        self.operator = OperatorFactory()
        self.network = NetworkFactory()
        self.satellite = SatelliteFactory(operator=self.operator, network=self.network)

    def test_satellite_view_api(self):
        """Test the Satellite View API"""

        satellite_serialized = {
            "sat_id": self.satellite.sat_id,
            "norad": self.satellite.norad_cat_id,
            "norad_follow_id": self.satellite.norad_follow_id,
            "launch": None,
            "name": self.satellite.name,
            "names": self.satellite.names,
            "call_sign": self.satellite.call_sign,
            "status": self.satellite.status,
            "launched": self.encoder.default(self.satellite.launched),
            "deployed": self.encoder.default(self.satellite.deployed),
            "decayed": self.encoder.default(self.satellite.decayed),
            "is_frequency_violator": self.satellite.is_frequency_violator,
            "created": self.encoder.default(self.satellite.created),
            "created_by": None,
            "citation": "",
            "operator": self.operator.name,
            "network": self.network.name,
            "countries": "",
            "website": "",
            "image": None,
            "unknown": False,
        }

        response = self.client.get("/api/satellites/")
        response_json = json.loads(response.content)
        self.assertEqual(response_json[0], satellite_serialized)


@pytest.mark.django_db(transaction=True)
class TransmitterViewApiTest(TestCase):
    """
    Tests the Transmitter View API
    """

    transmitters = []
    satellites = []
    statistics = []
    transmitters_for_sat = []
    satellite = None

    def setUp(self):
        self.transmitters = []
        self.satellites = []
        self.statistics = []
        self.transmitters_for_sat = []
        for _ in range(10):
            self.satellites.append(SatelliteFactory())
        for _ in range(10):
            self.transmitters.append(TransmitterFactory())

        self.satellite = SatelliteFactory()
        for _ in range(10):
            self.transmitters_for_sat.append(
                TransmitterFactory(satellite=self.satellite)
            )

    def test_transmitter_view_api(self):
        """Test the Transmitter View API wirh filter by UUID"""
        transmitter_type = "Приемник"
        if self.transmitters[0].type == "Transceiver":
            transmitter_type = "Передатчик"
        if self.transmitters[0].type == "Transponder":
            transmitter_type = "Приемопередатчик"

        transmitter_serialized = {
            "uuid": self.transmitters[0].uuid,
            "description": self.transmitters[0].description,
            "satellite_name": self.transmitters[0].satellite.name,
            "satellite_norad": self.transmitters[0].satellite.norad_cat_id,
            "satellite_launch": None,
            "type": transmitter_type,
            "status": self.transmitters[0].status,
            "uplink_low": self.transmitters[0].uplink_low,
            "uplink_high": self.transmitters[0].uplink_high,
            "uplink_drift": self.transmitters[0].uplink_drift,
            "downlink_low": self.transmitters[0].downlink_low,
            "downlink_high": self.transmitters[0].downlink_high,
            "downlink_drift": self.transmitters[0].downlink_drift,
            "downlink_mode": self.transmitters[0].downlink_mode.name,
            "uplink_mode": self.transmitters[0].uplink_mode.name,
            "invert": self.transmitters[0].invert,
            "baud": self.transmitters[0].baud,
            "citation": self.transmitters[0].citation,
            "unconfirmed": self.transmitters[0].unconfirmed,
            "stat": {},
            "unknown": False,
            "approved": True,
        }

        response = self.client.get(
            "/api/transmitters/?uuid={0}".format(self.transmitters[0].uuid)
        )
        response_json = json.loads(response.content)
        response_json[0]["stat"] = {}
        self.assertEqual(response_json[0], transmitter_serialized)
        self.assertEqual(len(response_json), 1)

    def test_transmitter_view_api_with_satellite_filter(self):
        """Test the Transmitter View API wirh filter by satellite_norad"""
        response = self.client.get(
            "/api/transmitters/?satellite__norad_cat_id={0}".format(
                self.satellite.norad_cat_id
            )
        )
        response_json = json.loads(response.content)
        uuids = []
        self.assertEqual(len(response_json), self.satellite.transmitter_entries.count())
        for transmitter in response_json:
            self.assertEqual(transmitter["satellite_name"], self.satellite.name)
            uuids.append(transmitter["uuid"])
        transmitters_sat = self.satellite.transmitter_entries.values("uuid")
        for obj in transmitters_sat:
            self.assertIn(obj["uuid"], uuids)


@pytest.mark.django_db(transaction=True)
class StationViewApiTest(TestCase):
    """
    Tests the Station View API
    """

    station = None

    def setUp(self):
        self.encoder = JSONEncoder()
        self.station = StationFactory()
        self.antenna = AntennaFactory(station=self.station)
        self.frequency_range = FrequencyRangeFactory(antenna=self.antenna)

    def test_station_view_api(self):
        """Test the Station View API"""

        antenna_types = {
            "Dipole": "dipole",
            "V-Dipole": "v-dipole",
            "Discone": "discone",
            "Ground Plane": "ground",
            "Yagi": "yagi",
            "Cross Yagi": "cross-yagi",
            "Helical": "helical",
            "Parabolic": "parabolic",
            "Vertical": "vertical",
            "Turnstile": "turnstile",
            "Quadrafilar": "quadrafilar",
            "Eggbeater": "eggbeater",
            "Lindenblad": "lindenblad",
            "Parasitic Lindenblad": "paralindy",
            "Patch": "patch",
            "Other Directional": "other direct",
            "Other Omni-Directional": "other omni",
        }

        ser_ants = [
            {
                "band": self.frequency_range.bands,
                "frequency": self.frequency_range.min_frequency,
                "frequency_max": self.frequency_range.max_frequency,
                "antenna_type": antenna_types[self.antenna.antenna_type.name],
                "antenna_type_name": self.antenna.antenna_type.name,
            }
        ]

        station_serialized = {
            "id": self.station.id,
            "altitude": self.station.alt,
            "antenna": ser_ants,
            "client_version": self.station.client_version,
            "created": self.encoder.default(self.station.created),
            "description": self.station.description,
            "last_seen": self.encoder.default(self.station.last_seen),
            "lat": self.station.lat,
            "lng": self.station.lng,
            "min_horizon": self.station.horizon,
            "name": self.station.name,
            "observations": 0,
            "qthlocator": self.station.qthlocator,
            "target_utilization": self.station.target_utilization,
            "status": self.station.get_status_display(),
        }

        response = self.client.get("/api/stations/")
        response_json = json.loads(response.content)
        self.assertEqual(response_json, [station_serialized])


class ObservationViewApiTest(TestCase):
    """
    Tests the Observation API View
    """

    def setUp(self):
        self.observations = []
        for _ in range(ObservationCursorPagination.page_size * 2 + 1):
            self.observations.append(ObservationFactory())

    def test_observations_listview_pagination(self):
        """
        Tests the pagination of the observations list view
        """
        response = self.client.get("/api/observations/")
        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert isinstance(data, list)
        assert len(data) == ObservationCursorPagination.page_size
        assert response.get("link")
        links = parse_header_links(response["link"])
        assert links
        next_link = next((link for link in links if link["rel"] == "next"), None)
        assert next_link
        assert next_link.get("url")

        next_response = self.client.get(next_link["url"])
        assert next_response.status_code == status.HTTP_200_OK
        data = next_response.json()
        assert len(data) == ObservationCursorPagination.page_size
        links = parse_header_links(next_response["link"])
        assert links
        prev_link = next((link for link in links if link["rel"] == "prev"), None)
        assert prev_link
        assert prev_link.get("url")
        assert self.client.get(prev_link["url"]).status_code == status.HTTP_200_OK

        next_link = next((link for link in links if link["rel"] == "next"), None)
        assert next_link
        assert next_link.get("url")
        assert self.client.get(next_link["url"]).status_code == status.HTTP_200_OK


@pytest.mark.django_db(transaction=True)
class JobViewApiTest(TestCase):
    """
    Tests the Job View API
    """

    observation = None
    satellites = []
    transmitters = []
    stations = []

    def setUp(self):
        for _ in range(10):
            self.satellites.append(SatelliteFactory())
        for _ in range(10):
            self.stations.append(StationFactory())
        self.future_observation = ObservationFactory(start=now() + timedelta(days=1))
        self.past_observation = ObservationFactory(start=now() - timedelta(days=1))

    def test_job_view_api(self):
        """Test the Job View API"""
        response = self.client.get("/api/jobs/")
        response_json = json.loads(response.content)
        self.assertEqual(len(response_json), 1)
        self.assertEqual(response_json[0]["id"], self.future_observation.id)
        self.assertNotEqual(response_json[0]["id"], self.past_observation.id)


@pytest.mark.django_db(transaction=True)
class DemodDataViewApiTest(TestCase):
    """
    Tests the DemodData View API
    """

    satellite = None
    transmitter = None
    observation = None
    station = None
    user = None
    demoddata = None

    def setUp(self):
        self.encoder = JSONEncoder()
        self.user = UserFactory()
        self.station = StationFactory()
        self.satellite = SatelliteFactory()
        self.transmitter = TransmitterFactory(satellite=self.satellite)
        self.observation = ObservationFactory(
            transmitter=self.transmitter,
            satellite=self.satellite,
            ground_station=self.station,
        )
        self.demoddata = DemodDataFactory(
            transmitter=self.transmitter,
            satellite=self.satellite,
            station=self.station,
            observation=self.observation,
        )

    def test_demoddata_view_api_authentication(self):
        """Test for API demoddata with unauthorized request"""
        response = self.client.get("/api/demoddata/")
        self.assertEqual(response.status_code, 401)

    def test_demoddata_view_api_without_filters(self):
        """Test for API demoddata without filters"""
        self.client.force_login(self.user)
        response = self.client.get("/api/demoddata/")
        self.assertEqual(response.status_code, 400)

    def test_demoddata_view_api(self):
        """Test for API demoddata with unauthorized request"""
        self.client.force_login(self.user)

        demoddata_serialized = {
            "id": self.demoddata.id,
            "sat_id": self.satellite.sat_id,
            "norad_cat_id": self.satellite.norad_cat_id,
            "transmitter": self.transmitter.uuid,
            "demodulated_data": self.demoddata.display_payload_hex(),
            "observer": "",
            "timestamp": self.encoder.default(self.demoddata.timestamp),
            "version": "",
            "observation_id": self.observation.id,
            "station_id": self.station.id,
            "payload_json": "",
            "is_image": False,
            "sonik_data": True,
            "used_in_gallery": False,
            "gallery_cover": False,
        }

        response = self.client.get(
            f"/api/demoddata/?satellite={0}&format=json".format(
                self.satellite.norad_cat_id
            )
        )
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_json["results"][0], demoddata_serialized)


@pytest.mark.django_db(transaction=True)
class ModesViewApiTest(TestCase):
    """
    Tests the Modes View API
    """

    mode = None

    def setUp(self):
        self.mode = ModeFactory()

    def test_demoddata_view_api(self):
        """
        Test for API Mode with unauthorized request
        """
        mode_serializer = {"id": self.mode.id, "name": self.mode.name}

        response = self.client.get("/api/modes/?format=json")
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_json[0], mode_serializer)


@pytest.mark.django_db(transaction=True)
class TleViewApiTest(TestCase):
    """
    Tests for Tle View API
    """

    satellite = None
    tle = None

    def setUp(self):
        self.satellite = SatelliteFactory()
        self.tle = TleFactory(satellite=self.satellite)
        self.encoder = JSONEncoder()

    def test_tle_view_api(self):
        """
        Test for API Tle with unauthorized requests
        """

        tle_serializer = {
            "tle0": self.tle.tle0,
            "tle1": self.tle.tle1,
            "tle2": self.tle.tle2,
            "updated": self.encoder.default(self.tle.updated),
            "tle_source": self.tle.tle_source,
            "satellite": {
                "name": self.satellite.name,
                "norad_cat_id": self.satellite.norad_cat_id,
                "sat_id": self.satellite.sat_id,
            },
        }
        response = self.client.get("/api/tles/?format=json")
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_json["results"][0], tle_serializer)


@pytest.mark.django_db(transaction=True)
class LatestTleViewApiTest(TestCase):
    """
    Tests for LatestTleView API
    """

    satellite = None
    tle = None
    latest_tle = None

    def setUp(self):
        self.satellite = SatelliteFactory()
        self.tle = TleFactory(satellite=self.satellite)
        self.latest_tle = LatestTleFactory(
            satellite=self.satellite, latest=self.tle, latest_distributable=self.tle
        )
        self.encoder = JSONEncoder()

    def test_latesttle_view_api(self):
        """
        Test for API LatestTle with unauthorized requests
        """
        latest_tle_serializer = {
            "last_modified": self.encoder.default(self.latest_tle.last_modified),
            "satellite": {
                "name": self.satellite.name,
                "norad_cat_id": self.satellite.norad_cat_id,
                "sat_id": self.satellite.sat_id,
            },
            "latest": {
                "tle0": self.tle.tle0,
                "tle1": self.tle.tle1,
                "tle2": self.tle.tle2,
                "updated": self.encoder.default(self.tle.updated),
                "tle_source": self.tle.tle_source,
            },
        }
        response = self.client.get("/api/latesttles/?format=json")
        response_json = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_json[0], latest_tle_serializer)
